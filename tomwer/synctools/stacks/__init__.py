"""
Contains the stack managed by qtSignals and used by the Orange add-on in order
to insure 'parallel computation' of the different process and keep gui alive.
"""
