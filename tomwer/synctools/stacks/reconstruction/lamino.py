#/*##########################################################################
# Copyright (C) 2016-2017 European Synchrotron Radiation Facility
#
# This file is part of the PyMca X-ray Fluorescence Toolkit developed at
# the ESRF by the Software group.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/


__author__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/10/2018"


from queue import Queue

from silx.gui import qt

from tomwer.core.log import TomwerLogger
from tomwer.core.process.reconstruction.lamino.tofu import \
    tofuLaminoReconstruction
from tomwer.core.scan.scanbase import TomoBase
from tomwer.core.utils.Singleton import singleton
from tomwer.web.client import OWClient

logger = TomwerLogger(__name__)


@singleton
class LaminoReconstructionStack(qt.QObject, Queue, OWClient):
    """
    Manage a stack of lamino (tofu) reconstruction
    """

    sigReconsFinished = qt.Signal(TomoBase)
    sigReconsFailed = qt.Signal(TomoBase)
    sigReconsMissParams = qt.Signal(TomoBase)

    def __init__(self):
        qt.QObject.__init__(self)
        Queue.__init__(self)
        OWClient.__init__(self, logger)
        self.reconsThread = _LaminoReconsThread()
        self.reconsThread.sigThReconsFinished.connect(self._dealWithFinishedRecons)
        self.reconsThread.sigThReconsFailed.connect(self._dealWithFailedRecons)
        self.reconsThread.sigThMissingParams.connect(self._dealWithThMissingParams)
        self._forceSync = False

    def add(self, scan, recons_params, additional_opts, remove_existing,
            callback):
        """
        add a reconstruction and will run it as soon as possible

        :param scan: the folder of the acquisition to reconstruct
        :type scan: :class:`.TomoBase`
        :param dict reconsParams: parameters of the reconstruction
        :param dict additional_opts: not managed directly by the gui
        :param bool remove_existing: if True then remove output dir before
                                     reconstruction
        :param callback: function to call after the reconstruction execution
        """
        assert isinstance(recons_params, dict)
        assert isinstance(scan, TomoBase)
        Queue.put(self, (scan, recons_params, additional_opts,
                         remove_existing, callback))
        if self.canExecNext():
            self.execNext()

    def execNext(self):
        """
        Launch the next reconstruction if any
        """
        if Queue.empty(self):
            return

        assert(not self.reconsThread.isRunning())
        scan, recons_params, additional_opts, remove_existing, callback = Queue.get(self)
        self.reconsThread.init(scan=scan,
                               recons_params=recons_params,
                               additional_opts=additional_opts,
                               remove_existing=remove_existing)
        self.reconsThread.sigThReconsFinished.connect(callback)
        self.reconsThread.start()
        if self._forceSync is True:
            self.reconsThread.wait()

    def canExecNext(self):
        """
        Can we launch an ftserie reconstruction.
        Reconstruction can't be runned in parallel

        :return: True if no reconstruction is actually running
        """
        return not self.reconsThread.isRunning()

    def _dealWithFinishedRecons(self, scan):
        assert isinstance(scan, TomoBase)
        info = 'reconstruction %s is finished' % scan.path
        logger.info(info)
        self.sigReconsFinished.emit(scan)
        self.execNext()

    def _dealWithThMissingParams(self, scan):
        assert isinstance(scan, TomoBase)
        self.sigReconsMissParams.emit(scan)
        self.execNext()

    def _dealWithFailedRecons(self, scan):
        assert isinstance(scan, TomoBase)
        self.sigReconsFailed.emit(scan)
        self.execNext()

    def setMockMode(self, b):
        self.reconsThread.setMockMode(b)
        self.execNext()

    def setForceSync(self, b=True):
        self._forceSync = b


class _LaminoReconsThread(qt.QThread):
    """Thread used for running lamino reconstrucion using Tofu"""
    sigThReconsFinished = qt.Signal(TomoBase)
    "Emitted if reconstruction ended with success"
    sigThReconsFailed = qt.Signal(TomoBase)
    "Emitted if reconstruction failed"
    sigThMissingParams = qt.Signal(TomoBase)
    "Emitted if missing some reconstruction parameters"

    def __init__(self):
        qt.QThread.__init__(self)
        self.scan = None
        self.recons_params = None
        self.additional_opts = None
        self.callback = None
        self.remove_existing = False

    def init(self, recons_params, additional_opts, remove_existing, scan):
        assert isinstance(scan, TomoBase)
        self.recons_params = recons_params
        self.additional_opts = additional_opts
        self.remove_existing = remove_existing
        self.scan = scan

    def run(self):
        try:
            tofuLaminoReconstruction(scan_id=self.scan.path,
                                     recons_param=self.recons_params,
                                     additional_options=self.additional_opts,
                                     delete_existing=self.remove_existing)
        except ValueError as error:
            logger.warning(error)
            self.sigThMissingParams.emit("Some parameters are missing or are incoherent")
        except Exception as error:
            logger.error("fail to run lamino reconstruction for %s reason is "
                         "%s" % (self.scan, error))
            self.sigThReconsFailed.emit("fail to run reconstruction %s" % self.scan.path)
        else:
            self.sigThReconsFinished.emit(self.scan)
