# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "29/05/2017"

import os
import unittest
import time
import signal
import multiprocessing
import functools
import shutil
import tempfile
from tomwer.test.utils import UtilsTest
from tomwer.luigi.utils import lugiserver_is_running
from ..parser import scheme_load
from ..scheme import Scheme
from .. import WorkflowTask
import luigi
from luigi import scheduler
from luigi import server
import luigi.cmdline
from luigi.six.moves.urllib.parse import urlencode, ParseResult, quote as urlquote
from nose.plugins.attrib import attr
from tomwer.luigi import WorkflowTask

try:
    from unittest import mock
except ImportError:
    import mock


class TestLuigi(unittest.TestCase):
    """Test that we can convert a simple ows workflow into luigi workflow
    """
    @classmethod
    def setUpClass(cls):
        # get the .ows file from the web if needed
        dataSetID = 'owsfiles'
        datafolder = UtilsTest.getDataset(dataSetID)
        assert(os.path.isdir(datafolder))
        cls.datafile = os.path.join(datafolder, 'testLuigi_2FinalsNodes.ows')
        assert(os.path.isfile(cls.datafile))

    def setUp(self):
        self.tmp_folder = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmp_folder)

    def testSchemeRead(self):
        """Make sure we the scheme is correctly loaded after parsing the file
        """
        scheme = Scheme()
        with open(self.datafile, "rb") as f:
            scheme_load(scheme, f)

        self.assertTrue(len(scheme.nodes) is 3)
        finalNodes = scheme.finalsNodes()
        self.assertTrue(len(finalNodes) is 2)
        self.assertTrue(len(scheme.links) is 2)

    def testSchemeSave(self):
        scheme = Scheme()
        with open(self.datafile, "rb") as f:
            scheme_load(scheme, f)

        output_file = os.path.join(self.tmp_folder, 'outputfile.ows')
        scheme.save_to(output_file)
        self.assertTrue(os.path.exists(output_file))
        self.assertTrue(os.path.getsize(output_file) > 0)

        scheme = Scheme()
        with open(output_file, "rb") as f:
            scheme_load(scheme, f)

        self.assertTrue(len(scheme.nodes) is 3)
        self.assertTrue(len(scheme.links) is 2)
        finalNodes = scheme.finalsNodes()
        self.assertTrue(len(finalNodes) is 2)


# come from the luigi unit tests 9see luigi/luigig/test/server_test)
class _LocalLuigiServerTest(unittest.TestCase):
    """
    Test to start and stop the server in a more "standard" way
    """
    class ServerClient(object):
        def __init__(self):
            self.tempdir = tempfile.mkdtemp()
            self.port = 8083

        @mock.patch('daemon.DaemonContext')
        def run_server(self, daemon_context):
            luigi.cmdline.luigid([
                '--port', str(self.port),
                '--background',  # This makes it a daemon
                '--logdir', self.tempdir,
                '--pidfile', os.path.join(self.tempdir, 'luigid.pid')
            ])

        def scheduler(self):
            return luigi.rpc.RemoteScheduler('http://localhost:' + str(self.port))

    server_client_class = ServerClient

    def start_server(self):
        self._process = multiprocessing.Process(
            target=self.server_client.run_server
        )
        self._process.start()
        time.sleep(0.1)  # wait for server to start
        self.sch = self.server_client.scheduler()
        self.sch._wait = lambda: None

    def stop_server(self):
        self._process.terminate()
        self._process.join(timeout=1)
        if self._process.is_alive():
            os.kill(self._process.pid, signal.SIGKILL)

    def setUp(self):
        # skip proxy as run locally
        self.proxy = {}
        for proxy_key in ('http_proxy', 'https_proxy'):
            if proxy_key in os.environ:
                self.proxy[proxy_key] = os.environ[proxy_key]
                del os.environ[proxy_key]

        self.server_client = self.server_client_class()
        state_path = tempfile.mktemp(suffix=self.id())
        self.addCleanup(functools.partial(os.unlink, state_path))
        luigi.configuration.get_config().set('scheduler', 'state_path', state_path)
        self.start_server()

    def tearDown(self):
        # set back proxy
        for _proxy_key, _proxy_val in self.proxy.items():
            os.environ[_proxy_key] = _proxy_val
        self.stop_server()
        shutil.rmtree(self.server_client.tempdir)

    def test_with_cmdline(self):
        """
        Test to run against the server as a normal luigi invocation does
        """
        params = ['Task', '--scheduler-port', str(self.server_client.port), '--no-lock']
        self.assertTrue(luigi.interface.run(params))



@unittest.skipIf(lugiserver_is_running() is True, 'local luigi server already running')
class TestScenario_01(_LocalLuigiServerTest):
    """
    Test scenario:

    datawatcher -> data transfert
    """
    def setUp(self):
        _LocalLuigiServerTest.setUp(self)
        fileID = 'luigi_test_01.ows'
        self._test_file = UtilsTest.getOrangeTestFile(fileID)

        self.source_folder = tempfile.mkdtemp()
        self.output_folder = tempfile.mkdtemp()
        self.datasetIDs = ('test10', 'test01')
        self.datasets = {}
        for datasetID in self.datasetIDs:
            self.datasets[datasetID] = os.path.join(self.source_folder, datasetID)
            shutil.copytree(src=UtilsTest().getDataset(datasetID),
                            dst=self.datasets[datasetID])

        self.updateConfigurationFile()

    def updateConfigurationFile(self):
        """Add path to be send to the transfer"""
        scheme = Scheme()
        with open(self._test_file, "rb") as f:
            scheme_load(scheme, f)

        # node 0 should be tomodir and node 1 data transfert
        assert '0' in scheme.nodes_dict
        assert scheme.nodes_dict['0']._qualified_name == 'orangecontrib.tomwer.widgets.control.TomoDirOW.TomoDirOW'
        assert '1' in scheme.nodes_dict
        assert scheme.nodes_dict['1']._qualified_name == 'orangecontrib.tomwer.widgets.control.DataTransfertOW.DataTransfertOW'

        print(scheme.nodes_dict['0'].properties)
        scheme.nodes_dict['0'].properties['folderObserved'] = self.source_folder
        scheme.nodes_dict['1'].properties['forceDestDir'] = self.output_folder
        # TODO ?: also set properties for obsMethod, srcPattern, destPattern, patternObs
        # write back the configuration
        scheme.save_to(self._test_file)

    def tearDown(self):
        _LocalLuigiServerTest.tearDown(self)

    def testRunLocal(self):
        scheme = Scheme()
        with open(self._test_file, "rb") as f:
            scheme_load(scheme, f)

        self.assertTrue(len(scheme.nodes) is 2)
        self.assertTrue(len(scheme.links) is 1)
        self.assertTrue(len(scheme.finalsNodes()) is 0)
        self.assertTrue(len(scheme.controlNodes()) is 1)

        self.assertTrue(len(os.listdir(self.output_folder)) is 0)
        self.assertTrue(len(os.listdir(self.source_folder)) is 2)


        # TODO: update configuration file to define output folder

        task_class = 'esrf.tomwer.WorkflowTask'

        params = [task_class,
                  '--workers', '2',
                  '--configuration-file', self._test_file,
                  '--scheduler-port', str(self.server_client.port),
                  '--no-lock'
                  ]
        self.assertTrue(luigi.interface.run(params))

        timeout = 20
        # wait for copy

        assert len(os.listdir(self.output_folder)) is 2

        # workflow_luigi = toLuigi 'luigi_test_02.ows'
        # workflow_luigi.
        # workflow_luigi.run()

        self.assertTrue(len(os.listdir(self.output_folder)) is 2)
        self.assertTrue(len(os.listdir(self.source_folder)) is 1)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestLuigi, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
