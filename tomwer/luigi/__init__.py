# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

"""Module to access the tomwer library through luigi tasks"""

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "26/07/2018"


from tomwer.luigi.scheme import Scheme
from tomwer.luigi.scheme import scheme_load
from .task.task import *
import tempfile
import logging

_logger = logging.getLogger(__name__)


def remove_proxy():
    proxy = {}
    for proxy_key in ('http_proxy', 'https_proxy'):
        if proxy_key in os.environ:
            proxy[proxy_key] = os.environ[proxy_key]
            del os.environ[proxy_key]
    return proxy


def exec_(scheme, scan=None, properties_folder=None, workers=2, name=None):
    """
    
    :param scheme: 
    :param properties_folder: 
    :param workers: 
    :param str name: workflow name
    :return: 
    """
    # define the properties dir
    remove_proxy()
    properties_folder = properties_folder or tempfile.mkdtemp()
    properties_files = {}
    for node in scheme.nodes:
        properties_files[node] = node.write_properties_file(properties_folder)

    # TODO: we might simplify this on create a high level process which will be
    # to englobe all final_nodes
    for final_node in scheme.finalsNodes():
        # TODO: remove test and generate auto string
        final_node.run(scan=scan, workers=workers, name=(name or 'test'))

    for endless_node in scheme.endlessNodes():
        endless_node.run(workers=workers, name=(name or 'test'))
