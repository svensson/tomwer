# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "12/12/2018"


import luigi
import logging
import importlib
import os
from silx.io import dictdump
from silx.gui import qt
from luigi.parameter import ParameterVisibility
from tomwer.core.scan.scanfactory import ScanFactory
import datetime

app = qt.QApplication.instance() or qt.QApplication([])

_logger = logging.getLogger(__name__)


class _TomwerTask(luigi.Task):
    """
    Tomwer luigi's task base class
    """
    task_namespace = 'esrf.tomwer'

    properties_file = luigi.Parameter(significant=False, default=None)

    node_id = luigi.IntParameter(significant=True)

    workflow_name = luigi.Parameter(significant=True)

    start_day = luigi.DateParameter(default=datetime.date.today(),
                                    significant=False)

    start_time = luigi.DateParameter(default=datetime.date.today(),
                                     significant=True,
                                     visibility=ParameterVisibility.PRIVATE)

    def __init__(self, tomwer_class, *arg, **kwargs):
        luigi.Task.__init__(self, *arg, **kwargs)
        self.tomwer_class = tomwer_class
        self._object_properties = None
        """properties dictionary"""
        self._dependencies = []
        """list of upstream nodes"""
        _logger.info('tomwer class should be: %s' % self.tomwer_class)
        self._load_properties()
        self._instance = self._create_process()
        self._instance.setProperties(self.object_properties)
        if hasattr(self._instance, 'setForceSync'):
            self._instance.setForceSync(True)
        if hasattr(self, 'scan'):
            self.scan_exec = self.scan

    @property
    def object_properties(self):
        return self._object_properties

    @property
    def key(self):
        return '_'.join((self.tomwer_class, str(self.node_id),
                         str(hash(self.properties_file))))

    def _create_process(self):
        """Create the process corresponding to the 'luigi_class'"""
        _splits = self.tomwer_class.split('.')
        module_name = '.'.join(_splits[:-1])
        class_name = _splits[-1]
        _module = importlib.import_module(module_name)
        _class = getattr(_module, class_name)
        _logger.info(
            'create instance of %s from module %s' % (class_name, module_name))
        return _class()

    def _load_properties(self):
        if self.properties_file in (None, ''):
            self._object_properties = {}
            self.properties_file = ''
        elif not os.path.exists(self.properties_file):
            raise ValueError('unable to found the properties_file %s' % self.properties_file)
        else:
            try:
                properties = dictdump.load(self.properties_file)
                self._object_properties = properties['properties']
                if 'node' in properties and 'dependencies' in properties['node']:
                    self._dependencies = properties['node']['dependencies']
                # deal with the case there is only one dependency
                if type(self._dependencies) is int:
                    self._dependencies = [self._dependencies, ]
            except Exception as error:
                _logger.warning("Fail to load configuration from ",
                                self.properties_file,
                                ". Reason is", error)

    def requires(self):
        for dependency in self._dependencies:
            property_folder = os.path.dirname(self.properties_file)
            task_file = self.deduce_property_file(folder=property_folder,
                                                  task_id=int(dependency))
            if os.path.exists(task_file) is False:
                raise ValueError('Fail to found task file %s' % task_file)
            from tomwer.luigi.scheme import Node  # avoid cyclic import
            node = Node._from_dict(property_file=task_file)

            # manage with case output dir is changed by the process.
            # necessary because create the the task before executing them
            self.scan_exec = self._instance.get_output_value('data') or self.scan
            yield node.get_luigi_task(scan=self.scan_exec,
                                      workflow_name=self.workflow_name)

    @staticmethod
    def deduce_property_file(folder, task_id):
        return os.path.join(folder, 'task_%s.ini' % str(task_id))

    def _write_output(self):
        with self.output().open('w') as outfile:
            for output in self._instance.outputs:
                output_name = output.name
                _value = self._instance.get_output_value(output_name)
                outfile.write('{name} | {value}\n'.format(
                                name=output_name,
                                value=_value)
                )

    def read_inputs(self):
        inputs = {}
        for _input in self.input():
            with _input.open('r') as infile:
                lines = infile.read().splitlines()
                for line in lines:
                    name, value = line.split(' | ')
                    inputs[name] = value
        return inputs


class SingleProcessTask(_TomwerTask):
    """
    Task used to launch a `SingleProcess` 
    """
    task_namespace = 'esrf.tomwer'

    scan = luigi.Parameter(significant=True)

    def run(self):
        print('RUN SIMPLE TASK %s for %s' % (self.tomwer_class, self.scan))
        inputs = self.read_inputs()
        # deal with the root node
        if len(inputs) is 0:
            if hasattr(self._instance, 'start'):
                self._instance.start()
            else:
                # TODO: manage directly a scan object from a json file
                self._instance.process(ScanFactory.create_scan_object(self.scan))
        else:
            for input_name, scan_obj in inputs.items():
                if input_name == 'change recons params':
                    _logger.warning('`change recons params` signal is ignore for now')
                    continue
                handler = self._instance.input_handler(name=input_name)
                if handler is None:
                    raise ValueError('No handler define by %s for %s' % (self.tomwer_class, input_name))
                # might be a bottleneck, so might evolve
                scan = scan_obj
                if scan_obj == 'None':
                    scan = self.scan
                tomo_scan = ScanFactory.create_scan_object(scan)
                output = getattr(self._instance, handler)(tomo_scan)
                # TODO: to be done for json compatibility. Will simplify the processing
                # self.dump_output(output)
        # TODO: processEvents should be removed
        while app.hasPendingEvents():
            app.processEvents()

        self._write_output()

    def output(self):
        """For Single process, the output is the scan ID"""
        return luigi.LocalTarget('/tmp/esrf_tomwer/%s_%s' % (self.workflow_name, self.scan_exec))


class EndlessProcessTask(_TomwerTask):
    """
    Task used to launch an `EndlessProcess`
    """
    task_namespace = 'esrf.tomwer'

    control_cycle_duration = luigi.FloatParameter(significant=False, default=0.2)

    def run(self):
        self._instance.start()

        # endless process for now have no inputs.

        while self._continue() is True:
            while app.hasPendingEvents():
                app.processEvents()
            event_is_set = self._instance.scan_found_event.wait(self.control_cycle_duration)

            if event_is_set:
                self._instance.scan_found_event.clear()
                # we need to clean the subworkflow information to re-run them
                if os.path.exists(self.output().fn):
                    self.output().remove()

                for requirement_subworkflow in self._requirements_subworkflow:
                    property_folder = os.path.dirname(self.properties_file)
                    task_file = self.deduce_property_file(
                            folder=property_folder,
                            task_id=int(requirement_subworkflow))
                    if os.path.exists(task_file) is False:
                        raise ValueError(
                                'Fail to found task file %s' % task_file)
                    from tomwer.luigi.scheme import Node  # avoid cyclic import
                    node = Node._from_dict(property_file=task_file)

                    # manage with case output dir is changed by the process.
                    # necessary because create the the task before executing them
                    scan = self._instance.get_output_value('data')
                    # run the subprocess
                    yield node.get_luigi_task(scan=scan,
                                              workflow_name=self.workflow_name)
        # WARNING: output should be write at the end, we cannot create one at
        # each new scan found. So we have to pass them with scan
        self._write_output()

    def _continue(self):
        self._load_properties()
        assert 'requested_status' in self.control
        if self.control['requested_status'] == 'stop':
            self._instance.stop()
            return False
        return True

    def _load_properties(self):
        super()._load_properties()
        prop = dictdump.load(self.properties_file)
        self.control = prop['control']
        if 'node' in prop and 'subworkflow_requirements' in prop['node']:
            self._requirements_subworkflow = prop['node']['subworkflow_requirements']
            if type(self._requirements_subworkflow) is int:
                self._requirements_subworkflow = [self._dependencies, ]
        else:
            self._requirements_subworkflow = []

    def output(self):
        """For Single process, the output is the scan ID"""
        return luigi.LocalTarget('/tmp/esrf_tomwer/%s_%s' % (self.workflow_name, self.task_id))
