# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "07/02/2019"


from .task import EndlessProcessTask
import luigi
import os
import logging
_logger = logging.getLogger(__name__)


class DarkListTask(EndlessProcessTask):

    tomwer_class_name = 'tomwer.core.process.scanlist.ScanList'

    task_namespace = 'esrf.tomwer'

    def __init__(self, *arg, **kwargs):
        EndlessProcessTask.__init__(self, tomwer_class=self.tomwer_class_name, *arg, **kwargs)

    def run(self):
        for scanID in self._instance._scanIDs:
            if not os.path.isdir(scanID):
                _logger.warning('%s is not an existing folder path, skip it' % scanID)
                continue
            for requirement_subworkflow in self._requirements_subworkflow:
                property_folder = os.path.dirname(self.properties_file)
                task_file = self.deduce_property_file(
                        folder=property_folder,
                        task_id=int(requirement_subworkflow))
                if os.path.exists(task_file) is False:
                    raise ValueError(
                            'Fail to found task file %s' % task_file)
                from tomwer.luigi.scheme import Node  # avoid cyclic import
                node = Node._from_dict(property_file=task_file)

                # manage with case output dir is changed by the process.
                # necessary because create the the task before executing them
                # run the subprocess
                yield node.get_luigi_task(scan=scanID,
                                          workflow_name=self.workflow_name)
        self._write_output()

    def output(self):
        """For Single process, the output is the scan ID"""
        return luigi.LocalTarget('/tmp/esrf_tomwer/%s_%s' % (self.workflow_name, self.task_id))

