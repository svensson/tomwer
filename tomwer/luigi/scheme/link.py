# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H.Payno"]
__license__ = "MIT"
__date__ = "29/05/2017"


global next_link_free_id
next_link_free_id = 0


def get_next_link_free_id():
    global next_link_free_id
    _id = next_link_free_id
    next_link_free_id += 1
    return _id


class Link(object):
    """
    
    :param `.Node` source_node:
    :param `.Node` sink_node:
    :param str source_channel:
    :param str sink_channel:
    """

    def __init__(self, source_node, sink_node, source_channel, sink_channel,
                 id=None):
        self.id = id or get_next_link_free_id()
        if isinstance(source_node, int):
            self.source_node_id = source_node
        else:
            self.source_node_id = source_node.id

        if isinstance(sink_node, int):
            self.sink_node_id = sink_node
        else:
            self.sink_node_id = sink_node.id

        self.source_channel = source_channel
        self.sink_channel = sink_channel
