# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H.Payno"]
__license__ = "MIT"
__date__ = "29/05/2017"


import tempfile
import importlib
import os
from luigi.task_register import Register
from silx.io import dictdump
import luigi
import logging

_logger = logging.getLogger(__file__)


global next_node_free_id
next_node_free_id = 0


def get_next_node_free_id():
    global next_node_free_id
    _id = next_node_free_id
    next_node_free_id += 1
    return _id


IGNORED_WIDGETS = [
    'orangecontrib.tomwer.widgets.ImageStackViewerWidget.ImageStackViewerWidget',
]

WIDGETS_TO_CORE_PROC = {
    'orangecontrib.tomwer.widgets.control.DataListOW.DataListOW': 'esrf.tomwer.DarkListTask',
    'orangecontrib.tomwer.widgets.control.DataTransfertOW.DataTransfertOW': 'esrf.tomwer.FolderTransfertTask',
    'orangecontrib.tomwer.widgets.control.TomoDirOW.TomoDirOW': 'esrf.tomwer.DataWatcherTask',
    'orangecontrib.tomwer.widgets.control.DataValidatorOW.DataValidatorOW': 'esrf.tomwer.ScanValidatorTask',
    'orangecontrib.tomwer.widgets.reconstruction.DarkRefAndCopyOW.DarkRefAndCopyOW': 'esrf.tomwer.DarkRefsTask',
    'orangecontrib.tomwer.widgets.reconstruction.FtseriesOW.FtseriesOW': 'esrf.tomwer.FtseriesTask',
}



def check_widget_core_proc():
    pass


class Node(object):
    """
    Node in the `.Scheme`. Will be associated to a tomwer process.
    
    :param int id: unique id of the node.
    :param dict properties: properties of the node
    :param str luigi_task: luigi task associate to this node
    """
    def __init__(self, id=None, properties=None, luigi_task=None,
                 property_file=None):
        self.id = id or get_next_node_free_id()
        """int of the node id"""
        self.properties = properties or {}
        """dict of the node properties"""
        self.upstream_nodes = set()
        """Set of upstream nodes"""
        self.downstream_nodes = set()
        """Set of downstream nodes"""
        self.configuration = ''
        """"""
        self._instance = None
        """process instance"""
        self._qualified_name = luigi_task
        """id of the luigi task or the Orange widget path."""
        self._property_file = property_file
        """file storing the node properties, used to run the luigi Task"""

    @property
    def qualified_name(self):
        return self._qualified_name

    @qualified_name.setter
    def qualified_name(self, name):
        self._qualified_name = name

    @property
    def property_file(self):
        if self._property_file is None:
            self.write_properties_file(tempfile.mkdtemp())
        return self._property_file

    @property
    def endless(self):
        """Return if the tomwer instance is endless or not"""
        if self.luigi_class is None:
            return False
        else:
            return self._tomwer_class_(self.luigi_class).endless_process

    @property
    def inputs(self):
        if self.luigi_class is None:
            return []
        else:
            return self._tomwer_class_(self.luigi_class).inputs

    @property
    def outputs(self):
        if self.luigi_class is None:
            return []
        else:
            return self._tomwer_class_(self.luigi_class).outputs

    def _tomwer_class_(self, luigi_class):
        _class = Register.get_task_cls(luigi_class)
        tomwer_class_name = _class.tomwer_class_name
        sname = tomwer_class_name.rsplit('.')
        assert (len(sname) > 1)
        assert ('tomwer' == sname[0])
        class_name = sname[-1]
        del sname[-1]
        module_name = '.'.join(sname)
        m = importlib.import_module(module_name)
        return getattr(m, class_name)


    @property
    def luigi_class(self):
        """process class name defined in tomwer/core/process"""
        if self.qualified_name in WIDGETS_TO_CORE_PROC:
            return WIDGETS_TO_CORE_PROC[self.qualified_name]
        else:
            return self.qualified_name

    def get_subworkflow_requirements(self):
        """Return the list of subworkflow requirements"""
        res = []
        def all_sub_node_are_endless(node):
            for down_node in node.downstream_nodes:
                if down_node.endless is False:
                    return False
            return True

        for node in self.downstream_nodes:
            if node.isfinal() is 0 or all_sub_node_are_endless(node):
                res.append(node.id)
            else:
                res = res + node.get_subworkflow_requirements()
        return res

    def write_properties_file(self, directory):
        assert os.path.exists(directory)
        self._property_file = os.path.join(directory,
                                           'task_%s.ini' % str(self.id))
        control = {}
        if self.endless is True:
            control['requested_status'] = 'start'

        _dict = {
            'properties': self.properties,
            'node': self._to_dict(),
            'control': control,
        }
        dictdump.dicttoini(ddict=_dict, inifile=self._property_file)
        return self._property_file

    def _to_dict(self):
        # upstream nodes
        upstream_nodes = []
        [upstream_nodes.append(node.id) for node in self.upstream_nodes]

        # inputs
        inputs = []
        # raw test, for now inputs and outputs can only be str
        for _input in self.inputs:
            if _input.type == str:
                inputs.append(_input._asdict())
            else:
                _logger.warning('skip input %s, not managed for now' % _input.type)
        # outputs
        outputs = []
        for _output in self.outputs:
            if _output.type == str:
                outputs.append(_output._asdict())
            else:
                _logger.warning('skip output %s, not managed for now' % _output.type)

        return {
            'id': self.id,
            'luigi_class': self.luigi_class,
            'dependencies': self.get_dependencies(),
            'endless': self.endless,
            'subworkflow_requirements': self.get_subworkflow_requirements(),
            'inputs': inputs,
            'outputs': outputs,
        }

    def get_dependencies(self):
        """
        Return list of dependencies (as node ids). A dependencie is an upstream
        node which is not endless.

        :return: list of node ids
        """
        res = []
        for node in self.upstream_nodes:
            if node.endless is False:
                res.append(node.id)
        return res

    @staticmethod
    def _from_dict(_dict=None, property_file=None):
        """
        
        :param _dict: 
        :param _property_file: 
        :return: 
        """
        if _dict is None:
            assert property_file is not None
            _dict = dictdump.load(property_file)
        assert 'properties' in _dict
        assert 'node' in _dict
        _node_def = _dict['node']
        return Node(id=_node_def['id'],
                    properties=_dict['properties'],
                    luigi_task=_node_def['luigi_class'],
                    property_file=property_file)

    def run(self, name, scan=None, workers=2):
        params = [self.luigi_class,
                  '--workers', str(workers),
                  '--properties-file', self.property_file,
                  '--no-lock',  # ignore if similar process already exists
                  '--node-id', str(self.id),
                  '--workflow-name', name,
                  ]

        if self.endless is True:
            params = params + ['--control-cycle-duration', '0.2']
        else:
            params = params + ['--scan', str(scan), ]
        luigi.interface.run(params)

    def get_luigi_task(self, scan, workflow_name):
        """

        :param scan: for now scan is given only and managed only if tsk is
                     a single process task
        :return: instance of the task, initialized with requested property
        """
        _class = Register.get_task_cls(self.luigi_class)
        return _class(properties_file=self.property_file,
                      node_id=self.id,
                      workflow_name=workflow_name,
                      scan=scan)

    def isfinal(self):
        return len(self.downstream_nodes) is 0
