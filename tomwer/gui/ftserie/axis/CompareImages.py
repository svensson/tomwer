# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["v. Valls", "H. Payno"]
__license__ = "MIT"
__date__ = "26/02/2019"

from silx.gui import qt
from silx.gui.plot import CompareImages as _CompareImages
from silx.gui import icons as silx_icons
from tomwer.gui import icons as tomwer_icons
from silx.gui.plot import tools
import numpy
from silx.gui.colors import Colormap
import weakref
import logging
_logger = logging.getLogger(__name__)


_MODE_RAW_COMPARISON = 'raw A - B'

class CompareImagesToolBar(qt.QToolBar):
    """ToolBar containing specific tools to custom the configuration of a
    :class:`CompareImages` widget

    Use :meth:`setCompareWidget` to connect this toolbar to a specific
    :class:`CompareImages` widget.

    :param Union[qt.QWidget,None] parent: Parent of this widget.
    """
    def __init__(self, parent=None):
        qt.QToolBar.__init__(self, parent)

        self.__compareWidget = None

        menu = qt.QMenu(self)
        self.__visualizationAction = qt.QAction(self)
        self.__visualizationAction.setMenu(menu)
        self.__visualizationAction.setCheckable(False)
        self.addAction(self.__visualizationAction)
        self.__visualizationGroup = qt.QActionGroup(self)
        self.__visualizationGroup.setExclusive(True)
        self.__visualizationGroup.triggered.connect(self.__visualizationModeChanged)

        icon = silx_icons.getQIcon("compare-mode-a")
        action = qt.QAction(icon, "Display the first image only", self)
        action.setIconVisibleInMenu(True)
        action.setCheckable(True)
        action.setShortcut(qt.QKeySequence(qt.Qt.Key_A))
        action.setProperty("mode", CompareImages.VisualizationMode.ONLY_A)
        menu.addAction(action)
        self.__aModeAction = action
        self.__visualizationGroup.addAction(action)

        icon = silx_icons.getQIcon("compare-mode-b")
        action = qt.QAction(icon, "Display the second image only", self)
        action.setIconVisibleInMenu(True)
        action.setCheckable(True)
        action.setShortcut(qt.QKeySequence(qt.Qt.Key_B))
        action.setProperty("mode", CompareImages.VisualizationMode.ONLY_B)
        menu.addAction(action)
        self.__bModeAction = action
        self.__visualizationGroup.addAction(action)

        icon = silx_icons.getQIcon("compare-mode-vline")
        action = qt.QAction(icon, "Vertical compare mode", self)
        action.setIconVisibleInMenu(True)
        action.setCheckable(True)
        action.setShortcut(qt.QKeySequence(qt.Qt.Key_V))
        action.setProperty("mode", CompareImages.VisualizationMode.VERTICAL_LINE)
        menu.addAction(action)
        self.__vlineModeAction = action
        self.__visualizationGroup.addAction(action)

        icon = silx_icons.getQIcon("compare-mode-hline")
        action = qt.QAction(icon, "Horizontal compare mode", self)
        action.setIconVisibleInMenu(True)
        action.setCheckable(True)
        action.setShortcut(qt.QKeySequence(qt.Qt.Key_H))
        action.setProperty("mode", CompareImages.VisualizationMode.HORIZONTAL_LINE)
        menu.addAction(action)
        self.__hlineModeAction = action
        self.__visualizationGroup.addAction(action)

        icon = silx_icons.getQIcon("compare-mode-rb-channel")
        action = qt.QAction(icon, "Blue/red compare mode (additive mode)", self)
        action.setIconVisibleInMenu(True)
        action.setCheckable(True)
        action.setShortcut(qt.QKeySequence(qt.Qt.Key_C))
        action.setProperty("mode", CompareImages.VisualizationMode.COMPOSITE_RED_BLUE_GRAY)
        menu.addAction(action)
        self.__brChannelModeAction = action
        self.__visualizationGroup.addAction(action)

        icon = silx_icons.getQIcon("compare-mode-rbneg-channel")
        action = qt.QAction(icon, "Yellow/cyan compare mode (subtractive mode)", self)
        action.setIconVisibleInMenu(True)
        action.setCheckable(True)
        action.setShortcut(qt.QKeySequence(qt.Qt.Key_W))
        action.setProperty("mode", CompareImages.VisualizationMode.COMPOSITE_RED_BLUE_GRAY_NEG)
        menu.addAction(action)
        self.__ycChannelModeAction = action
        self.__visualizationGroup.addAction(action)

        icon = tomwer_icons.getQIcon("compare_mode_a_minus_b")
        action = qt.QAction(icon, "Raw A - B gray mode", self)
        action.setIconVisibleInMenu(True)
        action.setCheckable(True)
        action.setShortcut(qt.QKeySequence(qt.Qt.Key_W))
        action.setProperty("mode", _MODE_RAW_COMPARISON)
        menu.addAction(action)
        self.__ycChannelModeAction = action
        self.__visualizationGroup.addAction(action)

    def setCompareWidget(self, widget):
        """
        Connect this tool bar to a specific :class:`CompareImages` widget.

        :param Union[None,CompareImages] widget: The widget to connect with.
        """
        compareWidget = self.getCompareWidget()
        if compareWidget is not None:
            compareWidget.sigConfigurationChanged.disconnect(self.__updateSelectedActions)
        compareWidget = widget
        if compareWidget is None:
            self.__compareWidget = None
        else:
            self.__compareWidget = weakref.ref(compareWidget)
        if compareWidget is not None:
            widget.sigConfigurationChanged.connect(self.__updateSelectedActions)
        self.__updateSelectedActions()

    def getCompareWidget(self):
        """Returns the connected widget.

        :rtype: CompareImages
        """
        if self.__compareWidget is None:
            return None
        else:
            return self.__compareWidget()

    def __updateSelectedActions(self):
        """
        Update the state of this tool bar according to the state of the
        connected :class:`CompareImages` widget.
        """
        widget = self.getCompareWidget()
        if widget is None:
            return

        mode = widget.getVisualizationMode()
        action = None
        for a in self.__visualizationGroup.actions():
            actionMode = a.property("mode")
            if mode == actionMode:
                action = a
                break
        old = self.__visualizationGroup.blockSignals(True)
        if action is not None:
            # Check this action
            action.setChecked(True)
        else:
            action = self.__visualizationGroup.checkedAction()
            if action is not None:
                # Uncheck this action
                action.setChecked(False)
        self.__updateVisualizationMenu()
        self.__visualizationGroup.blockSignals(old)

    def __visualizationModeChanged(self, selectedAction):
        """Called when user requesting changes of the visualization mode.
        """
        self.__updateVisualizationMenu()
        widget = self.getCompareWidget()
        if widget is not None:
            mode = selectedAction.property("mode")
            widget.setVisualizationMode(mode)

    def __updateVisualizationMenu(self):
        """Update the state of the action containing visualization menu.
        """
        selectedAction = self.__visualizationGroup.checkedAction()
        if selectedAction is not None:
            self.__visualizationAction.setText(selectedAction.text())
            self.__visualizationAction.setIcon(selectedAction.icon())
            self.__visualizationAction.setToolTip(selectedAction.toolTip())
        else:
            self.__visualizationAction.setText("")
            self.__visualizationAction.setIcon(qt.QIcon())
            self.__visualizationAction.setToolTip("")

    def __alignmentModeChanged(self, selectedAction):
        """Called when user requesting changes of the alignment mode.
        """
        self.__updateAlignmentMenu()
        widget = self.getCompareWidget()
        if widget is not None:
            mode = selectedAction.property("mode")
            widget.setAlignmentMode(mode)

    def __updateAlignmentMenu(self):
        """Update the state of the action containing alignment menu.
        """
        selectedAction = self.__alignmentGroup.checkedAction()
        if selectedAction is not None:
            self.__alignmentAction.setText(selectedAction.text())
            self.__alignmentAction.setIcon(selectedAction.icon())
            self.__alignmentAction.setToolTip(selectedAction.toolTip())
        else:
            self.__alignmentAction.setText("")
            self.__alignmentAction.setIcon(qt.QIcon())
            self.__alignmentAction.setToolTip("")

    def __keypointVisibilityChanged(self):
        """Called when action managing keypoints visibility changes"""
        widget = self.getCompareWidget()
        if widget is not None:
            keypointsVisible = self.__displayKeypoints.isChecked()
            widget.setKeypointsVisible(keypointsVisible)


import silx
if silx._version.MINOR > 10 and silx._version.MAJOR == 0:

    class CompareImages(_CompareImages.CompareImages):

        def _createToolBars(self, plot):
            """Create tool bars displayed by the widget"""
            toolBar = tools.InteractiveModeToolBar(parent=self, plot=plot)
            self._interactiveModeToolBar = toolBar
            toolBar = tools.ImageToolBar(parent=self, plot=plot)
            self._imageToolBar = toolBar
            toolBar = CompareImagesToolBar(self)
            toolBar.setCompareWidget(self)
            self._compareToolBar = toolBar
else:
    # TODO: should be removed when silx.version >= 0.11
    class CompareImages(_CompareImages.CompareImages):

        def _createToolBars(self, plot):
            """Create tool bars displayed by the widget"""
            toolBar = tools.InteractiveModeToolBar(parent=self, plot=plot)
            self._interactiveModeToolBar = toolBar
            toolBar = tools.ImageToolBar(parent=self, plot=plot)
            self._imageToolBar = toolBar
            toolBar = CompareImagesToolBar(self)
            toolBar.setCompareWidget(self)
            self._compareToolBar = toolBar

        def __updateData(self):
            """Compute aligned image when the alignement mode changes.

            This function cache input images which are used when
            vertical/horizontal separators moves.
            """
            raw1, raw2 = self.__raw1, self.__raw2
            if raw1 is None or raw2 is None:
                return

            alignmentMode = self.getAlignmentMode()
            self.__transformation = None

            if alignmentMode == _CompareImages.AlignmentMode.ORIGIN:
                yy = max(raw1.shape[0], raw2.shape[0])
                xx = max(raw1.shape[1], raw2.shape[1])
                size = yy, xx
                data1 = self.__createMarginImage(raw1, size, transparent=True)
                data2 = self.__createMarginImage(raw2, size, transparent=True)
                self.__matching_keypoints = [0.0], [0.0], [1.0]
            elif alignmentMode == _CompareImages.AlignmentMode.CENTER:
                yy = max(raw1.shape[0], raw2.shape[0])
                xx = max(raw1.shape[1], raw2.shape[1])
                size = yy, xx
                data1 = self.__createMarginImage(raw1, size, transparent=True, center=True)
                data2 = self.__createMarginImage(raw2, size, transparent=True, center=True)
                self.__matching_keypoints = ([data1.shape[1] // 2],
                                             [data1.shape[0] // 2],
                                             [1.0])
            elif alignmentMode == _CompareImages.AlignmentMode.STRETCH:
                data1 = raw1
                data2 = self.__rescaleImage(raw2, data1.shape)
                self.__matching_keypoints = ([0, data1.shape[1], data1.shape[1], 0],
                                             [0, 0, data1.shape[0], data1.shape[0]],
                                             [1.0, 1.0, 1.0, 1.0])
            elif alignmentMode == _CompareImages.AlignmentMode.AUTO:
                # TODO: sift implementation do not support RGBA images
                yy = max(raw1.shape[0], raw2.shape[0])
                xx = max(raw1.shape[1], raw2.shape[1])
                size = yy, xx
                data1 = self.__createMarginImage(raw1, size)
                data2 = self.__createMarginImage(raw2, size)
                self.__matching_keypoints = [0.0], [0.0], [1.0]
                try:
                    data1, data2 = self.__createSiftData(data1, data2)
                    if data2 is None:
                        raise ValueError("Unexpected None value")
                except Exception as e:
                    # TODO: Display it on the GUI
                    _logger.error(e)
                    self.__setDefaultAlignmentMode()
                    return
            else:
                assert(False)

            mode = self.getVisualizationMode()
            if mode == _CompareImages.VisualizationMode.COMPOSITE_RED_BLUE_GRAY_NEG:
                data1 = self.__composeImage(data1, data2, mode)
                data2 = numpy.empty((0, 0))
            elif mode == _CompareImages.VisualizationMode.COMPOSITE_RED_BLUE_GRAY:
                data1 = self.__composeImage(data1, data2, mode)
                data2 = numpy.empty((0, 0))
            elif mode == _CompareImages.VisualizationMode.ONLY_A:
                data2 = numpy.empty((0, 0))
            elif mode == _CompareImages.VisualizationMode.ONLY_B:
                data1 = numpy.empty((0, 0))
            elif mode == _MODE_RAW_COMPARISON:
                dtype = data1.dtype
                data1 = data1.astype(numpy.float64) - data2.astype(numpy.float64)
                data1 = data1.astype(data1.dtype)
                data2 = numpy.empty((0, 0))

            self.__data1, self.__data2 = data1, data2
            self.__plot.addImage(data1, z=0, legend="image1", resetzoom=False)
            self.__plot.addImage(data2, z=0, legend="image2", resetzoom=False)
            self.__image1 = self.__plot.getImage("image1")
            self.__image2 = self.__plot.getImage("image2")
            self.__updateKeyPoints()

            # Set the separator into the middle
            if self.__previousSeparatorPosition is None:
                value = self.__data1.shape[1] // 2
                self.__vline.setPosition(value, 0)
                value = self.__data1.shape[0] // 2
                self.__hline.setPosition(0, value)
            self.__updateSeparators()

            # Avoid to change the colormap range when the separator is moving
            # TODO: The colormap histogram will still be wrong
            mode1 = self.__getImageMode(data1)
            mode2 = self.__getImageMode(data2)
            if mode1 == "intensity" and mode1 == mode2:
                if self.__data1.size == 0:
                    vmin = self.__data2.min()
                    vmax = self.__data2.max()
                elif self.__data2.size == 0:
                    vmin = self.__data1.min()
                    vmax = self.__data1.max()
                else:
                    vmin = min(self.__data1.min(), self.__data2.min())
                    vmax = max(self.__data1.max(), self.__data2.max())
                colormap = Colormap(vmin=vmin, vmax=vmax)
                self.__image1.setColormap(colormap)
                self.__image2.setColormap(colormap)
