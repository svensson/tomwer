# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["C. Nemoz", "H. Payno"]
__license__ = "MIT"
__date__ = "25/02/2019"


from silx.gui import qt
from tomwer.core.log import TomwerLogger
from tomwer.core.utils import image
import functools
import numpy
import enum
from silx.gui.plot.CompareImages import VisualizationMode
from tomwer.core.scan.scanbase import ScanBase
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.synctools.ftseries import _QAxisRP, QReconsParams
from tomwer.core.process.reconstruction.reconsparam.axis import AxisMode
from .ftaxiswidget import FtAxisTabWidget
import scipy.signal
from silx.io.url import DataUrl
from silx.io import utils as silx_io_utils
from .CompareImages import CompareImages
from tomwer.gui.utils.buttons import PadlockButton


_logger = TomwerLogger(__name__)


class AxisWindow(qt.QMainWindow):
    """
    QMainWindow for defining the rotation axis

    .. snapshotqt:: img/AxisWindow.png

        from tomwer.gui.ftserie.axis import AxisWindow
        from tomwer.synctools.ftseries import QReconsParams
        import scipy.misc
        import scipy.ndimage
        
        recons_params = QReconsParams()
        widget = AxisWindow(recons_params)
        imgA = scipy.misc.ascent()
        imgB = scipy.ndimage.affine_transform(imgA, (1, 1), offset=(0, 10))

        widget.setImages(imgA=imgA, imgB=imgB, flipB=False)
        widget.show()
    """

    sigComputationRequested = qt.Signal()
    """signal emitted when a computation is requested"""

    sigApply = qt.Signal()
    """signal emitted when the axis reconstruction parameters are validated"""

    sigAxisEditionLocked = qt.Signal(bool)
    """Signal emitted when the status of the reconstruction parameters edition
    change"""

    def __init__(self, axis, parent=None):
        qt.QMainWindow.__init__(self, parent)
        if isinstance(axis, _QAxisRP):
            raise ValueError('For now we cannot accept _QAxisRP instances '
                             'because ftaxiswidget require some field of FT '
                             'struct')
        elif isinstance(axis, QReconsParams):
            # self._recons_params = axis.axis  # TODO: in the future it should deal with
            # _QAxisRP only instead of QReconsParam
            self.__recons_params = axis
        else:
            raise TypeError('axis should be an instance of _QAxisRP or '
                            'QReconsParam')

        self._imgA = None
        self._imgB = None
        self._shiftedImgA = None
        self._flipB = True
        """Option if we want to flip the image B"""

        self.setWindowFlags(qt.Qt.Widget)
        self._mainWidget = CompareImages(parent=self)
        _mode = VisualizationMode.COMPOSITE_RED_BLUE_GRAY
        self._mainWidget.setVisualizationMode(_mode)
        self.setCentralWidget(self._mainWidget)

        self._dockWidgetCtrl = qt.QDockWidget(parent=self)
        self._dockWidgetCtrl.layout().setContentsMargins(0, 0, 0, 0)
        self._dockWidgetCtrl.setFeatures(qt.QDockWidget.DockWidgetMovable)
        self._controlWidget = _AxisGUIControl(parent=self)
        self._dockWidgetCtrl.setWidget(self._controlWidget)
        self.addDockWidget(qt.Qt.RightDockWidgetArea, self._dockWidgetCtrl)

        # add buttons
        self._buttons = qt.QWidget(parent=self)
        self._buttons.setLayout(qt.QHBoxLayout())

        self._lockBut = PadlockButton(parent=self)
        self._buttons.layout().addWidget(self._lockBut)
        self._lockLabel = qt.QLabel('lock axis position', parent=self)
        self._buttons.layout().addWidget(self._lockLabel)

        spacer = qt.QWidget(self)
        spacer.setSizePolicy(qt.QSizePolicy.Expanding, qt.QSizePolicy.Minimum)
        self._buttons.layout().addWidget(spacer)

        self._computeBut = qt.QPushButton('compute', parent=self)
        self._buttons.layout().addWidget(self._computeBut)
        style = qt.QApplication.style()
        applyIcon = style.standardIcon(qt.QStyle.SP_DialogApplyButton)
        self._applyBut = qt.QPushButton(applyIcon, 'validate', parent=self)
        self._buttons.layout().addWidget(self._applyBut)

        self._controlWidget.layout().addWidget(self._buttons)

        # signal / slot connection
        self._controlWidget.sigShiftChanged.connect(self._updateShift)
        self._controlWidget.sigRoiChanged.connect(self._updateShift)
        self._controlWidget.sigAuto.connect(self._updateAuto)
        self._lockBut.toggled.connect(self.setLocked)
        self._computeBut.pressed.connect(self._computationRequested)
        self._applyBut.pressed.connect(self._validated)

        # expose API
        self.getXShift = self._controlWidget.getXShift
        self.getYShift = self._controlWidget.getYShift
        self.getROI = self._controlWidget.getROI
        self._setShift = self._controlWidget.setShift
        self.setXShift = self._controlWidget.setXShift
        self.setYShift = self._controlWidget.setYShift
        self.getShiftStep = self._controlWidget.getShiftStep
        self.setShiftStep = self._controlWidget.setShiftStep
        self.getAxis = self._controlWidget.getAxis
        self.getMode = self._controlWidget.getMode
        self.isLocked = self._lockBut.isLocked

        # adapt gui to the axis value
        self.setReconsParams(axis=self.__recons_params)

    def _computationRequested(self):
        self.sigComputationRequested.emit()

    def setLocked(self, locked):
        old = self.blockSignals(True)
        self._controlWidget.setLocked(locked)
        self._lockBut.setChecked(locked)
        self.blockSignals(old)
        self.sigAxisEditionLocked.emit(locked)

    def _validated(self):
        """callback when the validate button is activated"""
        self.sigApply.emit()

    def setReconsParams(self, axis):
        """
        
        :param AxisRP axis: axis to edit
        :return: 
        """
        # assert isinstance(axis, _QAxisRP)
        assert isinstance(axis, QReconsParams)
        self.resetShift()
        self._controlWidget.setAxis(axis)

    def setScan(self, scan):
        """
        Update the interface concerning the given scan. Try to display the
        radios for angle 0 and 180.

        :param scan: scan for which we want the axis updated.
        :type scan: Union[str, tomwer.core.scan.ScanBase]
        """
        _scan = scan
        if type(scan) is str:
            try:
                _scan = ScanFactory.create_scan_object(scan)
            except ValueError:
                raise ValueError('Fail to discover a valid scan in %s' % scan)
        elif not isinstance(_scan, ScanBase):
                raise ValueError('type of %s (%s) is invalid, scan should be a '
                                 'file/dir path or an instance of ScanBase' % (scan, type(scan)))
        assert isinstance(_scan, ScanBase)
        # TODO: get evolution and select two from there to be displayed.
        radio_a, radio_b = _scan.getDefaultRadiosForAxisCalc()
        if radio_a is not None and radio_b is not None:
            self.setImages(imgA=radio_a, imgB=radio_b)
        else:
            _logger.error('fail to find radios for angle 0 and 180. Unable to '
                          'update axis gui')

    def setImages(self, imgA, imgB, flipB=True):
        """

        :warning: does not reset the shift when change images

        :param imgA: first image to compare. Will be the one shifted
        :param imgB: second image to compare
        :param bool flipB: True if the image B has to be flipped
        """
        assert imgA is not None
        assert imgB is not None
        _imgA = imgA
        _imgB = imgB

        def loadDataUrl(_url):
            try:
                _img = silx_io_utils.get_data(_url)
            except:
                _logger.error('Fail to load data from %s' % _img)
                return None
            else:
                return _img

        if isinstance(_imgA, DataUrl):
            _imgA = loadDataUrl(_imgA)
        if isinstance(_imgB, DataUrl):
            _imgB = loadDataUrl(_imgB)

        if _imgA.shape != _imgB.shape:
            _logger.error("The two provided images have incoherent shapes "
                          "(%s vs %s)" % (_imgA.shape, _imgB.shape))
        elif _imgA.ndim is not 2:
            _logger.error("Image shape are not 2 dimensional")
        else:
            self._flipB = flipB
            self._imgA = _imgA
            self._imgB = _imgB
            self._controlWidget._roiControl.setLimits(width=self._imgA.shape[1],
                                                      height=self._imgA.shape[0])
            self._updateShift(xShift=self.getXShift(), yShift=self.getYShift())

    def _updateShift(self, xShift=None, yShift=None):
        if self._imgA is None or self._imgB is None:
            return
        xShift = xShift or self.getXShift()
        yShift = yShift or self.getYShift()

        _imgA, _imgB = self._getRawImages()
        # apply shift
        if xShift == 0.0 and yShift == 0.0:
            self._shiftedImgA = _imgA
        else:
            try:
                self._shiftedImgA = image.shift_img(data=_imgA,
                                                    dx=self.getXShift(),
                                                    dy=self.getYShift())
            except ValueError as e:
                _logger.warning(e)

        self._mainWidget.setData(image1=self._shiftedImgA, image2=_imgB)

    def _getRawImages(self):

        def selectROI(data, width, height):
            x_center = data.shape[1] // 2
            y_center = data.shape[0] // 2
            x_min = x_center - width // 2
            x_max = x_center + width // 2
            y_min = y_center - height // 2
            y_max = y_center + height // 2
            return data[y_min:y_max, x_min:x_max]

        # get images and apply ROI if any
        _roi = self.getROI()
        _imgA = self._imgA
        _imgB = self._imgB
        if self._flipB is True:
            _imgB = numpy.fliplr(_imgB)
        if _roi is not None:
            assert type(_roi) is tuple
            _imgA = selectROI(_imgA, width=_roi[0], height=_roi[1])
            _imgB = selectROI(_imgB, width=_roi[0], height=_roi[1])
        return _imgA, _imgB

    def _updateAuto(self):
        _imgA, _imgB = self._getRawImages()
        correlation = scipy.signal.correlate2d(in1=_imgA, in2=_imgB)
        y, x = numpy.unravel_index(numpy.argmax(correlation), correlation.shape)
        self._setShift(x=x, y=y)

    def resetShift(self):
        self._controlWidget.blockSignals(True)
        self._controlWidget.reset()
        self._controlWidget.blockSignals(False)
        if self._imgA and self._imgB:
            self.setImages(imgA=self._imgA, imgB=self._imgB)


class _AxisGUIControl(qt.QWidget):
    """
    Widget to define the shift to apply on an image
    """

    sigShiftChanged = qt.Signal(float, float)
    """Signal emitted when requested shift changed. Parameter is x, y"""

    def __init__(self, parent, axis=None):
        qt.QWidget.__init__(self, parent)
        self._xShift = 0
        self._yShift = 0

        self._axis = None
        # TODO: in the future we should manage an instance of _QAxisRP
        # instead of a QReconsParams but for now ftaxiswidget is editing some
        # paramters of 'FT' struct (2019)
        # self.setReconsParams(axis or _QAxisRP())
        self._axis = None

        self.setLayout(qt.QVBoxLayout())

        self._main_widget_recons_params = QReconsParams(empty=True)
        self._main_widget_recons_params.copy(self._axis)
        self._manualSelectionWidget = _AxisManualSelection(parent=self,
                                                           shift_mode=ShiftMode.x_only)
        self._manualSelectionWidget.layout().setContentsMargins(0, 0, 0, 0)

        self._displacementSelector = self._manualSelectionWidget._displacementSelector
        self._shiftControl = self._manualSelectionWidget._shiftControl
        self._roiControl = self._manualSelectionWidget._roiControl

        self._mainWidget = FtAxisTabWidget(parent=self,
                                           manual_sel_widget=self._manualSelectionWidget,
                                           reconsparams=self._main_widget_recons_params)

        self.layout().addWidget(self._mainWidget)

        # signal / slot connection
        callback = functools.partial(self._incrementShift, 'left')
        self._shiftControl.sigShiftLeft.connect(callback)
        callback = functools.partial(self._incrementShift, 'right')
        self._shiftControl.sigShiftRight.connect(callback)
        callback = functools.partial(self._incrementShift, 'top')
        self._shiftControl.sigShiftTop.connect(callback)
        callback = functools.partial(self._incrementShift, 'bottom')
        self._shiftControl.sigShiftBottom.connect(callback)
        self._shiftControl.sigReset.connect(self._resetShift)
        self._shiftControl.sigShiftChanged.connect(self.setShift)

        # expose API
        self.getShiftStep = self._displacementSelector.getShiftStep
        self.setShiftStep = self._displacementSelector.setShiftStep
        self.sigRoiChanged = self._roiControl.sigRoiChanged
        self.sigAuto = self._shiftControl.sigAuto
        self.getROI = self._roiControl.getROI
        self.getMode = self._mainWidget.getMode

        self.setAxis(axis or QReconsParams())

    def setAxis(self, axis):
        assert isinstance(axis, QReconsParams)
        if self._axis:
            self._axis.sigChanged.disconnect(self._updateAxisView)
        assert isinstance(axis.axis, _QAxisRP)
        self._axis = axis.axis
        self._recons_params = axis
        self._mainWidget.setReconsParams(self._recons_params)
        self._updateAxisView()
        self._axis.sigChanged.connect(self._updateAxisView)

    def _updateAxisView(self):
        self._axis.blockSignals(True)
        if self._axis.cor_position is not None:
            self.setXShift(self._axis.position_value)
        self._axis.blockSignals(False)

        self._manualSelectionWidget.setVisible(self._axis.mode is AxisMode.manual)

    def getAxis(self):
        return self._axis

    def _incrementShift(self, direction):
        assert direction in ('left', 'right', 'top', 'bottom')
        if direction == 'left':
            self.setXShift(self._xShift - self.getShiftStep())
        elif direction == 'right':
            self.setXShift(self._xShift + self.getShiftStep())
        elif direction == 'top':
            self.setYShift(self._yShift + self.getShiftStep())
        else:
            self.setYShift(self._yShift - self.getShiftStep())

        self._shiftControl._updateShiftInfo(x=self._xShift, y=self._yShift)
        self.sigShiftChanged.emit(self._xShift, self._yShift)

    def _resetShift(self):
        self._xShift = 0
        self._yShift = 0
        self._shiftControl._updateShiftInfo(x=self._xShift, y=self._yShift)
        self.sigShiftChanged.emit(self._xShift, self._yShift)

    def getXShift(self):
        return self._xShift

    def getYShift(self):
        return self._yShift

    def setXShift(self, x):
        if x == self._xShift:
            return
        self._xShift = x
        if self._axis:
            self._axis.blockSignals(True)
            self._axis.position_value = x
            self._axis.blockSignals(False)
        self._shiftControl._updateShiftInfo(x=self._xShift, y=self._yShift)
        self.sigShiftChanged.emit(self._xShift, self._yShift)

    def setYShift(self, y):
        if y == self._yShift:
            return
        self._yShift = y
        self._shiftControl._updateShiftInfo(x=self._xShift, y=self._yShift)
        self.sigShiftChanged.emit(self._xShift, self._yShift)

    def setShift(self, x, y):
        if x == self._xShift and y == self._yShift:
            return
        self._xShift = x
        self._yShift = y
        self._shiftControl._updateShiftInfo(x=self._xShift, y=self._yShift)
        self.sigShiftChanged.emit(self._xShift, self._yShift)

    def reset(self):
        self._xShift = 0
        self._yShift = 0
        self.sigShiftChanged.emit(self._xShift, self._yShift)

    def setLocked(self, locked):
        self._mainWidget.setEnabled(not locked)


class _AxisManualSelection(qt.QWidget):
    def __init__(self, parent, shift_mode):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())
        self._displacementSelector = _DisplacementSelector(parent=self)
        self.layout().addWidget(self._displacementSelector)

        self._shiftControl = _ShiftControl(parent=self, shift_mode=shift_mode)
        self.layout().addWidget(self._shiftControl)

        self._roiControl = _ROIControl(parent=self)
        self.layout().addWidget(self._roiControl)


class _ROIControl(qt.QGroupBox):
    """
    Widget used to define the ROI on images to compare
    """
    sigRoiChanged = qt.Signal(object)
    """Signal emitted when the ROI changed"""

    def __init__(self, parent):
        qt.QGroupBox.__init__(self, 'ROI selection', parent)
        self.setLayout(qt.QVBoxLayout())

        self._buttonGrp = qt.QButtonGroup(parent=self)
        self._buttonGrp.setExclusive(True)

        self._roiWidget = qt.QWidget(parent=self)
        self._roiWidget.setLayout(qt.QHBoxLayout())
        self._roiWidget.layout().setContentsMargins(0, 0, 0, 0)
        self._roiButton = qt.QRadioButton('ROI', parent=self._roiWidget)
        self._roiWidget.layout().addWidget(self._roiButton)
        self._buttonGrp.addButton(self._roiButton)
        self._roiDefinition = _ROIDefinition(parent=self)
        self._roiWidget.layout().addWidget(self._roiDefinition)
        self.layout().addWidget(self._roiWidget)

        self._fullImgButton = qt.QRadioButton('full image', parent=self)
        self._buttonGrp.addButton(self._fullImgButton)
        self.layout().addWidget(self._fullImgButton)

        # connect signal / Slot
        self._roiButton.toggled.connect(self._roiDefinition.setEnabled)

        # expose API
        self.sigRoiChanged = self._roiDefinition.sigRoiChanged
        self.getROI = self._roiDefinition.getROI
        self.setLimits = self._roiDefinition.setLimits

        # setup for full image
        self._roiButton.setChecked(True)


class _ROIDefinition(qt.QWidget):
    """
    Widget used to define ROI width and height.

    :note: emit ROI == None if setDisabled
    """
    sigRoiChanged = qt.Signal(object)
    """Signal emitted when the ROI changed"""

    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QGridLayout())

        self.layout().addWidget(qt.QLabel('width', parent=self), 0, 0)
        self._widthSB = qt.QSpinBox(parent=self)
        self._widthSB.setSingleStep(2)
        self._widthSB.setMaximum(10000)
        self._widthSB.setSuffix(" px")
        self.layout().addWidget(self._widthSB, 0, 1)
        self.layout().addWidget(qt.QLabel('height', parent=self), 1, 0)
        self._heightSB = qt.QSpinBox(parent=self)
        self._heightSB.setSingleStep(2)
        self._heightSB.setSuffix(" px")
        self._heightSB.setMaximum(10000)
        self.layout().addWidget(self._heightSB, 1, 1)

        # Signal / Slot connection
        self._widthSB.editingFinished.connect(self.__roiChanged)
        self._heightSB.editingFinished.connect(self.__roiChanged)

    def __roiChanged(self, *args, **kwargs):
        self.sigRoiChanged.emit(self.getROI())

    def setLimits(self, width, height):
        """

        :param int x: width maximum value
        :param int height: height maximum value
        """
        for spinButton in (self._widthSB, self._heightSB):
            spinButton.blockSignals(True)
        assert type(width) is int
        assert type(height) is int
        valueChanged = False
        if self._widthSB.value() > width:
            self._widthSB.setValue(width)
            valueChanged = True
        if self._heightSB.value() > height:
            self._heightSB.setValue(height)
            valueChanged = True

        # if this is the first limit definition, propose default width and
        # height
        if self._widthSB.value() is 0:
            self._widthSB.setValue(min(256, width))
            valueChanged = True
        if self._heightSB.value() is 0:
            self._heightSB.setValue(min(256, height))
            valueChanged = True

        # define minimum / maximum
        self._widthSB.setRange(1, width)
        self._heightSB.setRange(1, height)
        for spinButton in (self._widthSB, self._heightSB):
            spinButton.blockSignals(False)
        if valueChanged is True:
            self.__roiChanged()

    def getROI(self):
        """

        :return: (width, height) or None
        :rtype: Union[None, tuple]
        """
        if self.isEnabled():
            return (self._widthSB.value(), self._heightSB.value())
        else:
            return None

    def setEnabled(self, *arg, **kwargs):
        qt.QWidget.setEnabled(self, *arg, **kwargs)
        self.__roiChanged()


@enum.unique
class ShiftMode(enum.Enum):
    x_only = 0
    y_only = 1
    x_and_y = 2


class _ShiftControl(qt.QWidget):
    """
    Widget to control the shift step we want to apply
    """
    sigShiftLeft = qt.Signal()
    """Signal emitted when the left button is activated"""
    sigShiftRight = qt.Signal()
    """Signal emitted when the right button is activated"""
    sigShiftTop = qt.Signal()
    """Signal emitted when the top button is activated"""
    sigShiftBottom = qt.Signal()
    """Signal emitted when the bottom button is activated"""
    sigReset = qt.Signal()
    """Signal emitted when the reset button is activated"""
    sigAuto = qt.Signal()
    """Signal emitted when the auto button is activated"""

    def __init__(self, parent, shift_mode):
        """

        :param parent: qt.QWidget
        :param ShiftMode shift_mode: what are the shift we want to control
        """
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QGridLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)

        self._leftButton = qt.QPushButton('left', parent=self)
        self.layout().addWidget(self._leftButton, 1, 0)

        self._rightButton = qt.QPushButton('right', parent=self)
        self.layout().addWidget(self._rightButton, 1, 3)

        self._topButton = qt.QPushButton('top', parent=self)
        self.layout().addWidget(self._topButton, 0, 1)

        self._bottomButton = qt.QPushButton('bottom', parent=self)
        self.layout().addWidget(self._bottomButton, 2, 1)

        self._resetButton = qt.QPushButton('reset', parent=self)
        self.layout().addWidget(self._resetButton, 3, 2, 3, 4)

        self._autoButton = qt.QPushButton('auto', parent=self)
        self.layout().addWidget(self._autoButton, 3, 0, 3, 2)
        self._autoButton.hide()

        self._shiftInfo = _ShiftInformation(parent=self)
        self.layout().addWidget(self._shiftInfo, 1, 1)
        self._shiftInfo._updateShiftInfo(x=0.0, y=0.0)

        # Signal / Slot connection
        self._leftButton.pressed.connect(self.sigShiftLeft.emit)
        self._rightButton.pressed.connect(self.sigShiftRight.emit)
        self._topButton.pressed.connect(self.sigShiftTop.emit)
        self._bottomButton.pressed.connect(self.sigShiftBottom.emit)
        self._resetButton.pressed.connect(self.sigReset.emit)
        self._autoButton.pressed.connect(self.sigAuto.emit)

        # expose API
        self._updateShiftInfo = self._shiftInfo._updateShiftInfo
        self.sigShiftChanged = self._shiftInfo.sigShiftChanged

        self.setShiftMode(shift_mode)

    def setShiftMode(self, shift_mode):
        show_x_shift = shift_mode in (ShiftMode.x_only, ShiftMode.x_and_y)
        show_y_shift = shift_mode in (ShiftMode.y_only, ShiftMode.x_and_y)
        self._leftButton.setVisible(show_x_shift)
        self._rightButton.setVisible(show_x_shift)
        self._topButton.setVisible(show_y_shift)
        self._bottomButton.setVisible(show_y_shift)
        self._shiftInfo._xLE.setVisible(show_x_shift)
        self._shiftInfo._xLabel.setVisible(show_x_shift)
        self._shiftInfo._yLE.setVisible(show_y_shift)
        self._shiftInfo._yLabel.setVisible(show_y_shift)


class _ShiftInformation(qt.QWidget):
    """
    Widget displaying information about the current x and y shift.
    Both x shift and y shift are editable.
    """
    class _ShiftLineEdit(qt.QLineEdit):
        def __init__(self, *args, **kwargs):
            qt.QLineEdit.__init__(self, *args, **kwargs)
            validator = qt.QDoubleValidator(parent=self, decimals=2)
            self.setValidator(validator)

        def sizeHint(self):
            return qt.QSize(40, 10)

    sigShiftChanged = qt.Signal(float, float)
    """Signal emitted ony when xLE and yLE edition is finished"""

    def __init__(self, parent):
        qt.QWidget.__init__(self, parent)
        self.setLayout(qt.QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)

        self._xLabel = qt.QLabel('x=', parent=self)
        self.layout().addWidget(self._xLabel)
        self._xLE = _ShiftInformation._ShiftLineEdit('', parent=self)
        self.layout().addWidget(self._xLE)

        self._yLabel = qt.QLabel('y=', parent=self)
        self.layout().addWidget(self._yLabel)
        self._yLE = _ShiftInformation._ShiftLineEdit('', parent=self)
        self.layout().addWidget(self._yLE)

        # connect Signal / Slot
        self._xLE.editingFinished.connect(self._shiftChanged)
        self._yLE.editingFinished.connect(self._shiftChanged)

    def _updateShiftInfo(self, x, y):
        self.blockSignals(True)
        self._xLE.setText('{0:.1f}'.format(x))
        self._yLE.setText('{0:.1f}'.format(y))
        self.blockSignals(False)

    def _shiftChanged(self, *args, **kwargs):
        self.sigShiftChanged.emit(float(self._xLE.text()), float(self._yLE.text()))


class _DisplacementSelector(qt.QGroupBox):
    """
    Group box to define the displacement step value
    """
    def __init__(self, parent):
        qt.QGroupBox.__init__(self, 'shift step', parent)
        self.setLayout(qt.QVBoxLayout())
        self._buttonGrp = qt.QButtonGroup(parent=self)
        self._buttonGrp.setExclusive(True)

        self._rawButton = qt.QRadioButton('Raw (1 pixel)', parent=self)
        self.layout().addWidget(self._rawButton)
        self._buttonGrp.addButton(self._rawButton)

        self._fineButton = qt.QRadioButton('Fine (0.1 pixel)', parent=self)
        self.layout().addWidget(self._fineButton)
        self._buttonGrp.addButton(self._fineButton)

        self._manualWidget = qt.QWidget(parent=self)
        self._manualWidget.setLayout(qt.QHBoxLayout())
        self._manualWidget.layout().setContentsMargins(0, 0, 0, 0)
        self._manualWidget.layout().setSpacing(0)
        self._manualButton = qt.QRadioButton('Manual', parent=self._manualWidget)
        self._manualWidget.layout().addWidget(self._manualButton)
        self._manualLE = qt.QLineEdit('0.5', parent=self._manualWidget)
        validator = qt.QDoubleValidator(parent=self._manualLE, decimals=2)
        validator.setBottom(0.0)
        self._manualLE.setValidator(validator)
        self._manualWidget.layout().addWidget(self._manualLE)

        self.layout().addWidget(self._manualWidget)
        self._manualLE.setEnabled(False)
        self._buttonGrp.addButton(self._manualButton)

        self._rawButton.setChecked(True)

        # signal / slot connection
        self._manualButton.toggled.connect(self._manualLE.setEnabled)

    def getShiftStep(self):
        """

        :return: displacement shift defined
        :rtype: float
        """
        if self._rawButton.isChecked():
            return 1.0
        elif self._fineButton.isChecked():
            return 0.1
        else:
            return float(self._manualLE.text())

    def setShiftStep(self, value):
        """

        :param float value: shift step
        """
        assert type(value) is float
        self._manualButton.setChecked(True)
        self._manualLE.setText(str(value))
