# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H.Payno"]
__license__ = "MIT"
__date__ = "29/05/2017"

import unittest
from . import test_axis
from . import test_datalist
from . import test_datawatcher
from . import test_h5editor
from . import test_inputwidget
from . import test_lamino
from . import test_reconsparamset_editor
from . import test_recpyhstwidget
from . import test_scanselector
from . import test_scanvalidator
from . import test_stacks
from . import test_syncreconsparam


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTests([
        test_axis.suite(),
        test_datalist.suite(),
        test_datawatcher.suite(),
        test_h5editor.suite(),
        test_inputwidget.suite(),
        test_lamino.suite(),
        test_reconsparamset_editor.suite(),
        test_recpyhstwidget.suite(),
        test_scanvalidator.suite(),
        test_scanselector.suite(),
        test_stacks.suite(),
        test_syncreconsparam.suite(),
    ])
    return test_suite
