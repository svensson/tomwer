# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "27/02/2019"


from tomwer.gui.ftserie.axis import AxisWindow
from tomwer.test.utils import skip_gui_test
from tomwer.test.utils import UtilsTest
from tomwer.core.process.reconstruction.reconsparam.axis import AxisMode
from tomwer.synctools.ftseries import QReconsParams, _QAxisRP
from silx.gui.utils.testutils import TestCaseQt
from silx.io.url import DataUrl
from silx.gui import qt
import numpy.random
import unittest
import os


@unittest.skipIf(skip_gui_test(), reason='skip gui test')
class TestWindowAxis(TestCaseQt):
    """Test that the datalist widget work correctly"""
    def setUp(self):
        self.recons_params = QReconsParams()
        self._window = AxisWindow(axis=self.recons_params)
        self.scan_path = UtilsTest.getDataset("D2_H2_T2_h_")

    def tearDown(self):
        self._window.setAttribute(qt.Qt.WA_DeleteOnClose)
        self._window.close()

    def testSetImagesUrl(self):
        """Test the setImages function"""
        self.assertTrue(self._window._mainWidget.getPlot().getActiveImage() is None)
        scheme = 'fabio'
        imgA_url = DataUrl(file_path=os.path.join(self.scan_path, list(os.listdir(self.scan_path))[0]),
                           scheme=scheme)
        imgB_url = DataUrl(file_path=os.path.join(self.scan_path, list(os.listdir(self.scan_path))[1]),
                           scheme=scheme)
        self._window.setImages(imgA=imgA_url, imgB=imgB_url)
        self.qapp.processEvents()
        self.assertTrue(self._window._mainWidget.getPlot().getActiveImage() is not None)

    def testSetImagesNumpyArray(self):
        """Test the setImages function"""
        self.assertTrue(self._window._mainWidget.getPlot().getActiveImage() is None)
        imgA = numpy.random.random((100, 100))
        imgB = numpy.random.random((100, 100))
        self._window.setImages(imgA=imgA, imgB=imgB)
        self.qapp.processEvents()
        self.assertTrue(self._window._mainWidget.getPlot().getActiveImage() is not None)

    def testSetScan(self):
        """Test the setScan function"""
        self.assertTrue(self._window._mainWidget.getPlot().getActiveImage() is None)
        self._window.setScan(self.scan_path)
        self.assertTrue(self._window._mainWidget.getPlot().getActiveImage() is not None)

    def testShiftButtons(self):
        """Test that the 'left', 'right', ... buttons and the shift steps are
        correctly working"""
        imgA = numpy.random.random((100, 100))
        imgB = numpy.random.random((100, 100))
        self._window.setImages(imgA=imgA, imgB=imgB)
        self.qapp.processEvents()
        self.assertTrue(self._window.getXShift() == 0)
        self.assertTrue(self._window.getYShift() == 0)
        self.assertTrue(self._window.getShiftStep() == 1.0)
        self._window._controlWidget._shiftControl._leftButton.click()
        self.qapp.processEvents()
        self.assertTrue(self._window.getXShift() == -1)
        self.assertTrue(self._window.getYShift() == 0)
        self._window._controlWidget._shiftControl._rightButton.click()
        self.qapp.processEvents()
        self.assertTrue(self._window.getXShift() == 0)
        self.assertTrue(self._window.getYShift() == 0)
        self._window._controlWidget._displacementSelector._fineButton.toggle()
        self.qapp.processEvents()
        self.assertTrue(self._window.getShiftStep() == 0.1)
        self._window._controlWidget._shiftControl._topButton.click()
        self.qapp.processEvents()
        self.assertTrue(self._window.getXShift() == 0)
        self.assertTrue(self._window.getYShift() == 0.1)
        self._window.setShiftStep(0.2)
        self.assertTrue(self._window.getShiftStep() == 0.2)
        self._window._controlWidget._shiftControl._bottomButton.click()
        self.qapp.processEvents()
        self.assertTrue(self._window.getXShift() == 0)
        self.assertTrue(self._window.getYShift() == -0.1)

    def testAxisObjectGlobal(self):
        """Test that the GUI change according to the Axis object"""
        axis_obj = _QAxisRP()
        recons_params = QReconsParams()
        recons_params.axis = axis_obj
        self._window.setReconsParams(recons_params)
        self.qapp.processEvents()
        self.assertTrue(self._window.getMode() is AxisMode.global_)
        # TODO: change the mode to see if the object is modify

    def testAxisObjectManual(self):
        """Test that the GUI change according to the Axis object"""
        axis_obj = _QAxisRP()
        axis_obj.mode = AxisMode.manual
        axis_obj.position_value = -6.0
        # note : for now only the xshift is managed !
        recons_params = QReconsParams()
        recons_params.axis = axis_obj
        self._window.setReconsParams(recons_params)
        self.assertTrue(self._window.getMode() is AxisMode.manual)
        self.assertTrue(self._window.getXShift() == -6.0)
        self.assertTrue(self._window.getYShift() == 0.0)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestWindowAxis, ):
        test_suite.addTest(
            unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")


