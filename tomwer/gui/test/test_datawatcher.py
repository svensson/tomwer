# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "09/06/2017"

import os
import shutil
import tempfile
import time
import unittest

from silx.gui.utils.testutils import TestCaseQt
from silx.gui import qt

from tomwer.core.process import datawatcher
from tomwer.core.process.datawatcher.datawatcherprocess import \
    _DataWatcherProcess
from tomwer.gui.datawatcher import DataWatcherWidget
from tomwer.gui.datawatcher.datawatcherobserver import _QDataWatcherFixObserver
from tomwer.gui.datawatcher.datawatcherobserver import _QDataWatcherObserver
from tomwer.gui.datawatcher.datawatcherobserver import _QOngoingObservation
from tomwer.synctools.rsyncmanager import RSyncManager
from tomwer.test.utils import UtilsTest
from tomwer.test.utils import skip_gui_test


@unittest.skipIf(skip_gui_test(), reason='skip gui test')
class TestTomoDirObserver(TestCaseQt):
    """
    Simple test to make sure the timeout of tomodir is working properly
    """

    FOLDER_WITH_DATA = 4

    FOLDER_WITHOUT_DATA = 3

    def setUp(self):
        TestCaseQt.setUp(self)
        self.observeFolder = tempfile.mkdtemp()

        self.dataSetID = 'test10'
        dataDir = UtilsTest.getDataset(self.dataSetID)
        for iFolder in range(self.FOLDER_WITH_DATA):
            shutil.copytree(dataDir,
                            os.path.join(self.observeFolder, 'f' + str(iFolder), self.dataSetID))

        for iFolder in range(self.FOLDER_WITHOUT_DATA):
            os.makedirs(os.path.join(self.observeFolder, 'empty' + str(iFolder)))

        # observer
        self.observer = _QDataWatcherObserver(headDir=self.observeFolder,
                                              startByOldest=False,
                                              obsMethod=datawatcher.DET_END_XML,
                                              srcPattern=None,
                                              destPattern=None,
                                              observationClass=_QOngoingObservation)

    def tearDown(self):
        self.observer.wait()
        self.observer = None
        if os.path.isdir(self.observeFolder):
            shutil.rmtree(self.observeFolder)
        TestCaseQt.tearDown(self)

    def testRun(self):
        self.observer.run()
        self.assertTrue(len(self.observer.observations) is self.FOLDER_WITH_DATA)
        self.observer.waitForObservationFinished(2)
        for dir, thread in self.observer.observations.dict.items():
            self.assertTrue(thread.status == 'acquisition ended')

    def testRunAcquisitionNotFinished(self):
        for iFolder in range(self.FOLDER_WITH_DATA):
            folder = os.path.join(self.observeFolder, 'f' + str(iFolder), self.dataSetID)
            xmlFile = os.path.join(folder, self.dataSetID + '.xml')
            os.remove(xmlFile)

        self.observer.run()
        self.assertTrue(
            len(self.observer.observations) is self.FOLDER_WITH_DATA)
        self.observer.waitForObservationFinished(2)
        for dir, thread in self.observer.observations.dict.items():
            self.assertTrue(thread.status == 'waiting for acquisition ending')

        f1 = list(self.observer.observations.dict.keys())[0]
        self.observer.cancelObservation(f1)

        self.assertTrue(
            len(self.observer.observations) is self.FOLDER_WITH_DATA - 1)


@unittest.skipIf(skip_gui_test(), reason='skip gui test')
class TestTomoDirFixObserver(TestCaseQt):
    def setUp(self):
        TestCaseQt.setUp(self)

        self.observeFolder = tempfile.mkdtemp()
        self.dataSetID = 'test10'
        dataDir = UtilsTest.getDataset(self.dataSetID)
        shutil.copytree(dataDir,
                        os.path.join(self.observeFolder, self.dataSetID))

        self.thread = _QDataWatcherFixObserver(
            scanID=os.path.join(self.observeFolder, self.dataSetID),
            obsMethod=datawatcher.DET_END_XML,
            srcPattern=None,
            destPattern=None,
            patternObs=None,
            observationRegistry=None)

    def tearDown(self):
        self.thread.wait()
        self.thread = None
        shutil.rmtree(self.observeFolder)
        TestCaseQt.tearDown(self)

    def testRunOk(self):
        self.thread.start()
        self.thread.wait(5)
        self.assertTrue(self.thread.status == 'acquisition ended',
                        msg="status is %s" % self.thread.status)

    def testFolderNotExisting(self):
        self.thread.setDirToObserve('toto/pluto')
        self.thread.start()
        self.thread.wait(5)
        self.assertTrue(self.thread.status == 'failure',
                        msg="status is %s" % self.thread.status)

    def testRunWaitingXML(self):
        fileXML = os.path.join(self.thread.dataWatcherProcess.RootDir,
                               self.dataSetID + '.xml')
        assert os.path.isfile(fileXML)
        os.remove(fileXML)
        self.thread.start()
        self.thread.wait(5)
        self.assertTrue(self.thread.status == 'waiting for acquisition ending',
                        msg="status is %s" % self.thread.status)


class _ObservationCounter(qt.QObject):
    """Basically simulate the next box which has to receive some signal to
    process"""
    def __init__(self):
        qt.QObject.__init__(self)
        self.scansCounted = 0
        self.scansID = []

    def add(self, scanID):
        self.scansCounted = self.scansCounted + 1
        if scanID.path not in self.scansID:
            self.scansID.append(scanID.path)

    def getReceivedScan(self):
        return self.scansCounted

    def getDifferentScanReceived(self):
        return len(self.scansID)

    def clear(self):
        self.scansID = []
        self.scansCounted = 0


@unittest.skipIf(skip_gui_test(), reason='skip gui test')
class TestTomoDir(TestCaseQt):
    """
    Make sur the TomoDir process is valid
    """

    WAIT_BTW_LOOP = 0.1

    def setUp(self):
        TestCaseQt.setUp(self)
        # create a folder with an unfinished acquisition
        self.observeFolder = tempfile.mkdtemp()

        self.dataSetIDs = 'test10', 'test01'
        for scanID in self.dataSetIDs:
            dataDir = UtilsTest.getDataset(scanID)
            shutil.copytree(dataDir,
                            os.path.join(self.observeFolder, scanID))

        self.observationCounter = _ObservationCounter()

        # tomodir
        self.tomodir = DataWatcherWidget()
        self.tomodir.setFolderObserved(self.observeFolder)
        self.tomodir.obsMethod = datawatcher.DET_END_XML
        self.tomodir.setSrcAndDestPattern(srcPattern=None, destPattern=None)
        self.tomodir.setWaitTimeBtwLoop(self.WAIT_BTW_LOOP)
        self.tomodir.sigScanReady.connect(self.observationCounter.add)

    def tearDown(self):
        self.tomodir.stop()
        self.tomodir = None
        if os.path.isdir(self.observeFolder):
            shutil.rmtree(self.observeFolder)
        TestCaseQt.tearDown(self)

    def testInfinity(self):
        """
        Test that if we ar ein the infinity mode and two acquisitions are
        existing we will found only those two observations once and that the
        tomodir object is still observing
        """
        self.observationCounter.clear()

        self.tomodir.start()
        self.qapp.processEvents()
        time.sleep(self.WAIT_BTW_LOOP * 2.0)  # make sure no multiple signal are emitted
        self.qapp.processEvents()
        time.sleep(0.4)
        self.qapp.processEvents()
        self.tomodir.waitForObservationFinished()
        self.assertTrue(self.tomodir.observationThread is not None)
        self.assertTrue(len(self.tomodir.lastFoundScans) == 2)
        self.assertTrue(self.observationCounter.getReceivedScan() == 2)
        self.assertTrue(self.observationCounter.getDifferentScanReceived() == 2)
        self.tomodir.stop()

    def testPatternFileObsMethod(self):
        """
        Test the DET_END_USER_ENTRY observation method.
        Will look for a file pattern given by the user in the scan directory
        """
        def tryPattern(pattern, filePrefix, fileSuffix):
            self.tomodir.stop()
            self.observationCounter.clear()

            self.tomodir.setObsMethod((datawatcher.DET_END_USER_ENTRY,
                                       {'pattern': pattern}))

            tempfile.mkdtemp(prefix=filePrefix,
                             suffix=fileSuffix,
                             dir=os.path.join(self.observeFolder, 'test10'))

            self.assertTrue(
                self.observationCounter.getDifferentScanReceived() is 0)

            self.tomodir.start()
            self.tomodir.waitForObservationFinished()
            self.qapp.processEvents()
            time.sleep(0.4)
            self.qapp.processEvents()

            return self.observationCounter.getDifferentScanReceived() is 1

        self.assertTrue(tryPattern(pattern='*tatayoyo*.cfg',
                                   filePrefix='tatayoyo',
                                   fileSuffix='.cfg'))

        self.assertFalse(tryPattern(pattern='*tatayoyo*.cfg',
                                    filePrefix='tatayoyo',
                                    fileSuffix=''))

        self.assertFalse(tryPattern(pattern='tatayoyo.cfg',
                                    filePrefix='tatayoyo',
                                    fileSuffix='.cfg'))


@unittest.skipIf(skip_gui_test(), reason='skip gui test')
class TestTomodirAborted(TestCaseQt):
    """Test behavior of the TomoDir when some acquisition are aborted"""
    def setUp(self):
        TestCaseQt.setUp(self)
        # create a folder with an unfinished acquisition
        self.observeFolder = tempfile.mkdtemp()

        # tomodir
        self.tomodir = DataWatcherWidget()
        self.tomodir.setFolderObserved(self.observeFolder)
        self.tomodir.obsMethod = datawatcher.DET_END_XML
        self.tomodir.setSrcAndDestPattern(srcPattern=None, destPattern=None)

        self.dataDir01 = UtilsTest.getDataset('test01')
        self.dataDir10 = UtilsTest.getDataset('test10')

        RSyncManager().setForceSync(False)

    def tearDown(self):
        self.tomodir.stop()
        self.tomodir = None
        if os.path.exists(self.observeFolder):
            shutil.rmtree(self.observeFolder)
        TestCaseQt.tearDown(self)

    def testAbortedFindNext(self):
        """Make sure if an acquisition is aborted then we will skip it
        and that the NEXT acquisition will be found"""
        self.tomodir.setWaitTimeBtwLoop(0.1)
        self.assertTrue(len(self.tomodir.getIgnoredFolders()) is 0)

        dirTest01 = os.path.join(self.observeFolder, 'test01')
        shutil.copytree(self.dataDir01, dirTest01)
        # add the `abort file`
        open(os.path.join(dirTest01, 'test01' + _DataWatcherProcess.ABORT_FILE),
             'a')
        self.tomodir.start()
        self.tomodir.observationThread.wait()
        self.qapp.processEvents()
        self.assertTrue(len(self.tomodir.getIgnoredFolders()) is 0)
        self.assertTrue(self.tomodir.isObserving is True)
        dirTest10 = os.path.join(self.observeFolder, 'test10')
        shutil.copytree(self.dataDir10, dirTest10)
        while self.qapp.hasPendingEvents():
            self.qapp.processEvents()

        time.sleep(0.4)
        while self.qapp.hasPendingEvents():
            self.qapp.processEvents()
        self.assertTrue(len(self.tomodir.lastFoundScans) is 1)
        self.assertTrue(dirTest10 in self.tomodir.lastFoundScans)

    def testAbortedFindPrevious(self):
        """Make sure if an acquisition is aborted then we will skip it
        and that the PREVIOUS acquisition will be found"""
        self.tomodir.setWaitTimeBtwLoop(0.1)
        self.assertTrue(len(self.tomodir.getIgnoredFolders()) is 0)

        dirTest10 = os.path.join(self.observeFolder, 'test10')
        shutil.copytree(self.dataDir10, dirTest10)
        dirTest01 = os.path.join(self.observeFolder, 'test01')
        shutil.copytree(self.dataDir01, dirTest01)
        # add the `abort file`
        open(os.path.join(dirTest01, 'test01' + _DataWatcherProcess.ABORT_FILE),
             'a')
        self.tomodir.start()
        self.tomodir.observationThread.wait()
        self.qapp.processEvents()
        self.assertTrue(self.tomodir.isObserving is True)
        time.sleep(0.2)
        self.qapp.processEvents()
        time.sleep(0.2)
        self.qapp.processEvents()
        self.assertTrue(len(self.tomodir.lastFoundScans) is 1)
        self.assertTrue(dirTest10 in self.tomodir.lastFoundScans)

    def testAbortedWhenWaitingFor(self):
        """
        Make sure will skip the scan if aborted even if observation started
        """
        self.tomodir.setWaitTimeBtwLoop(0.1)
        self.assertTrue(len(self.tomodir.getIgnoredFolders()) is 0)

        dirTest01 = os.path.join(self.observeFolder, 'test01')
        shutil.copytree(self.dataDir01, dirTest01)
        self.tomodir.start()
        # add the `abort file`
        open(os.path.join(dirTest01, 'test01' + _DataWatcherProcess.ABORT_FILE),
             'a')
        self.tomodir.observationThread.wait()
        self.qapp.processEvents()
        self.assertTrue(len(self.tomodir.getIgnoredFolders()) is 0)
        self.assertTrue(self.tomodir.isObserving is True)
        dirTest10 = os.path.join(self.observeFolder, 'test10')
        shutil.copytree(self.dataDir10, dirTest10)
        self.qapp.processEvents()
        time.sleep(0.2)
        self.qapp.processEvents()
        self.assertTrue(len(self.tomodir.lastFoundScans) is 1)
        self.assertTrue(dirTest10 in self.tomodir.lastFoundScans)

    def testAbortedRemovingFolder(self):
        """Make sure the aborted folder is removed after found by tomodir"""
        RSyncManager().setForceSync(True)
        self.tomodir.setWaitTimeBtwLoop(0.1)
        dirTest01 = os.path.join(self.observeFolder, 'test01')
        shutil.copytree(self.dataDir01, dirTest01)
        # add the `abort file`
        open(os.path.join(dirTest01, 'test01' + _DataWatcherProcess.ABORT_FILE),
             'a')
        self.tomodir.start()
        self.tomodir.observationThread.wait()
        assert dirTest01 in self.tomodir.observationThread.observations.dict
        obs = self.tomodir.observationThread.observations.dict[dirTest01]
        obs.wait()
        self.assertFalse(os.path.exists(dirTest01))


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestTomoDirObserver, TestTomoDir, TestTomoDirFixObserver,
               TestTomodirAborted):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
