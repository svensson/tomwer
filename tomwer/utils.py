# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/01/2018"


try:
    from silx.gui import qt
except ImportError:
    raise ImportError("Can't found silx modules")
from tomwer.gui import icons
try:
    import tomwer._version
    versionLoaded = True
except ImportError:
    versionLoaded = False


def getMainSplashScreen():
    pixmap = icons.getQPixmap('tomwer')
    splash = qt.QSplashScreen(pixmap)
    splash.show()
    splash.raise_()
    _version = '???'
    if versionLoaded is True:
        _version = tomwer._version.version
    text = 'version ' + str(_version)
    splash.showMessage(text, qt.Qt.AlignLeft | qt.Qt.AlignBottom, qt.Qt.white)
    return splash


def addHTMLLine(txt):
    return "<li>" + txt + "</li>"
