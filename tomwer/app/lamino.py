#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import logging
import os
import signal
import sys

from silx.gui import qt

from tomwer.core.process.reconstruction.lamino.tofu import \
    tofuLaminoReconstruction
from tomwer.gui.reconstruction.lamino.tofu import TofuWindow
from tomwer.utils import getMainSplashScreen

_logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.INFO)
logging.getLogger().setLevel(logging.INFO)


def getinputinfo():
    return "tomwer lamino [scanDir]"


def sigintHandler(*args):
    """Handler for the SIGINT signal."""
    qt.QApplication.quit()


def main(argv):
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'scan_path',
        help='Data file to show (h5 file, edf files, spec files)')
    parser.add_argument(
        '--debug',
        dest="debug",
        action="store_true",
        default=False,
        help='Set logging system in debug mode')
    parser.add_argument(
        '--test-cmd',
        dest="test_cmd",
        action="store_true",
        default=False,
        help='Will not apply the tofu process but only display the command used'
             'to call tofu')
    options = parser.parse_args(argv[1:])

    global app  # QApplication must be global to avoid seg fault on quit
    app = qt.QApplication.instance() or qt.QApplication([])

    qt.QLocale.setDefault(qt.QLocale.c())

    signal.signal(signal.SIGINT, sigintHandler)
    sys.excepthook = qt.exceptionHandler

    timer = qt.QTimer()
    timer.start(500)
    # Application have to wake up Python interpreter, else SIGINT is not
    # catched
    timer.timeout.connect(lambda: None)

    splash = getMainSplashScreen()
    options.scan_path = os.path.abspath(options.scan_path.rstrip(os.path.sep))
    dialog = ToFuDialog(parent=None, scanID=options.scan_path)
    dialog._setTestCmd(options.test_cmd)
    splash.finish(dialog)
    dialog.exec_()


class ToFuDialog(qt.QDialog):
    """Simple dialog to launch the lamino reconstruction using tofu"""
    def __init__(self, parent, scanID):
        qt.QDialog.__init__(self, parent)
        self._scanID = scanID
        self._testCmd = False

        self.setLayout(qt.QVBoxLayout())
        self.setWindowTitle('Lamino reconstruction using tofu')

        types = qt.QDialogButtonBox.Ok | qt.QDialogButtonBox.Cancel
        self._buttons = qt.QDialogButtonBox(parent=self)
        self._buttons.setStandardButtons(types)
        self.tofuWidget = TofuWindow(parent=self)
        self.tofuWidget.loadFromScan(self._scanID)
        self.layout().addWidget(self.tofuWidget)
        self.layout().addWidget(self._buttons)

        self._buttons.button(
            qt.QDialogButtonBox.Ok).clicked.connect(self.accept)
        self._buttons.button(
            qt.QDialogButtonBox.Cancel).clicked.connect(self.reject)

    def accept(self):
        recons_param = self.tofuWidget.getParameters()
        add_options = self.tofuWidget.getAdditionalOptions()
        delete_existing = self.tofuWidget.removeOutputDir()
        tofuLaminoReconstruction(scan_id=self._scanID,
                                 recons_param=recons_param,
                                 additional_options=add_options,
                                 delete_existing=delete_existing,
                                 exec_cmd=(not self._testCmd))

    def _setTestCmd(self, forTest):
        self._testCmd = forTest


if __name__ == '__main__':
    main(sys.argv)
