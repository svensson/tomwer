#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
from silx.gui import qt
import logging
import signal
import functools
from tomwer.utils import getMainSplashScreen
from tomwer.gui.ftserie.axis.axis import AxisWindow
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.synctools.ftseries import QReconsParams
from tomwer.core.process.reconstruction.axis import AxisProcess


class _AxisProcessGUI(AxisProcess, AxisWindow):
    def __init__(self, scan):
        recons_params = QReconsParams()
        AxisProcess.__init__(self, recons_params=recons_params.axis)
        AxisWindow.__init__(self, axis=recons_params)
        self._lockBut.hide()
        self._lockLabel.hide()
        self._applyBut.hide()

        # connect Signal / Slot
        callback = functools.partial(self.process, scan)
        self.sigComputationRequested.connect(callback)


def main(argv):
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'scan_path',
        help='Data file to show (h5 file, edf files, spec files)')
    parser.add_argument(
        '--debug',
        dest="debug",
        action="store_true",
        default=False,
        help='Set logging system in debug mode')

    options = parser.parse_args(argv[1:])

    if options.debug:
        logging.root.setLevel(logging.DEBUG)

    global app  # QApplication must be global to avoid seg fault on quit
    app = qt.QApplication.instance() or qt.QApplication([])

    qt.QLocale.setDefault(qt.QLocale.c())
    signal.signal(signal.SIGINT, sigintHandler)
    sys.excepthook = qt.exceptionHandler

    timer = qt.QTimer()
    timer.start(500)
    # Application have to wake up Python interpreter, else SIGINT is not
    # catched
    timer.timeout.connect(lambda: None)

    splash = getMainSplashScreen()
    options.scan_path = options.scan_path.rstrip(os.path.sep)

    scan = ScanFactory.create_scan_object(scan_path=options.scan_path)

    window = _AxisProcessGUI(scan=scan)
    splash.finish(window)
    window.show()
    app.exec_()


def getinputinfo():
    return "tomwer axis [scanDir]"


def sigintHandler(*args):
    """Handler for the SIGINT signal."""
    qt.QApplication.quit()



if __name__ == '__main__':
    main(sys.argv)
