import luigi
import time
import argparse
import sys
from tomwer.luigi.scheme import scheme_load
from tomwer.luigi import exec_


def getinputinfo():
    return "tomwer toluigi workflowdef.ows"


def main(argv):
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'file_path',
        help='Path to the file containing the workflow description (.ows, .xml)')
    options = parser.parse_args(argv[1:])

    # TODO: add workers
    start_time = time.time()
    scheme = scheme_load(_file=options.file_path)
    exec_(scheme, scan=None, name='test workflow conversion')

    print("--- %s seconds ---" % (time.time() - start_time))
    exit(0)

if __name__ == "__main__":
    main(sys.argv)
