#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import sys
import os
from silx.gui import qt
import argparse
from tomwer.utils import getMainSplashScreen
from tomwer.gui.stacks import RadioStack
import signal

logging.basicConfig()
_logger = logging.getLogger(__name__)


def getinputinfo():
    return "tomwer radiostack [scanDir]"


def addFolderAndSubFolder(stack, path):
    stack.addLeafScan(path)
    for f in os.listdir(path):
        _path = os.path.join(path, f)
        if os.path.isdir(_path) is True:
            addFolderAndSubFolder(stack, _path)


def sigintHandler(*args):
    """Handler for the SIGINT signal."""
    qt.QApplication.quit()


def main(argv):
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'scan_path',
        help='Root dir to browse and to extract all radio under it.')
    parser.add_argument(
        '--debug',
        dest="debug",
        action="store_true",
        default=False,
        help='Set logging system in debug mode')

    options = parser.parse_args(argv[1:])

    global app  # QApplication must be global to avoid seg fault on quit
    app = qt.QApplication.instance() or qt.QApplication([])

    qt.QLocale.setDefault(qt.QLocale.c())
    signal.signal(signal.SIGINT, sigintHandler)
    sys.excepthook = qt.exceptionHandler
    timer = qt.QTimer()
    timer.start(500)
    # Application have to wake up Python interpreter, else SIGINT is not
    # catched
    timer.timeout.connect(lambda: None)

    splash = getMainSplashScreen()
    widget = RadioStack()
    options.scan_path = options.scan_path.rstrip(os.path.sep)
    addFolderAndSubFolder(stack=widget, path=options.scan_path)

    splash.finish(widget)
    widget.show()
    app.exec_()


if __name__ == '__main__':
    main(sys.argv)
