# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/06/2018"


import glob
import os
import subprocess
import sys
from collections import OrderedDict

from tomwer.core.log import TomwerLogger
from tomwer.core.process.baseprocess import SingleProcess, _input_desc, \
    _output_desc
from tomwer.core.process.reconstruction.darkref.settings import DARKHST_PREFIX, \
    REFHST_PREFIX
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.core.scan.scanbase import TomoBase
from tomwer.core.utils import getDARK_N
from tomwer.core.utils.char import PSI_CHAR

logger = TomwerLogger(__name__)


SLICE_STACK_TYPE = 'slice stack'
ROTATION_CENTER_TYPE = 'rotation center'
LAMINO_ANGLE_TYPE = 'lamino angle'
PSI_ANGLE_TYPE = ' '.join((PSI_CHAR, 'angle'))

SCAN_TYPES = OrderedDict((
        (SLICE_STACK_TYPE, 'z'),
        (ROTATION_CENTER_TYPE, 'center-position-x'),
        (LAMINO_ANGLE_TYPE, 'axis-angle-x'),
        (PSI_ANGLE_TYPE, 'axis-angle-y'),
))

SCAN_TYPES_I = {}
for s_type, value in SCAN_TYPES.items():
    SCAN_TYPES_I[value] = s_type


def _retrieveOptionsCommand(scan_id, recons_param, additional_opts):
    """Return the options command under the style '--name optValue' from the
    ToFuReconstructionParam and a string of additional options"""
    assert isinstance(recons_param, dict)
    options = ['']

    # deal with option that will can be None
    for opt in ('retry-timeout', 'retries', 'reduction-mode', 'dark-scale',
                'darks', 'slices-per-device', 'slice-memory-coeff',
                'number', 'volume-angle-x', 'volume-angle-y',
                'volume-angle-z', 'output', 'center-position-x',
                'center-position-z', 'axis-angle-x', 'axis-angle-y',
                'z-parameter', 'retrieval-method', 'energy', 'pixel-size',
                'propagation-distance', 'regularization-rate',
                'thresholding-rate', 'flats', 'flats2'):
        if recons_param[opt] is not None:
            # special case for option that can contain Unix filename pattern matching (*)
            if opt in ('darks', 'flats', 'flats2'):
                options = options + [' '.join([opt, '"' + str(recons_param[opt] + '"')])]
            else:
                options = options + [' '.join([opt, str(recons_param[opt])])]

    # deal with specific case of the scan
    if scan_id is not None:
        concert_radio_path = os.path.join(scan_id, 'radios')
        proj_file_fn = concert_radio_path + os.sep + 'frame_*.tif'
        if (os.path.exists(concert_radio_path) and
                len(glob.glob(proj_file_fn)) > 0):
            options = options + ['projections "' + proj_file_fn + '"']
        else:
            options = options + ['projections "' + scan_id + os.sep + os.path.basename(scan_id) + '*.edf"']

    # deal with region
    assert 'region' in recons_param
    if recons_param['region'] is not None:
        assert type(recons_param['region']) in (list, tuple)
        assert len(recons_param['region']) is 3
        options = options + ['region=%s,%s,%s' % recons_param['region']]

    # deal with overallangle wich need a '-' for obscure reason
    assert type(recons_param['overall-angle']) is float
    options = options + ['overall-angle ' + str(-1.0*recons_param['overall-angle'])]

    # deal with x and y region
    for region in ('x-region', 'y-region'):
        value = recons_param[region]
        if value is not None:
            value = str(value).replace(' ', '')
            value = value.lstrip('(').rstrip(')')
            options = options + ['='.join((region, value))]

    # deal with option that will be defined only if set to true
    for opt in ('verbose', 'dry-run', 'absorptivity'):
        assert opt in recons_param
        if recons_param[opt] is True:
            options.append(opt)

    return (' --'.join(options) + ' ' + additional_opts)


def tofuLaminoReconstruction(scan_id, recons_param, additional_options,
                             delete_existing, exec_cmd=True):
    """Process a reconstruction for lamino using tofu

    :param str scan_id: path of the scan to reconstruct
    :param dict recons_param: parameters for the reconstruction
    :param str additional_options: additional options to be add at the tofu reco
                                  call.
    :param bool delete_existing: if True then remove output dir if given
    :param bool exec_cmd: if True, will run reconstruction, otherwise will only
                          display the reconstruction parameters.
    """
    if exec_cmd is True:
        if hasTofu() is False:
            logger.error('Cannot launch tofu reconstruction because '
                         'tofu is not installed.')
            return
        if (delete_existing is True and 'output' in recons_param):
            logger.info('remove output dir: %s' % recons_param['output'])
            outputdir = recons_param['output']
            if not outputdir.endswith(os.sep):
                outputdir += os.sep
            for _file in glob.glob(outputdir + '*.tif'):
                try:
                    os.remove(_file)
                except Exception as e:
                    logger.error(e)

    options = _retrieveOptionsCommand(scan_id=scan_id, recons_param=recons_param,
                                      additional_opts=additional_options)

    try:
        logger.info('launch command : ' + "tofu reco" + options)
        if exec_cmd is False:
            print("tofu reco" + options)
        else:
            subprocess.call("tofu reco " + options, shell=True)
        return True
    except OSError:
        return False


def hasTofu():
    """
    
    :return: true if the os knows the tofu command
    """
    if not sys.platform.startswith('linux'):
        return False
    try:
        subprocess.call(["tofu", "--version"], stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE)
        return True
    except:
        return False


class LaminoReconstruction(SingleProcess):
    """
    Process to launch the lamino reconstruction

    TODO: setting parameters should be group with the gui/lamino/tofu/xxx files
    But this binding is probably overkilled as we should use the tofu python
    binding for it. But no python3 version at the moment.
    """

    inputs = [
        _input_desc(name='data', type=TomoBase, doc='scan path', handler='process'),
    ]

    outputs = [
        _output_desc(name='data', type=TomoBase, doc='scan path')
    ]

    DEFAULT_RECONSTRUCTION_PARAMETERS_VALS = {
        'region': None,
        'retry-timeout': None,
        'retries': None,
        'reduction-mode': None,
        'dark-scale': None,
        'darks': None,
        'slices-per-device': None,
        'slice-memory-coeff': 0.8,
        'number': None,
        'volume-angle-x': None,
        'volume-angle-y': None,
        'volume-angle-z': None,
        'output': None,
        'center-position-x': None,
        'center-position-z': None,
        'axis-angle-x':None,
        'axis-angle-y': None,
        'z-parameter': None,
        'retrieval-method': None,
        'energy': None,
        'pixel-size': None,
        'propagation-distance':None,
        'regularization-rate': None,
        'thresholding-rate':None,
        'flats':None,
        'flats2': None,
        'overall-angle': 360.0,
        'x-region': None,
        'y-region': None,
        'verbose': False,
        'dry-run': False,
        'absorptivity': False,
    }

    def __init__(self):
        SingleProcess.__init__(self)
        self._reconsparams = self.DEFAULT_RECONSTRUCTION_PARAMETERS_VALS
        self._additional_options = ''
        self._delete_existing = False
        self._dry_run = False

    @property
    def reconstruction_parameters(self):
        return self._reconsparams

    @reconstruction_parameters.setter
    def reconstruction_parameters(self, params):
        self._reconsparams = params

    @property
    def additional_options(self):
        return self._additional_options

    @additional_options.setter
    def additional_options(self, opts):
        self._additional_options = opts

    @property
    def delete_existing(self):
        return self._delete_existing

    @delete_existing.setter
    def delete_existing(self, _delete):
        assert type(_delete) is bool
        self._delete_existing = _delete

    @property
    def dry_run(self):
        return self._dry_run

    @dry_run.setter
    def dry_run(self, dryrun):
        assert type(dryrun) is bool
        self._dry_run = dryrun

    def process(self, scan):
        if type(scan) is dict:
            _scan = ScanFactory.create_scan_object_frm_dict(scan)
        else:
            _scan = scan
        assert isinstance(_scan, TomoBase)
        if _scan is None and 'projections' not in self._reconsparams:
            return None
        else:
            recons_params = self._deduce_missing_params(_scan.path)
            # copy reocns_params to the TomoBase object
            _scan.lamino_recons_params = recons_params
            res =  tofuLaminoReconstruction(scan_id=_scan.path,
                                            recons_param=recons_params,
                                            additional_options=self.additional_options,
                                            delete_existing=self.delete_existing,
                                            exec_cmd=(not self.dry_run))
            if res is False:
                logger.error('Reconstruction of', _scan.path, 'failed')
            if self._return_dict:
                return _scan.to_dict()
            else:
                return _scan

    def _deduce_missing_params(self, scan):
        """
        Deduce some parameters if possible and if not defined (like darks...)

        :param scan: 
        :return: dict
        """
        result = self.reconstruction_parameters
        if result['darks'] is None:
            result['darks'] = getDark(scan)

        if result['flats'] is None or result['flats2'] is None:
            flats, flats2 = getFlats(scan)
            if result['flats'] is None:
                result['flats'] = flats
            if result['flats2'] is None:
                result['flats2'] = flats2
        if result['dark-scale']:
            result['dark-scale'] = getDARK_N(scan)
        return result


def getDark(scan):
    """Return darks as a string for tofu from the scan path"""
    concert_projection_files = os.path.join(scan, 'radios')
    concert_darks_path = os.path.join(scan, 'darks')
    if (os.path.exists(concert_darks_path) and
            os.path.exists(concert_projection_files) and
                len(glob.glob(concert_darks_path + os.sep + 'frame_*.tif')) > 0):
        return concert_darks_path + os.sep + 'frame_*.tif'
    else:
        files = os.listdir(scan)
        for thFile in (DARKHST_PREFIX, 'dark.edf', 'darkend0000.edf',
                       'darkend000.edf', 'darkHST.edf'):
            if thFile in files:
                return os.path.join(scan, thFile)
        return None


def getFlats(scan):
    """
    Return flats as a string for tofu from the scan path

    :return: tuple (flats, secondFlats)
    """
    def treatRawRef(rawRefFiles):
        ns = set()
        for _file in rawRefFiles:
            name = _file.rstrip('.edf')
            ns.add(name.split('_')[-1])
        ns = sorted(ns)
        if len(ns) is 0:
            return None, None
        elif len(ns) is 1:
            return os.path.join(scan, 'ref*_' + ns[0] + '.edf')
        else:
            return (os.path.join(scan, 'ref*_' + ns[0] + '.edf'),
                    os.path.join(scan, 'ref*_' + ns[-1] + '.edf'))

    # deal with concert files
    concert_projection_files = os.path.join(scan, 'radios')
    concert_flats_path = os.path.join(scan, 'flats')
    if os.path.exists(concert_flats_path) and os.path.exists(concert_projection_files):
        flats = None
        flats2 = None
        if len(glob.glob(concert_flats_path + os.sep + 'frame_*.tif')) > 0:
            flats = concert_flats_path + os.sep + 'frame_*.tif'
        concert_flats2_path = os.path.join(scan, 'flats_2')
        if len(glob.glob(concert_flats2_path + os.sep + 'frame_*.tif')) > 0:
            flats2 = concert_flats2_path + os.sep + 'frame_*.tif'
        if flats is not None or flats2 is not None:
            return flats, flats2

    # deal with classical files
    files = os.listdir(scan)
    refHSTFiles = []  # files starting with refHST (treated by darkRef)
    rawRefFiles = []  # files starting with ref only (raw ref files)
    for _file in files:
        if _file.startswith('ref') and _file.endswith('.edf'):
            if _file.startswith(REFHST_PREFIX):
                refHSTFiles.append(_file)
            else:
                rawRefFiles.append(_file)

    refHSTFiles = sorted(refHSTFiles)
    if len(refHSTFiles) is 0:
        return treatRawRef(rawRefFiles)
    elif len(refHSTFiles) is 1:
        return os.path.join(scan, refHSTFiles[0]), None
    else:
        logger.warning('Found more than two refHST files')
        return (os.path.join(scan, refHSTFiles[0]),
                os.path.join(scan, refHSTFiles[-1]))
