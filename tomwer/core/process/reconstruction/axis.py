# coding: utf-8
###########################################################################
# Copyright (C) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################

"""contain the AxisProcess
"""

__authors__ = ["C.Nemoz", "H.Payno"]
__license__ = "MIT"
__date__ = "19/03/2019"

from .reconsparam.reconsparams import ReconsParams
from .reconsparam.axis import AxisRP, AxisMode
from tomwer.core.process.baseprocess import SingleProcess, _input_desc, \
    _output_desc
from tomwer.core.scan.scanbase import TomoBase
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.core.log import TomwerLogger

_logger = TomwerLogger(__name__)


class AxisProcess(SingleProcess):
    """
    Process to define the axis position
    """
    inputs = [_input_desc(name='data', type=TomoBase, handler='process',
                          doc='scan path'), ]
    outputs = [_output_desc(name='data', type=TomoBase, doc='scan path')]

    def __init__(self, recons_params):
        SingleProcess.__init__(self)
        assert recons_params is None or isinstance(recons_params, AxisRP)
        self._recons_params = recons_params or AxisRP()
        """Axis reconstruction parameters to apply"""
        self._locked = False
        """Boolean used to lock reconstruction parameters edition"""

    def set_recons_params(self, recons_params):
        assert isinstance(recons_params, AxisRP)
        self._recons_params = recons_params

    def set_editable(self, editable=True):
        self._locked = not editable

    def process(self, scan=None):
        if isinstance(scan, dict):
            _logger.warning('convert scan from a dict')
            _scan = ScanFactory.create_scan_object_frm_dict(scan)
        else:
            _scan = scan
        assert isinstance(_scan, TomoBase)

        # if the scan has no tomo reconstruction parameters yet create them
        if _scan.tomo_recons_params is None:
            _scan.tomo_recons_params = self._instanciateReconsParams()

        # copy axis recons parameters
        _scan.tomo_recons_params.copy(self._recons_params)
        assert _scan._tomo_recons_params.axis is not None

        if self._locked is True:
            # if the reconstruction parameters are lock, simple copy the already
            # computed axis position.
            # this has already be done when copying the Axis recons params
            pass
        else:
            try:
                _scan.tomo_recons_params.position = self.compute_axis_position(_scan)
            except NotImplementedError:
                _logger.error('Not implemented')
                _scan.tomo_recons_params.position = None
            except ValueError as e:
                _logger.error('Fail to compute axis position for', scan.path,
                              'reason is', e)
                _scan.tomo_recons_params.position = None
            else:
                _logger.info('Compute axis position (',
                             _scan.tomo_recons_params.position, ') for', _scan.path)

        if self._return_dict:
            return _scan.to_dict()
        else:
            return _scan

    def compute_axis_position(self, scan):
        """

        :return: position of the rotation axis. Use the `.AxisMode` defined
                 by the `.ReconsParams` of the `.AxisProcess`
        :rtype: float or None (if fail to compute the axis position)
        """
        if self._recons_params.mode is AxisMode.global_:
            return compute_global(scan=scan)
        elif self._recons_params.mode is AxisMode.manual:
            return self._recons_params.position_value
        elif self._recons_params.mode is AxisMode.excentrated:
            return self.compute_excentrated(scan=scan)
        elif self._recons_params.mode is AxisMode.near:
            return compute_near(scan=scan)
        elif self._recons_params.mode is AxisMode.read:
            return compute_read(scan=scan)
        else:
            raise NotImplementedError()

    def setMode(self, mode, value):
        if mode is AxisMode.manual:
            self._recons_params.cor_position = value
        else:
            raise NotImplementedError('mode not implemented yet')

    def _instanciateReconsParams(self):
        return ReconsParams(empty=True)


def compute_global(scan):
    """
    TODO

    :param scan:
    :return:
    """
    radio1, radio2 = scan.getDefaultRadiosForAxisCalc()
    if not (radio1 and radio2):
        raise ValueError('Unable to find radios to global axis calculation')
    else:
        raise NotImplementedError()

def compute_highlow(scan):
    """
    TODO

    :param scan:
    :return:
    """
    raise NotImplementedError()

def compute_excentrated(scan):
    """
    TODO

    :return:
    """
    raise NotImplementedError()


def compute_near(scan):
    """
    TODO

    :return:
    """
    raise NotImplementedError()


def compute_read(scan):
    """
    TODO

    :return:
    """
    raise NotImplementedError()

