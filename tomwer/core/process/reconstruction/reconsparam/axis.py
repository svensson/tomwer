# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/03/2019"


import enum
from .base import _ReconsParam, _assert_param_instance, _assert_cast_to_boolean, _assert_cast_to_int


@enum.unique
class AxisMode(enum.Enum):
    manual = 0
    global_ = 1
    highlow = 3
    excentrated = 4
    near = 5
    read = 6


@enum.unique
class CorMode(enum.Enum):
    use_0_180 = 0
    use_90_270 = 1


class AxisRP(_ReconsParam):
    """
    Define axis for ftserie reconstruction
    """

    def __init__(self):
        _ReconsParam.__init__(self)
        self.__mode = AxisMode.global_
        """Mode used for defining the COR (center of rotation)"""
        self.__position_value = 0.0
        self.__cor_position = 0.0
        """cor_position of rotation axis if fixed"""
        self.__to_the_center = True
        """center reconstructed region on rotation axis"""
        self.__files_during_scan = False
        """use images during scan for axis calc"""
        self.__cor_mode = CorMode.use_0_180
        """use 0-180 or 90-270 images to calculate COR."""
        self.__cor_error = False
        """systematic error on COR calculation"""
        self.__plot_figure = True
        """display image in case of highlow"""
        self.__ha = True
        """invoque multi proj algo for COR calculation"""
        self.__oversampling = 4

        # binding
        self._managed_params = {
            'POSITION': self.__class__.mode,
            'POSITION_VALUE': self.__class__.position_value,
            'TO_THE_CENTER': self.__class__.to_the_center,
            'FILESDURINGSCAN': self.__class__.use_images_during_scan,
            'COR_POSITION': self.__class__.cor_position,
            'COR_ERROR': self.__class__.cor_error,
            'PLOTFIGURE': self.__class__.plot_figure,
            'HA': self.__class__.ha,
            'OVERSAMPLING': self.__class__.oversampling,
        }

    @property
    def mode(self):
        return self.__mode

    @mode.setter
    def mode(self, mode):
        if isinstance(mode, str):
            try:
                name = mode
                # keep octave compatibility
                if name in ('accurate', 'global'):
                    name = AxisMode.global_.name
                _mode = getattr(AxisMode, name)
            except:
                raise ValueError('Fail to create axis mode from %s' % mode)
        else:
            _assert_param_instance(mode, AxisMode)
            _mode = mode
        self.__mode = _mode
        self.changed()

    @property
    def position_value(self):
        return self.__position_value

    @position_value.setter
    def position_value(self, value):
        _assert_param_instance(value, (int, float))
        if self.__position_value != value:
            self.__position_value = value
            self.changed()

    @property
    def cor_position(self):
        return self.__cor_position

    @cor_position.setter
    def cor_position(self, value):
        if self.__cor_position != value:
            self.__cor_position = value
            self.changed()

    @property
    def cor_error(self):
        return self.__cor_error

    @cor_error.setter
    def cor_error(self, cor_error):
        if self.__cor_error != cor_error:
            self.__cor_error = cor_error
            self.changed()

    @property
    def to_the_center(self):
        return self.__to_the_center

    @to_the_center.setter
    def to_the_center(self, b):
        _assert_param_instance(b, (bool, int, float))
        _assert_cast_to_boolean(b)
        if self.__to_the_center != b:
            self.__to_the_center = b
            self.changed()

    @property
    def use_images_during_scan(self):
        return self.__files_during_scan

    @use_images_during_scan.setter
    def use_images_during_scan(self, use):
        _assert_param_instance(use, (bool, int, float))
        _assert_cast_to_boolean(use)
        if self.__files_during_scan != use:
            self.__files_during_scan = use
            self.changed()

    @property
    def cor_mode(self):
        return self.__cor_mode

    @cor_mode.setter
    def cor_mode(self, mode):
        _assert_param_instance(mode, CorMode)
        if mode != self.__cor_error:
            self.__cor_mode = mode
            self.changed()

    @property
    def plot_figure(self):
        return self.__plot_figure

    @plot_figure.setter
    def plot_figure(self, plot):
        _assert_param_instance(plot, (bool, int, float))
        _assert_cast_to_boolean(plot)
        if bool(plot) != self.__plot_figure:
            self.__plot_figure = bool(plot)
            self.changed()

    @property
    def ha(self):
        return self.__ha

    @ha.setter
    def ha(self, ha):
        _assert_param_instance(ha, (bool, int, float))
        _assert_cast_to_boolean(ha)
        if self.__ha != bool(ha):
            self.__ha = bool(ha)
            self.changed()

    @property
    def oversampling(self):
        return self.__oversampling

    @oversampling.setter
    def oversampling(self, value):
        _assert_param_instance(value, (int, float))
        _assert_cast_to_int(value)
        if int(value) != self.__oversampling:
            self.__oversampling = int(value)
            self.changed()

    def to_dict(self):
        # keep octave compatibility
        if self.mode == AxisMode.global_:
            _mode = 'accurate'
        else:
            _mode = self.mode.name
        _dict = {
            'POSITION': _mode,
            'POSITION_VALUE': self.position_value,
            'TO_THE_CENTER': self.to_the_center,
            'FILESDURINGSCAN': self.use_images_during_scan,
            'COR_POSITION': self.cor_position,
            'COR_ERROR': self.cor_mode.value,
            'PLOTFIGURE': self.plot_figure,
            'HA': self.ha,
            'OVERSAMPLING': self.oversampling,
        }
        _dict.update(self.unmanaged_params)
        return _dict

    @staticmethod
    def from_dict(_dict):
        axis = AxisRP()
        return axis

    def load_from_dict(self, _dict):
        self._load_unmanaged_params(_dict)
        self.mode = _dict['POSITION']
        self.position_value = _dict['POSITION_VALUE']
        self.to_the_center = _dict['TO_THE_CENTER']
        self.files_during_scan = _dict['FILESDURINGSCAN']
        self.cor_position = _dict['COR_POSITION']
        self.cor_error = _dict['COR_ERROR']
        self.plot_figure = _dict['PLOTFIGURE']
        self.ha = _dict['HA']
        self.oversampling = _dict['OVERSAMPLING']
