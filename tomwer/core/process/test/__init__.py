# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H.Payno"]
__license__ = "MIT"
__date__ = "02/08/2017"

import unittest

from . import test_axis
from . import test_conditions
from . import test_darkref
from . import test_datatransfert
from . import test_datawatcher
from . import test_lamino
from . import test_pyhstcaller
from . import test_recons_params
from . import test_scanlist
from . import test_timer


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(test_axis.suite())
    test_suite.addTest(test_conditions.suite())
    test_suite.addTest(test_darkref.suite())
    test_suite.addTest(test_datawatcher.suite())
    test_suite.addTest(test_lamino.suite())
    test_suite.addTest(test_pyhstcaller.suite())
    test_suite.addTest(test_recons_params.suite())
    test_suite.addTest(test_scanlist.suite())
    test_suite.addTest(test_timer.suite())

    return test_suite
