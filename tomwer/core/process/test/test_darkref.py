# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "05/04/2019"


import shutil
import tempfile
import unittest
from tomwer.core.utils import mockScan
from tomwer.core.process.reconstruction.reconsparam import ReconsParams
from tomwer.core.scan.scanbase import TomoBase
from ..reconstruction.darkref.darkrefs import DarkRefs
from ..reconstruction.darkref.darkrefscopy import DarkRefsCopy


class TestDarkRefIO(unittest.TestCase):
    """Test inputs and outputs types of the handler functions"""

    def setUp(self):
        self.scan_folder = tempfile.mkdtemp()

        self.scan = mockScan(scanID=self.scan_folder,
                             nRadio=10,
                             nRecons=1,
                             nPagRecons=4,
                             dim=10)
        self.recons_params = ReconsParams()
        self.dkrf_process = DarkRefs(self.recons_params)

    def tearDown(self):
        shutil.rmtree(self.scan_folder)

    def testInputOutput(self):
        for input_type in (dict, TomoBase):
            for _input in DarkRefs.inputs:
                for return_dict in (True, False):
                    with self.subTest(handler=_input.handler,
                                      return_dict=return_dict,
                                      input_type=input_type):
                        input_obj = self.scan
                        if input_obj is dict:
                            input_obj = input_obj.to_dict()
                        self.dkrf_process._set_return_dict(return_dict)
                        out = getattr(self.dkrf_process, _input.handler)(input_obj)
                        if return_dict:
                            self.assertTrue(isinstance(out, dict))
                        else:
                            self.assertTrue(isinstance(out, TomoBase))


class TestDarkRefCopyIO(unittest.TestCase):
    """Test inputs and outputs types of the handler functions"""

    def setUp(self):
        self.scan_folder = tempfile.mkdtemp()

        self.scan = mockScan(scanID=self.scan_folder,
                             nRadio=10,
                             nRecons=1,
                             nPagRecons=4,
                             dim=10)
        self.recons_params = ReconsParams()
        self.dkrf_process = DarkRefsCopy(self.recons_params)

    def tearDown(self):
        shutil.rmtree(self.scan_folder)

    def testInputOutput(self):
        for input_type in (dict, TomoBase):
            for _input in DarkRefsCopy.inputs:
                for return_dict in (True, False):
                    with self.subTest(handler=_input.handler,
                                      return_dict=return_dict,
                                      input_type=input_type):
                        input_obj = self.scan
                        if input_obj is dict:
                            input_obj = input_obj.to_dict()
                        self.dkrf_process._set_return_dict(return_dict)
                        out = getattr(self.dkrf_process, _input.handler)(input_obj)
                        if return_dict:
                            self.assertTrue(isinstance(out, dict))
                        else:
                            self.assertTrue(isinstance(out, TomoBase))


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestDarkRefIO, TestDarkRefCopyIO):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite
