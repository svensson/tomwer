# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
"""
Basic class each orange widget processing an action should inheritate from
her corresponding class in core.
And all of those classes from core should implement this interface to deal
with the interpreter parser.
Allowing to convert an orane workflow to a TOWER workflow ( same action but no
GUI )
"""


__author__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "02/06/2017"


import threading
from collections import namedtuple

_input_desc = namedtuple(
        "_input_desc", ['name', 'type', 'handler', 'doc']
)

_output_desc = namedtuple(
        "_output_desc", ['name', 'type', 'doc']
)


class BaseProcess(object):
    """Class from which all tomwer process should inherit

    :param logger: the logger used by the class
    """

    endless_process = False
    """used to know if the equivalent luigi process is a one shot operation"""

    inputs = []
    """list of requested inputs"""

    outputs = []
    """list of generated outputs"""

    _output_values = {}

    def __init__(self):
        self._scheme_title = None  # title of the associated scheme
        self._return_dict = False
        """should the return type of the handler should be TomoBase instance
        objects or dict"""

    def setProperties(self, properties):
        raise NotImplementedError('BaseProcess is an abstract class')

    @staticmethod
    def properties_help():
        """

        :return: display the list of all managed keys and possible values
        :rtype: str
        """
        # TODO: use argsparse instead of this dict ?
        raise NotImplementedError('BaseProcess is an abstract class')

    def get_output_value(self, key):
        """

        :param str key: 
        :return: 
        """
        assert type(key) is str
        if key in self._output_values:
            return self._output_values[key]
        else:
            return None

    def clear_output_values(self):
        self._output_values.clear()

    def register_output(self, key, value):
        """

        :param str key: name of the output
        :param value: value of the output
        """
        assert self.key_exist(key)
        self._output_values[key] = value

    def key_exist(self, key):
        for _output in self.outputs:
            if _output.name == key:
                return True
        return False

    def input_handler(self, name):
        """

        :param str name: name of input (can be see as link type)
        :return: handler name (str) or None if name not defined
        """
        for _input in self.inputs:
            if _input.name == name:
                return _input.handler
        return None

    def _set_return_dict(self, return_dict):
        """

        :param bool return_dict: if True, force the process to return a dict
                                 instead of a `.TomoBase` object
        """
        self._return_dict = return_dict


class SingleProcess(BaseProcess):
    """
    Interface for Single process (which can be applied on a single scan)
    """
    # TODO: refactor, scan should be inputs
    def process(self, scan=None):
        """
        Process should return an int. Default are zero for success and one for
        failure. It can also return an error value

        :param scan:
        :return:
        :rtype: int
        """
        raise NotImplementedError('Base class')


class EndlessProcess(BaseProcess):
    """
    Interface for Single process (which can be applied on a single scan)
    """
    endless_process = True

    process_finished_event = threading.Event()
    '''event when the process is finished'''

    def start(self):
        raise NotImplementedError('Base class')

    def stop(self):
        raise NotImplementedError('Base class')
