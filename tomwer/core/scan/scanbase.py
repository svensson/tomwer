# coding: utf-8
#/*##########################################################################
# Copyright (C) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/


__authors__ = ["H.Payno"]
__license__ = "MIT"
__date__ = "09/08/2018"



class ScanBase(object):
    """
    Class representing a scan.
    Contains main functions needed by the processing

    :param str scan: path to the folder containing the scan
    :param str _type: path to the folder containing the scan
    """
    _DICT_TYPE_KEY = 'type'

    _DICT_PATH_KEY = 'path'

    def __init__(self, scan, _type):
        self._path = scan
        self._type = _type

    @property
    def path(self):
        """
        str - ID of the scan (directory containing projections files)
        """
        return self._path

    @path.setter
    def path(self, path):
        assert type(path) is str
        self._path = path

    @property
    def type(self):
        """
        str - type of the scanBase, can be 'edf' or 'hdf5' for now
        """
        return self._type

    def to_dict(self):
        """convert the TomoBase object to a dictionary"""
        raise NotImplementedError("Base class")

    def load_from_dict(self, _dict):
        raise NotImplementedError("Base class")



class TomoBase(ScanBase):
    """
    Class representing a tomography acquisition
    """

    _DICT_TOMO_RP_KEY = "tomo_recons_params"

    _DICT_LAMINO_RP_KEY = "lamino_recons_params"

    def __init__(self, scan, _type):
        ScanBase.__init__(self, scan, _type)
        self._flats = []
        self._darks = []
        self._projections = []
        self._reconstructions = []
        self._tomo_recons_params = None
        """Set of reconstruction parameters for tomography"""
        # TODO: this onformation can be used on ftseries to know if this is
        # the first time we are running a reconstruction or not
        self._lamino_recons_params = None
        """Set of reconstructions parameters for laminography"""

    @property
    def flats(self):
        """list of flats files"""
        return self._flats

    @flats.setter
    def flats(self, flats):
        self._flats = flats

    @property
    def darks(self):
        """list of darks files"""
        return self._darks

    @darks.setter
    def darks(self, darks):
        self._darks = darks

    @property
    def projections(self):
        """list of projections files"""
        return self._projections

    @projections.setter
    def projections(self, projections):
        self._projections = projections

    @property
    def reconstructions(self):
        """list of reconstruction files"""
        return self._reconstructions

    @reconstructions.setter
    def reconstructions(self, reconstructions):
        self._reconstructions = reconstructions

    @property
    def tomo_recons_params(self):
        return self._tomo_recons_params

    @tomo_recons_params.setter
    def tomo_recons_params(self, recons_params):
        self._tomo_recons_params = recons_params

    @property
    def lamino_recons_params(self):
        return self._lamino_recons_params

    @lamino_recons_params.setter
    def lamino_recons_params(self, recons_params):
        self._lamino_recons_params = recons_params

    # TODO: change name. Should be generalized to return Dataurl
    def getReconstructedFilesFromParFile(self, with_index):
        raise NotImplementedError('Base class')

    def getDefaultRadiosForAxisCalc(self):
        """
        Try to find two well adapted radios for Axis calculation
        :return: tuple(numpy.array, numpy.array)
        """
        raise NotImplementedError('Base class')

    def getFlat(self, index):
        """Return the flat file of given index"""
        raise NotImplementedError('Base class')

    def getDark(self):
        raise NotImplementedError('Base class')

    def flatFieldCorrection(self, data):
        """Apply flat field correction on the given data

        :param numpy.ndarray data: the data to apply correction on
        :return: corrected data
        :rtype: numpy.ndarray
        """
        raise NotImplementedError('Base class')

    def getReconsParamList(self):
        """

        :return: reconstruction parameters
        :rtype: ReconsParamList
        """
        raise NotImplementedError('Base class')

    def to_dict(self):
        res = {}
        res[self._DICT_TYPE_KEY] = self.type
        res[self._DICT_PATH_KEY] = self.path
        # res[self._JSON_TOMO_RP_KEY] = self.tomo_recons_params
        if self.tomo_recons_params:
            res[self._DICT_TOMO_RP_KEY] = self.tomo_recons_params.to_dict()
        else:
            res[self._DICT_TOMO_RP_KEY] = None
        res[self._DICT_LAMINO_RP_KEY] = self.lamino_recons_params
        return res

    to_dict.__doc__ = ScanBase.to_dict.__doc__

    def __eq__(self, other):
        """
        .. note:: does not lock on reconstructions, darks, flats as long those
                  should be deduced from the file / folder path
        """
        return (
            isinstance(other, self.__class__) or isinstance(self, other.__class__) and
            self.type == other.type and
            self.tomo_recons_params == other.tomo_recons_params and
            self.lamino_recons_params == other.lamino_recons_params and
            self.path == other.path
        )


class _TomoBaseDock(object):
    """
    Internal class to make difference between a simple TomoBase output and
    an output for a different processing (like scanvalidator.UpdateReconsParam)
    """
    def __init__(self, tomo_instance):
        self.__instance = tomo_instance

    @property
    def instance(self):
        return self.__instance
