# coding: utf-8
#/*##########################################################################
# Copyright (C) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/


__authors__ = ["H.Payno"]
__license__ = "MIT"
__date__ = "09/08/2018"


import glob
import os
import re
from collections import OrderedDict

import fabio
import numpy
import json
import io

from tomwer.core.log import TomwerLogger
from tomwer.core.process.reconstruction.darkref.settings import REFHST_PREFIX, \
    DARKHST_PREFIX
from tomwer.core.utils import getScanRange
from tomwer.core.utils import getTomo_N
from tomwer.core.utils.ftseriesutils import orderFileByLastLastModification
from silx.io.url import DataUrl
from silx.io import utils as silx_io_utils
from .scanbase import TomoBase

_logger = TomwerLogger(__name__)


global counter_rand
counter_rand = 1  # used to be sure to return a unique index on recons slices


class EDFTomoScan(TomoBase):
    """
    Class used to represent a tomography acquisition with hdf5 files.

    :param Union[str, None] scan: path of the scan
    """
    _TYPE = 'edf'

    def __init__(self, scan):
        TomoBase.__init__(self, scan=scan, _type=self._TYPE)
        self._dark = None

    def getFlat(self, projectionI=None):
        """
        If projectionI is not requested then return the mean value. Otherwise
        return the interpolated value for the requested projection.

        :param int or None projectionI:
        :return: Flat field value or None if can't deduce it
        """
        data = self._extractFromOneFile('refHST.edf', what='flat')
        if data is not None:
            return data

        data = self._extractFromPrefix(REFHST_PREFIX, what='flat',
                                       proI=projectionI)
        if data is not None:
            return data

        _logger.warning('Cannot retrieve flat file from %s' % self.path)
        return None

    def getDark(self):
        """
        For now only deal with one existing dark file.

        :return: image of the dark if existing. Else None
        """
        if self._dark is None:

            # first try to retrieve data from dark.edf file or darkHST.edf files
            self._dark = self._extractFromOneFile('dark.edf', what='dark')
            if self._dark is None:
                self._dark = self._extractFromOneFile('darkHST.edf',
                                                      what='dark')
            if self._dark is None:
                self._dark = self._extractFromPrefix(DARKHST_PREFIX,
                                                     what='dark')
            if self._dark is None:
                self._dark = self._extractFromPrefix('darkend', what='dark')

            if self._dark is None:
                _logger.warning('Cannot retrieve dark file from %s' % self.path)

        return self._dark

    def _extractFromOneFile(self, f, what):
        path = os.path.join(self.path, f)
        if os.path.exists(path):
            _logger.info('Getting %s from %s' % (what, f))
            try:
                data = fabio.open(path).data
            except:
                return None
            else:
                if data.ndim is 2:
                    return data
                elif data.ndim is 3:
                    _logger.warning('%s file contains several images. Taking '
                                    'the mean value' % what)
                    return numpy.mean(data.ndim)
        else:
            return None

    def _extractFromPrefix(self, pattern, what, proI=None):
        files = glob.glob(os.path.join(self.path, pattern + '*.edf'))
        if len(files) is 0:
            return None
        else:
            d = {}
            for f in files:
                index = self.guessIndexFromEDFFileName(f)
                if index is None:
                    _logger.error('cannot retrieve projection index for %s'
                                  '' % f)
                    return None
                else:
                    d[index] = fabio.open(f).data

            if len(files) is 1:
                return d[list(d.keys())[0]]

            oProj = OrderedDict(sorted(d.items()))
            # for now we only deal with interpolation between the higher
            # and the lower acquired file ()
            lowPI = list(oProj.keys())[0]
            uppPI = list(oProj.keys())[-1]

            lowPD = oProj[lowPI]
            uppPD = oProj[uppPI]

            if len(oProj) > 2:
                _logger.warning('Only bordering projections (%s and %s) will '
                                'be used for extracting %s' % (lowPI, proI, what))

            uppPI = uppPI
            index = proI
            if index is None:
                index = (uppPI - lowPI) / 2

            if (index >= lowPI) is False:
                index = lowPI
                _logger.warning('ProjectionI not in the files indexes'
                                'range (projectionI >= lowerProjIndex)')

            if (index <= uppPI) is False:
                index = uppPI
                _logger.warning('ProjectionI not in the files indexes'
                                'range upperProjIndex >= projectionI')

            # simple interpolation
            _nRef = (uppPI - lowPI)
            lowPI = lowPI

            w0 = (lowPI + (uppPI - index)) / _nRef
            w1 = index / _nRef

            return (w0 * lowPD + w1 * uppPD)

    @staticmethod
    def guessIndexFromEDFFileName(_file):
        name = _file.rstrip('.edf')
        ic = []
        while name[-1].isdigit():
            ic.append(name[-1])
            name = name[:-1]

        if len(ic) is 0:
            return None
        else:
            orignalOrder = ic[::-1]
            return int(''.join(orignalOrder))

    def getSampleEvolScan(self):
        """Return the 'extra' radios of a scan which are used to see if the scan
        moved during the acquisition.

        :return dict: angles as keys, radios as value. If no extra radio are
                      found, return the dictionary of all radios.
        """
        def orderImg(radios):
            radios = list(radios.values())
            radios.sort()
            return radios[waitedImgs - len(radios):]

        def _getAllRadios(radios):
            res = {}
            for radio in radios:
                res[os.path.basename(radios[radio])] = radios[radio]
            return res

        assert os.path.isdir(self.path)
        waitedImgs = getTomo_N(self.path)
        radios = EDFTomoScan.getRadioPaths(self.path)

        extraImg = 0
        if waitedImgs and waitedImgs < len(radios):
            extraImg = len(radios) - waitedImgs

        if extraImg is 4:
            res = orderImg(radios)
            scanRange = getScanRange(self.path)
            if scanRange != 360:
                _logger.warning('incoherent data information to retrieve scan'
                                ' evolultion (sample moved widget) ')
                return _getAllRadios(radios)
            radios = list(radios.values())
            radios.sort()
            return {
                '0': radios[0],
                '90': radios[waitedImgs//4-1],
                '180': radios[waitedImgs//2-1],
                '270': radios[waitedImgs//4*3-1],
                '360': radios[waitedImgs-1],
                '270(1)': res[0],
                '180(1)': res[1],
                '90(1)': res[2],
                '0(1)': res[3]}
        elif extraImg is 5:
            res = orderImg(radios)
            scanRange = getScanRange(self.path)
            if scanRange != 360:
                _logger.warning('incoherent data information to retrieve scan'
                                'evolultion (sample moved widget) ')
                return _getAllRadios(radios)

            radios = list(radios.values())
            radios.sort()
            return {
                '0': radios[0],
                '90': radios[waitedImgs//4-1],
                '180': radios[waitedImgs//2-1],
                '270': radios[waitedImgs//4*3-1],
                '360': radios[waitedImgs-1],
                '360(1)': res[0],
                '270(1)': res[1],
                '180(1)': res[2],
                '90(1)': res[3],
                '0(1)': res[4]}
        elif extraImg is 3:
            res = orderImg(radios)
            # TODO: should be self.getScanRange
            scanRange = getScanRange(self.path)
            if scanRange != 180:
                _logger.warning('incoherent data information to retrieve scan'
                                'evolultion (sample moved widget) ')
                return _getAllRadios(radios)
            radios = list(radios.values())
            radios.sort()
            return {
                '0': radios[0],
                '90': radios[(waitedImgs - 2)//2],
                '180': radios[waitedImgs-1],
                '180(1)': res[0],
                '90(1)': res[1],
                '0(1)': res[2]
            }
        elif extraImg is 2:
            res = orderImg(radios)
            scanRange = getScanRange(self.path)
            if scanRange != 180:
                _logger.warning('incoherent data information to retrieve scan'
                                'evolultion (sample moved widget) ')
                return _getAllRadios(radios)
            radios = list(radios.values())
            radios.sort()
            return {
                '0': radios[0],
                '90': radios[(waitedImgs - 2)//2],
                '180': radios[waitedImgs-1],
                '90(1)': res[0],
                '0(1)': res[1]
            }
        elif extraImg is 0:
            return _getAllRadios(radios)
        else:
            _logger.warning("find unusual number of scans to check if moved. "
                            "Can't deduce any angle pattern.")
            return _getAllRadios(radios)

    def flatFieldCorrection(self, data):
        """
        Apply the flat field correction on the given data.

        :param numpy.ndarray data: radio to correct
        :return numpy.ndarray: corrected data
        """
        assert type(data) is numpy.ndarray
        conditionOK = True
        dark = self.getDark()
        if dark is None:
            _logger.error(
                'cannot make flat field correction, dark not found')
            conditionOK = False

        if dark is not None and dark.ndim != 2:
            _logger.error(
                'cannot make flat field correction, dark should be of '
                'dimension 2')
            conditionOK = False

        flat = self.getFlat()
        if flat is None:
            _logger.error(
                'cannot make flat field correction, flat not found')
            conditionOK = False

        if flat is not None and flat.ndim != 2:
            _logger.error(
                'cannot make flat field correction, flat should be of '
                'dimension 2')
            conditionOK = False

        if dark is not None and flat is not None and dark.shape != flat.shape:
            _logger.error('Given dark and flat have incoherent dimension')
            conditionOK = False

        if data.shape != dark.shape:
            _logger.error('Image has invalid. Cannot apply flat field'
                          'correction it')
            conditionOK = False

        if conditionOK is False:
            return data

        return (data - dark) / (flat - dark)

    def updateDataset(self):
        """update list of radio and reconstruction by parsing the scan folder
        """
        self.projections = EDFTomoScan.getRadioPaths(self.path)
        self.reconstructions = EDFTomoScan.getReconstructionsPaths(self.path)

    @staticmethod
    def getReconstructionsPaths(scanID, withIndex=False):
        """
        Return the dict of files:
        * fitting with a reconstruction pattern and ending by .edf
        * .vol files

        :param scanID: is the path to the folder of acquisition
        :param bool withIndex: if False then return a list of slices otherwise
            return a dict with the index of the slice reconstructed.
        """
        def containsDigits(input):
            return any(char.isdigit() for char in input)

        if (scanID is None) or (not os.path.isdir(scanID)):
            if withIndex is True:
                return {}
            else:
                return []

        pyhst_files = EDFTomoScan.getPYHST_ReconsFile(scanID)
        if pyhst_files is not None:
            return EDFTomoScan.getReconstructedFilesFromParFile(pyhst_files, with_index=withIndex)
        else:
            folderBasename = os.path.basename(scanID)
            files = {} if withIndex is True else []
            if os.path.isdir(scanID):
                for f in os.listdir(scanID):
                    if f.endswith(".edf") and f.startswith(folderBasename) and 'slice_' in f:
                        localstring = f.rstrip('.edf')
                        if 'slice_' in localstring:
                            localstring = f.rstrip('.edf')
                            if 'slice_pag_' in localstring:
                                indexStr = localstring.split('slice_pag_')[-1].split('_')[0]
                            else:
                                indexStr = localstring.split('slice_')[-1].split('_')[0]
                            if containsDigits(indexStr):
                                gfile = os.path.join(scanID, f)
                                assert(os.path.isfile(gfile))
                                if withIndex is True:
                                    files[EDFTomoScan.getIndexReconstructed(f, scanID)] = gfile
                                else:
                                    files.append(gfile)
                    if f.endswith(".vol"):
                        if withIndex is True:
                            files[EDFTomoScan.getIndexReconstructed(f, scanID)] = os.path.join(scanID, f)
                        else:
                            files.append(os.path.join(scanID, f))
            return files

    @staticmethod
    def getRadioPaths(scanID):
        """Return the dict of radios for the given scan.
        Keys of the dictionary is the slice number
        Return all the file on the root of scan starting by the name of scan and
        ending by .edf

        :param scanID: is the path to the folder of acquisition
        """
        if(scanID is None) or not(os.path.isdir(scanID)):
            return []

        files = dict({})
        if os.path.isdir(scanID):
            for f in os.listdir(scanID):
                if EDFTomoScan.isARadioPath(f, scanID):
                    gfile = os.path.join(scanID, f)
                    files[EDFTomoScan.getIndexReconstructed(f, scanID)] = gfile

        return files

    @staticmethod
    def isARadioPath(fileName, scanID):
        """Return True if the given fileName can fit to a Radio name
        """
        fileBasename = os.path.basename(fileName)
        folderBasename = os.path.basename(scanID)

        if fileBasename.endswith(".edf") and fileBasename.startswith(folderBasename):
            localstring = fileName.rstrip('.edf')
            # remove the scan
            localstring = re.sub(folderBasename, '', localstring)
            if 'slice_' in localstring:
                # case of a reconstructed file
                return False
            if 'refHST' in localstring:
                return False
            s = localstring.split('_')
            if s[-1].isdigit():
                # check that the next value is a digit
                return True

        return False

    @staticmethod
    def getIndexReconstructed(reconstructionFile, scanID):
        """Return the slice reconstructed of a file from her name

        :param str reconstructionFile: the name of the file
        """
        folderBasename = os.path.basename(scanID)
        if reconstructionFile.endswith(".edf") and reconstructionFile.startswith(
                folderBasename):
            localstring = reconstructionFile.rstrip('.edf')
            # remove the scan
            localstring = re.sub(folderBasename, '', localstring)
            s = localstring.split('_')
            if s[-1].isdigit():
                return int(s[-1])
            else:
                _logger.warning("Fail to find the slice reconstructed for "
                                "file %s" % reconstructionFile)
        else:
            global counter_rand
            counter_rand = counter_rand + 1
            return counter_rand

    @staticmethod
    def getPYHST_ReconsFile(scanID):
        """Return the .par file used for the current reconstruction if any.
        Otherwise return None """
        if scanID == "":
            return None

        if scanID is None:
            raise RuntimeError('No current acquisition to validate')
        assert(type(scanID) is str)
        assert(os.path.isdir(scanID))
        folderID = os.path.basename(scanID)
        # look for fasttomo files ending by slice.par
        parFiles = glob.glob(os.path.join(scanID + folderID) + '*_slice.par')
        if len(parFiles) > 0:
            return orderFileByLastLastModification(scanID, parFiles)[-1]
        else:
            return None

    def load_from_dict(self, desc):
        from tomwer.core.process.reconstruction.reconsparam import ReconsParams  # avoid cyclic import
        if isinstance(desc, io.TextIOWrapper):
            data = json.load(desc)
        else:
            data = desc
        if not (self._DICT_TYPE_KEY in data and data[self._DICT_TYPE_KEY] == self._TYPE):
            raise ValueError('Description is not an EDFScan json description')

        assert self._DICT_PATH_KEY in data
        assert self._DICT_LAMINO_RP_KEY in data
        self.path = data[self._DICT_PATH_KEY]
        recons_param_data = data[self._DICT_TOMO_RP_KEY]
        if recons_param_data is not None:
            self.tomo_recons_params = ReconsParams.from_dict(recons_param_data)
        self.lamino_recons_params = data[self._DICT_LAMINO_RP_KEY]
        return self

    load_from_dict.__doc__ = TomoBase.load_from_dict.__doc__

    def getDefaultRadiosForAxisCalc(self):
        """
        Try to find a couple of `opposite` radios that can be used for axis
        calculation.

        :return: tuple (radio0, radio1), radios of angles (0, 180) if found,
                 else (90, 270), else (None, None)
        :rtype: tuple
        """
        radios_with_angle = self.getSampleEvolScan()
        for couple in (('0', '180'), ('90', '270')):
            if couple[0] in radios_with_angle and couple[1] in radios_with_angle:
                def load_data_and_ff_cor(file_path):
                    url_radio = DataUrl(file_path=file_path, scheme='fabio')
                    radio = silx_io_utils.get_data(url_radio)
                    return self.flatFieldCorrection(radio)

                radio_0 = load_data_and_ff_cor(file_path=radios_with_angle[couple[0]])
                radio_1 = load_data_and_ff_cor(file_path=radios_with_angle[couple[1]])
                return radio_0, radio_1
        return None, None

