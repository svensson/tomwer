# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "02/08/2017"


import unittest
from tomwer.core import utils
import tempfile
import os
import shutil
from tomwer.test.utils import UtilsTest


class TestGetClosestEnergy(unittest.TestCase):
    def setUp(self):
        self.topSrcFolder = tempfile.mkdtemp()
        self.dataSetID = 'scan_3_'
        self.dataDir = UtilsTest.getDataset(self.dataSetID)
        self.sourceS3 = os.path.join(self.topSrcFolder, self.dataSetID)
        shutil.copytree(src=os.path.join(self.dataDir),
                        dst=self.sourceS3)

        self.sourceT01 = os.path.join(self.topSrcFolder, 'test01')
        shutil.copytree(src=UtilsTest.getDataset('test01'),
                        dst=self.sourceT01)
        self.S3XMLFile = os.path.join(self.sourceS3, 'scan_3_.xml')
        self.S3Ref0000 = os.path.join(self.sourceS3, 'ref0000_0000.edf')
        self.S3Ref0010 = os.path.join(self.sourceS3, 'ref0000_0010.edf')

    def tearDown(self):
        shutil.rmtree(self.topSrcFolder)

    def testEnergyFromEDF(self):
        os.remove(self.S3XMLFile)
        self.assertTrue(
            utils.getClosestEnergy(scan=self.sourceS3,
                                   refFile=self.S3Ref0000) == 61)
        self.assertTrue(
            utils.getClosestEnergy(scan=self.sourceS3,
                                   refFile=self.S3Ref0010) == 61)

    def testEnergyFromXML(self):
        os.remove(self.S3Ref0000)
        os.remove(self.S3Ref0010)
        self.assertTrue(utils.getClosestEnergy(scan=self.sourceS3,
                                               refFile=self.S3Ref0000) == 10)
        self.assertTrue(utils.getClosestEnergy(scan=self.sourceS3,
                                               refFile=self.S3Ref0010) == 10)

    def testEnergyFromInfo(self):
        self.assertTrue(
            utils.getClosestEnergy(scan=self.sourceT01, refFile=None) == 19)

    def testDefaultEnergy(self):
        os.remove(self.S3XMLFile)
        os.remove(self.S3Ref0000)
        os.remove(self.S3Ref0010)
        self.assertTrue(utils.getClosestEnergy(scan=self.sourceS3,
                                               refFile=self.S3Ref0000) is None)
        self.assertTrue(utils.getClosestEnergy(scan=self.sourceS3,
                                               refFile=self.S3Ref0010) is None)


class TestGetClosestSREnergy(unittest.TestCase):
    def setUp(self):
        self.topSrcFolder = tempfile.mkdtemp()
        self.dataSetID = 'test10'
        self.dataDir = UtilsTest.getDataset(self.dataSetID)
        self.sourceT10 = os.path.join(self.topSrcFolder, self.dataSetID)
        shutil.copytree(src=os.path.join(self.dataDir),
                        dst=self.sourceT10)
        self.T10XMLFile = os.path.join(self.sourceT10, 'test10.xml')
        self.T10InfoFile = os.path.join(self.sourceT10, 'test10.info')
        self.T10Ref0000 = os.path.join(self.sourceT10, 'ref0000_0000.edf')

    def tearDown(self):
        shutil.rmtree(self.topSrcFolder)

    def testIntenistyFromInfo(self):
        self.assertTrue(
            utils.getClosestSRCurrent(scan=self.sourceT10, refFile=None) == 101.3)

    def testDefaultIntensity(self):
        os.remove(self.T10XMLFile)
        os.remove(self.T10InfoFile)
        self.assertTrue(utils.getClosestSRCurrent(scan=self.sourceT10) is None)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestGetClosestEnergy, TestGetClosestSREnergy):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
