# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "25/02/2019"


import numpy
import logging
_logger = logging.getLogger(__file__)
try:
    import scipy.ndimage.shift
    has_scipy_shift = True
except ImportError:
    has_scipy_shift = False
    _logger.info('no scipy.ndimage.shift detected, will use numpy.fft instead')


def shift_img(data, dx, dy):
    """
    Apply simple 2d image shift.

    :param numpy.ndarray data: 
    :param dx: x translation to be applied
    :param dy: y translation to be applied

    :return: shifted image
    :rtype: numpy.ndarray
    """
    assert data.ndim is 2
    _logger.info('apply shift dx=%s, dy=%s ' % (dx, dy))
    if has_scipy_shift:
        return scipy.ndimage.shift(input=data, shift=(dx, dy), order=3,
                                   mode='wrap')
    else:
        ynum, xnum = data.shape
        xmin = int(-numpy.fix(xnum / 2))
        xmax = int(numpy.ceil(xnum / 2) - 1)
        ymin = int(-numpy.fix(ynum / 2))
        ymax = int(numpy.ceil(ynum / 2) - 1)

        nx, ny = numpy.meshgrid(numpy.linspace(xmin, xmax, xnum),
                                numpy.linspace(ymin, ymax, ynum))

        res = abs(numpy.fft.ifft2(numpy.fft.fft2(data) * numpy.exp(
                1.0j * 2.0 * numpy.pi * (dy * ny / ynum + dx * nx / xnum))))
        return res

