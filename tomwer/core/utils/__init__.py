# coding: utf-8
# ##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "17/05/2017"

import fabio.edfimage
import numpy
import os
import psutil
from tomwer.core.log import TomwerLogger
import shutil
from tomwer.core import settings
from tomwer.unitsystem import metricsystem
from urllib.request import urlopen, ProxyHandler, build_opener
import fileinput
from lxml import etree
from xml.etree import cElementTree

logger = TomwerLogger(__name__)

MOCK_LOW_MEM = False    # if True will simulate the case the computer run into low memory


_RECONS_PATTERN = '_slice_'

_PAG_RECONS_PATTERN = '_slice_pag_'


def mockReconstruction(folder, nRecons=5, nPagRecons=0, volFile=False):
    """
    create reconstruction files into the given folder

    :param str folder: the path of the folder where to save the reconstruction
    :param nRecons: the number of reconstruction to mock
    :param nPagRecons: the number of paganin reconstruction to mock
    :param volFile: true if we want to add a volFile with reconstruction
    """
    assert type(nRecons) is int and nRecons >= 0
    basename = os.path.basename(folder)
    dim = 200
    for i in range(nRecons):
        f = os.path.join(folder, basename + str(_RECONS_PATTERN + str(i) + ".edf"))
        data = numpy.zeros((dim, dim))
        data[::i+2, ::i+2] = 1.0
        edf_writer = fabio.edfimage.EdfImage(data=data,
                                             header={"tata": "toto"})
        edf_writer.write(f)

    for i in range(nPagRecons):
        f = os.path.join(folder, basename + str(_PAG_RECONS_PATTERN + str(i) + ".edf"))
        data = numpy.zeros((dim, dim))
        data[::i+2, ::i+2] = 1.0
        edf_writer = fabio.edfimage.EdfImage(data=data,
                                             header={"tata": "toto"})
        edf_writer.write(f)

    if volFile is True:
        volFile = os.path.join(folder, basename + '.vol')
        infoVolFile = os.path.join(folder, basename + '.vol.info')
        dataShape = (nRecons, dim, dim)
        data = numpy.random.random(nRecons*dim * dim).reshape(nRecons, dim, dim)
        data.astype(numpy.float32).tofile(volFile)
        _createVolInfoFile(filePath=infoVolFile, shape=dataShape)


def _createVolInfoFile(filePath, shape, voxelSize=1, valMin=0.0, valMax=1.0,
                       s1=0.0, s2=1.0, S1=0.0, S2=1.0):
    assert len(shape) is 3
    f = open(filePath, 'w')
    f.writelines("\n".join([
        '! PyHST_SLAVE VOLUME INFO FILE',
        'NUM_X =  %s' % shape[2],
        'NUM_Y =  %s' % shape[1],
        'NUM_Z =  %s' % shape[0],
        'voxelSize =  %s' % voxelSize,
        'BYTEORDER = LOWBYTEFIRST',
        'ValMin =  %s' % valMin,
        'ValMax =  %s' % valMax,
        's1 =  %s' % s1,
        's2 =  %s' % s2,
        'S1 =  %s' % S1,
        'S2 =  %s' % S2
    ]))
    f.close()


def fastMockAcquisition(folder, n_radio=20):
    """
    Simple function creating an acquisition into the given directory
    This won't complete data, scan.info of scan.xml files but just create the
    structure that data watcher is able to detect in edf mode.
    """
    assert type(n_radio) is int and n_radio > 0
    basename = os.path.basename(folder)
    dim = 200
    if not os.path.exists(folder):
        os.mkdir(folder)

    info_file = os.path.join(folder, basename + '.info')
    if not os.path.exists(info_file):
        # write the info file
        open(info_file, 'w')

    # create scan files
    for i in range(n_radio):
        file_name = basename + '_{0:04d}'.format(i) + ".edf"
        f = os.path.join(folder, file_name)
        if not os.path.exists(f):
            data = numpy.random.random(dim * dim).reshape(dim, dim)
            edf_writer = fabio.edfimage.EdfImage(data=data,
                                                 header={"tata": "toto"})
            edf_writer.write(f)

    xml_file = os.path.join(folder, basename + '.xml')
    if not os.path.exists(xml_file):
        # write the final xml file
        root = cElementTree.Element("root")
        tree = cElementTree.ElementTree(root)
        tree.write(xml_file)


def isLowOnMemory(path=''):
    """

     :return: True if the RAM usage is more than MAX_MEM_USED (or low memory
        is simulated)
    """
    if path == settings.LBSRAM_ID:
        if settings.MOCK_LBSRAM is True:
            return MOCK_LOW_MEM
        else:
            assert os.path.isdir(path)
            return psutil.disk_usage(path).percent > settings.MAX_MEM_USED
    else:
        return (MOCK_LOW_MEM is True) or psutil.disk_usage(path).percent > settings.MAX_MEM_USED


def mockLowMemory(b=True):
    """Mock the case the computer is running into low memory
    """
    global MOCK_LOW_MEM
    MOCK_LOW_MEM = b
    return psutil.virtual_memory().percent > settings.MAX_MEM_USED


def mockScan(scanID, nRadio, nRecons, nPagRecons, dim):
    """
    Create some random radios and reconstruction in the folder

    :param str scanID: the folder where to save the radios and scans
    :param int nRadio: The number of radios to create
    :param int nRecons: the number of reconstruction to mock
    :param int nRecons: the number of paganin reconstruction to mock
    :param int dim: dimension of the files (nb row/columns)
    """
    assert type(scanID) is str
    assert type(nRadio) is int
    assert type(nRecons) is int
    assert type(dim) is int
    from tomwer.core.scan.scanfactory import ScanFactory  # avoid cyclic import

    fastMockAcquisition(folder=scanID, n_radio=nRadio)
    mockReconstruction(folder=scanID,
                       nRecons=nRecons,
                       nPagRecons=nPagRecons)
    return ScanFactory.create_scan_object(scanID)


def _getInformation(scan, refFile, information, _type, aliases=None):
    """
    Parse files contained in the given directory to get the requested
    information

    :param scan: directory containing the acquisition. Must be an absolute path
    :param refFile: the refXXXX_YYYY which should contain information about the
                    scan.
    :return: the requested information or None if not found
    """
    def parseRefFile(filePath):
        header = fabio.open(filePath).header
        for k in aliases:
            if k in header:
                return _type(header[k])
        return None

    def parseXMLFile(filePath):
        try:
            for alias in info_aliases:
                tree = etree.parse(filePath)
                elmt = tree.find("acquisition/" + alias)
                if elmt is None:
                    continue
                else:
                    info = _type(elmt.text)
                    if info == -1:
                        return None
                    else:
                        return info
        except etree.XMLSyntaxError as e:
            logger.warning(e)
            return None

    def parseInfoFile(filePath):
        def extractInformation(text, alias):
            text = text.replace(alias, '')
            text = text.replace('\n', '')
            text = text.replace(' ', '')
            text = text.replace('=', '')
            return _type(text)
        info = None
        f = open(filePath, "r")
        line = f.readline()
        while line:
            for alias in info_aliases:
                if alias in line:
                    info = extractInformation(line, alias)
                    break
            line = f.readline()
        f.close()
        return info

    info_aliases = [information]
    if aliases is not None:
        assert type(aliases) in (tuple, list)
        [info_aliases.append(alias) for alias in aliases]

    if not os.path.isdir(scan):
        return None

    if refFile is not None and os.path.isfile(refFile):
        info = parseRefFile(refFile)
        if info is not None:
            return info

    baseName = os.path.basename(scan)
    infoFiles = [os.path.join(scan, baseName + '.info')]
    infoOnDataVisitor = infoFiles[0].replace('lbsram', '')
    # hack to check in lbsram, would need to be removed to add some consistency
    if os.path.isfile(infoOnDataVisitor):
        infoFiles.append(infoOnDataVisitor)
    for infoFile in infoFiles:
        if os.path.isfile(infoFile) is True:
            info = parseInfoFile(infoFile)
            if info is not None:
                return info

    xmlFiles = [os.path.join(scan, baseName + '.xml')]
    xmlOnDataVisitor = xmlFiles[0].replace('lbsram', '')
    # hack to check in lbsram, would need to be removed to add some consistency
    if os.path.isfile(xmlOnDataVisitor):
        xmlFiles.append(xmlOnDataVisitor)
    for xmlFile in xmlFiles:
        if os.path.isfile(xmlFile) is True:
            info = parseXMLFile(xmlFile)
            if info is not None:
                return info

    return None


def getClosestEnergy(scan, refFile=None):
    """
    Parse files contained in the given directory to get information about the
    incoming energy for the serie `iSerie`

    :param scan: directory containing the acquisition
    :param refFile: the refXXXX_YYYY which should contain information about the
                    energy.
    :return: the energy in keV or none if no energy found
    """
    return _getInformation(os.path.abspath(scan), refFile, information='Energy',
                           aliases=['energy', 'ENERGY'], _type=float)


def getClosestSRCurrent(scan, refFile=None):
    """
    Parse files contained in the given directory to get information about the
    incoming energy for the serie `iSerie`

    :param scan: directory containing the acquisition
    :param refFile: the refXXXX_YYYY which should contain information about the
                    energy.
    :return: the energy in keV or none if no energy found
    """
    return _getInformation(os.path.abspath(scan), refFile, information='SRCUR',
                           aliases=['SrCurrent', 'machineCurrentStart'],
                           _type=float)


def getSRCurrent(scan, when):
    assert when in ('start', 'end')
    xmlFiles = [os.path.join(os.path.abspath(scan), os.path.basename(scan) + '.xml')]
    xmlOnDataVisitor = xmlFiles[0].replace('lbsram', '')
    # hack to check in lbsram, would need to be removed to add some consistency
    if os.path.isfile(xmlOnDataVisitor):
        xmlFiles.append(xmlOnDataVisitor)
    for xmlFile in xmlFiles:
        if os.path.isfile(xmlFile):
            try:
                tree = etree.parse(xmlFile)
                key = 'machineCurrentStart' if when == 'start' else 'machineCurrentStop'
                elmt = tree.find("acquisition/" + key)
                if elmt is None:
                    return None
                else:
                    info = float(elmt.text)
                    if info == -1:
                        return None
                    else:
                        return info
            except etree.XMLSyntaxError as e:
                logger.warning(e)
                return None
    return None

# TODO : should be moved in the scan module
def getTomo_N(scan):
    return _getInformation(os.path.abspath(scan), refFile=None,
                           information='TOMO_N', _type=int,
                           aliases=['tomo_N', 'Tomo_N'])

# TODO mv to scan module
def getScanRange(scan):
    return _getInformation(os.path.abspath(scan), refFile=None,
                           information='ScanRange', _type=int)


# TODO: mv to scan module
def getDARK_N(scan):
    return _getInformation(os.path.abspath(scan), refFile=None,
                           information='DARK_N', _type=int,
                           aliases=['dark_N',])


def rebaseParFile(_file, oldfolder, newfolder):
    """Update the given .par file to replace oldfolder location by the newfolder.

    .. warning:: make the replacement in place.

    :param _file: par file to update
    :param oldfolder: previous location of the .par file
    :param newfolder: new location of the .par file
    """
    with fileinput.FileInput(_file, inplace=True,
                             backup='.bak') as parfile:
        for line in parfile:
            line = line.rstrip().replace(oldfolder, newfolder)
            print(line)

# TODO: move to scan module
def getDim1Dim2(scan):
    """

    :param scan: path to the acquisition
    :return: detector definition
    :rtype: tuple of int
    """
    d1 = d2 = None
    _info_file = os.path.join(scan, os.path.basename(scan) + '.info')
    if os.path.isfile(_info_file):
        _dict = getParametersFromParOrInfo(_info_file)

        if 'dim_1' in _dict:
            d1 = int(_dict['dim_1'])
        if 'dim_2' in _dict:
            d2 = int(_dict['dim_2'])
    return d1, d2


# TODO: move to scan module
def getStartEndVoxels(scan):
    _keys = {
        'START_VOXEL_1': None,
        'START_VOXEL_2': None,
        'START_VOXEL_3': None,
        'END_VOXEL_1': None,
        'END_VOXEL_2': None,
        'END_VOXEL_3': None,
    }
    _par_file = os.path.join(scan, os.path.basename(scan) + '.par')
    _info_file = os.path.join(scan, os.path.basename(scan) + '.info')
    if os.path.isfile(_par_file):
        logger.info('Retrieve start and end voxel from %s' % _par_file)
        _dict = getParametersFromParOrInfo(_par_file)
        for key in _keys:
            if key in _keys:
                _keys[key] = _dict[key]
    elif os.path.isfile(_info_file):
        logger.info('Retrieve start and end voxel from %s' % _par_file)
        _dict = getParametersFromParOrInfo(_info_file)
        _keys['START_VOXEL_1'] = _keys['END_VOXEL_1'] = 0
        _keys['START_VOXEL_2'] = _keys['END_VOXEL_2'] = 0
        _keys['START_VOXEL_3'] = _keys['END_VOXEL_3'] = 0
        if 'dim_1' in _dict:
            _keys['END_VOXEL_1'] = _keys['END_VOXEL_2'] = int(_dict['dim_1']) - 1
        if 'dim_2' in _dict:
            _keys['END_VOXEL_3'] = int(_dict['dim_2']) - 1

    return (
        _keys['START_VOXEL_1'], _keys['END_VOXEL_1'],
        _keys['START_VOXEL_2'], _keys['END_VOXEL_2'],
        _keys['START_VOXEL_3'], _keys['END_VOXEL_3']
    )


# TODO: move to scan module
def getParametersFromParOrInfo(_file):
    """
    Create a dictionary from the file with the information name as keys and
    their values as values
    """
    assert os.path.exists(_file) and os.path.isfile(_file)
    ddict = {}
    f = open(_file, "r")
    lines = f.readlines()
    for line in lines:
        if not '=' in line:
            continue
        l = line.replace(' ', '')
        l = l.rstrip('\n')
        # remove on the line comments
        if '#' in l:
            l = l.split('#')[0]
        key, value = l.split('=')
        ddict[key.lower()] = value
    return ddict


# TODO: move to scan module
def getFirstProjFile(scan):
    """Return the first .edf containing a projection"""
    if os.path.isdir(scan) is False:
        return None
    files = sorted(os.listdir(scan))

    while (len(files) > 0 and (files[0].startswith(os.path.basename(scan)) and
           files[0].endswith('.edf')) is False):
        files.remove(files[0])

    if len(files) > 0:
        return os.path.join(scan, files[0])
    else:
        return None


def getPixelSize(scan):
    """
    Try to retrieve the pixel size from the set of files.

    :return: the pixel size in meter or None
    :rtype: None or float
    """
    if os.path.isdir(scan) is False:
        return None
    value = _getInformation(scan=scan, refFile=None, information='PixelSize',
                            _type=float)
    if value is None:
        parFile = os.path.join(scan, os.path.basename(scan) + '.par')
        if os.path.exists(parFile):
            ddict = getParametersFromParOrInfo(parFile)
            if 'IMAGE_PIXEL_SIZE_1'.lower() in ddict:
                value = float(ddict['IMAGE_PIXEL_SIZE_1'.lower()])
    # for now pixel size are stored in microns. We want to return them in meter
    if value is not None:
        return value * metricsystem.micrometer
    else:
        return None

url_base = "http://www.edna-site.org/pub/tomwer/"


def DownloadDataset(dataset, output_folder, timeout, unpack=False):
    # create if needed path scan
    url = url_base + dataset

    logger.info("Trying to download scan %s, timeout set to %ss",
                dataset, timeout)
    dictProxies = {}
    if "http_proxy" in os.environ:
        dictProxies['http'] = os.environ["http_proxy"]
        dictProxies['https'] = os.environ["http_proxy"]
    if "https_proxy" in os.environ:
        dictProxies['https'] = os.environ["https_proxy"]
    if dictProxies:
        proxy_handler = ProxyHandler(dictProxies)
        opener = build_opener(proxy_handler).open
    else:
        opener = urlopen
    logger.info("wget %s" % url)
    data = opener(url, data=None, timeout=timeout).read()
    logger.info("Image %s successfully downloaded." % dataset)

    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)

    try:
        archive_folder = os.path.join(output_folder, os.path.basename(dataset))
        with open(archive_folder, "wb") as outfile:
            outfile.write(data)
    except IOError:
        raise IOError("unable to write downloaded \
                        data to disk at %s" % archive_folder)

    if unpack is True:
        shutil.unpack_archive(archive_folder, extract_dir=output_folder, format='bztar')
        os.remove(archive_folder)
