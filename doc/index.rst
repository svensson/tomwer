======
TOMWER
======

TOMWER is a solution to help users to deal with workflows at beamline.
This have been developed by the ESRF based on the orange toolkit (https://orange.biolab.si/).

To build those workflows we are offering several widgets/operation at user.


.. toctree::
   :hidden:

   install.rst
   developer.rst
   other.rst
   applications/index.rst
   tutorials/index.rst


:doc:`install`
    Installation

:doc:`applications/index`
    applications

:doc:`tutorials/index`
    tutorials for using the orange add-on

:doc:`developer`
    developer information

:doc:`other`
    How to get help, graylog (web logs), environment variables.


for the html help from the GUI we need to keep those in the index.rst file

--------------------------------
tomwer on esrf official debian 8
--------------------------------

A tomwer module for debian 8 is provided.
This mean that if you have an esrf official debian 8 distribution you can load the module:

.. code-block:: bash

    module load tomwer

and execute tomwer.

.. code-block:: bash

    tomwer [my application]


To launch the orange add-on you can launch the `orange-canvas` application which
will also be loaded with the tomwer module.

If you have recorder an orange workflow (`.ows`) you can load it directly using:

.. code-block:: bash

    orange-canvas myWorkflow.ows


-------
Widgets
-------

.. the orange widgets section has to be stored here to fit the orange requirement (and accessing the help from orange-canvas)

This is the list of actual widgets/boxes proposed by the workflow tool:

.. toctree::
  :maxdepth: 1

  widgets/darkrefwidget.rst
  widgets/filterwidget.rst
  widgets/foldertransfertwidget.rst
  widgets/ftserieswidget.rst
  widgets/groupradio.rst
  widgets/groupslice.rst
  widgets/imagestackviewerwidget.rst
  widgets/liveslicewidget.rst
  widgets/samplemovedwidget.rst
  widgets/scanlistwidget.rst
  widgets/scanselectorwidget.rst
  widgets/scanvalidatorwidget.rst
  widgets/timerwidget.rst
  widgets/tofuwidget.rst
  widgets/tomodirwidget.rst
