Launch orange
-------------

If orange is installed using a python virtual environment first activate the python virtual environment.
For example:

.. code-block:: bash

    source /orange3venv/bin/activate

launching orange:

.. code-block:: bash

    orange-canvas

The following canvas should appear:

.. image:: img/orangeStart.png

Then select `new` to start a new orange project and enter a project. `tomwer-tutorial` for example.

On the left part you can see the different add-on. The one we are interest on is tomwer. Once activated
you should see the different widgets:

.. image:: img/tomwer_addon_widgets.png
