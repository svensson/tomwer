from tomwer.core.utils import fastMockAcquisition
from tomwer.luigi.scheme import Node, Link, Scheme
from silx.io import dictdump
import os
import random
import string
import argparse
import sys
import tomwer.luigi


def remove_proxy():
    proxy = {}
    for proxy_key in ('http_proxy', 'https_proxy'):
        if proxy_key in os.environ:
            proxy[proxy_key] = os.environ[proxy_key]
            del os.environ[proxy_key]
    return proxy


def start_workflow(parameters_files_dir, observed_folder):
    """"launch the workflow"""
        
    node1 = Node(luigi_task='esrf.tomwer.DataWatcherTask', properties={'observed_folder':observed_folder}, id=0)
    node2 = Node(luigi_task='esrf.tomwer.DarkRefsTask', id=1)
    node3 = Node(luigi_task='esrf.tomwer.FtseriesTask', id=2)

    nodes = [node1, node2, node3]

    links = [   
        Link(source_node=node1, source_channel='data', sink_node=node2, sink_channel='data'),
        Link(source_node=node2, source_channel='data', sink_node=node3, sink_channel='data'),
    ]

    scheme = Scheme(nodes=nodes, links=links)

    assert len(scheme.finalsNodes()) is 0
    assert len(scheme.endlessNodes()) is 1
    assert scheme.endlessNodes()[0].get_subworkflow_requirements() == [2]
    assert list(node3.get_dependencies()) == [node2.id, ]
    assert len(node2.get_dependencies()) is 0

    tomwer.luigi.exec_(scheme, properties_folder=parameters_files_dir, name='test_process')


def stop_workflow(parameters_files_dir):
    def write_parameters(status):
        parameters_file = os.path.join(parameters_files_dir,
                                       'data_watcher_properties.ini')
        control = {
            'requested_status': status
        }
        _dict = {
            'control': control
        }
        dictdump.dicttoini(ddict=_dict, inifile=parameters_file)

    write_parameters(status='stop')


def create_scan(observed_folder):
    observed_folder = '/nobackup/linazimov/payno/dev/install_esrforange/orangeProject/orange/test_output'
    folder = os.path.join(observed_folder, 'acqui_000')
    os.mkdir(folder)
    fastMockAcquisition(folder)


def main(argv):
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('action', )
    parser.add_argument('parameters_files_dir')
    parser.add_argument('observed_dir')
    options = parser.parse_args(argv[1:])
    if options.action =='start':
        start_workflow(parameters_files_dir=options.parameters_files_dir,
                       observed_folder=options.observed_dir)
    elif options.action == 'stop':
        stop_workflow(parameters_files_dir=options.parameters_files_dir)
    elif options.action == 'create_scan':
        if hasattr(random, 'choices'):
            scan_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        else:
            scan_name = ''
            for k in range(10):
                scan_name = scan_name + random.choice(string.ascii_uppercase + string.digits)
        _dir = os.path.join(options.observed_dir, scan_name)
        fastMockAcquisition(_dir)
    else:
        raise NotImplementedError()
    exit(0)


if __name__ == '__main__':
    main(sys.argv)
