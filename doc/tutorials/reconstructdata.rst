"""""""""""""""""""""""""""""""""""""""""
Tutorial 2 - reconstructionusing ftseries
"""""""""""""""""""""""""""""""""""""""""

Prerequisites
-------------

   - install tomwer & orange (see :doc:`../install`)
   - download dataset `` for the tutorial from ...
   - have a look to the first tutorial :doc:`transfertdata`
   - have pyhst2 install and configured.
   - have fastomo3 installed.
   - you might want to have a look on the official `orange website <https://orange.biolab.si/>`_ where some documentation can be found.


.. include:: launchorange.rst


Creating the workflow
---------------------

Adding scan list
~~~~~~~~~~~~~~~~

We can define a list of scan that we already know they are containing
To add a new scan directory select `add`.

For this example please register the folder containing the dataset ``

.. image:: img/tuto_2_datalist.png

.. note:: During acquisition the usage of the 'data watcher' will
    be more appropriate (see `Tutorial 1 - data transfer_`). You should use the data list only if acquisition are valid and finished.

Adding ftserie
~~~~~~~~~~~~~~

Now that scan are specified we will have to reconstruct the raw data. For this we need to create an instance of :class:`FtseriesOW`

This widget offers interface to tune your reconstruction and once it's activated it will launch the reconstruction.
Connect the output of the 'datalist' with the input of 'ftserie'.

.. image:: img/tuto_2_datalist_ftserie.png

Adding scan validator
~~~~~~~~~~~~~~~~~~~~~

After the ftserie we can connect a `scanvalidator`. It will help you to see the result of reconstruction and allow you to change the reconstruction parameters.
Once the scan are not validated they will be 'stopped' by the scan validator.

.. image:: img/tuto_2_datalist_ftserie_scanvalidator.png


.. note:: for lbsram system some security are existing to free scan 'stopped' by the `scanvalidator`.

Launching the workflow
----------------------

Defining the reconstruction parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First you need to specify the properties of the reconstruction if this is not done.
To do saw open the interface of the ftserie instance (double click on it).

Then the following pop up should appears:

.. image:: img/tuto_2_ftseriewidget.png


Selection of the folder containing data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If not done yet please register the folder containing the dataset. Here the folder should be the used for acquisition.
Unlike the `datawatcher` the `scanlist` won't parse sub folder for valid acquisition.


Starting the workflow
~~~~~~~~~~~~~~~~~~~~~

Once the setup of the `scan list` and `ftserie` are done you can start the workflow by clicking on the `send` button from the `scan list` interface.

.. image:: img/tuto_2_launching.png


Reconstruction observation
--------------------------

Once the reconstruction is done. You can see the result of the reconstruction using the `scan validator` widget.


Going further
-------------

You can also generate dark and flat field using the `dark and flat field` widget.


.. note:: if you want to run simple reconstruction after recosntructions you can also use the :doc:`../applications/ftserie` application.
