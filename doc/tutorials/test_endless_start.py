from tomwer.core.utils import fastMockAcquisition
from tomwer.luigi.scheme import Node, Link, Scheme
from silx.io import dictdump
import luigi
import os
import random
import string
import argparse
import sys


def remove_proxy():
    proxy = {}
    for proxy_key in ('http_proxy', 'https_proxy'):
        if proxy_key in os.environ:
            proxy[proxy_key] = os.environ[proxy_key]
            del os.environ[proxy_key]
    return proxy


def start_process(parameters_files_dir, observed_folder):
    """"launch the endless process"""
    parameters_file = os.path.join(parameters_files_dir, 'data_watcher_properties.ini')

    def write_parameters(status):
        parameters = {
            'observed_folder': observed_folder
        }
        control = {
            'requested_status': status
        }
        _dict  = {
          'properties': parameters,
          'control': control,
        }
        dictdump.dicttoini(ddict=_dict, inifile=parameters_file)

    remove_proxy()
    write_parameters(status='start')

    luigi_task = 'esrf.tomwer.DataWatcherTask'
    params = [luigi_task,
              '--workers', '2',
              '--properties-file', parameters_file,
              '--no-lock',  # ignore if similar process already exists
              '--workflow-name', 'test3',
              '--node-id', '1',          
              ]

    assert luigi.interface.run(params) is True


def stop_process(parameters_files_dir):
    def write_parameters(status):
        parameters_file = os.path.join(parameters_files_dir,
                                       'data_watcher_properties.ini')
        control = {
            'requested_status': status
        }
        _dict = {
            'control': control
        }
        dictdump.dicttoini(ddict=_dict, inifile=parameters_file)

    write_parameters(status='stop')


def create_scan(observed_folder):
    fastMockAcquisition(observed_folder)


def main(argv):  
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('action')
    parser.add_argument('parameters_files_dir')
    parser.add_argument('observed_dir')
    options = parser.parse_args(argv[1:])

    if options.action =='start':
        start_process(parameters_files_dir=options.parameters_files_dir,
                      observed_folder=options.observed_dir)
    elif options.action == 'stop':
        stop_process(parameters_files_dir=options.parameters_files_dir)
    elif options.action == 'create_scan':
        if hasattr(random, 'choices'):
            scan_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        else:
            scan_name = ''
            for k in range(10):
                scan_name = scan_name + random.choice(string.ascii_uppercase + string.digits)
        _dir = os.path.join(options.observed_dir, scan_name)
        fastMockAcquisition(_dir)
    else:
        raise NotImplementedError()
    exit(0)


if __name__ == '__main__':
    main(sys.argv)
