""""""""""""""""""""""""""
Tutorial 1 - data transfer
""""""""""""""""""""""""""

Prerequisites
-------------

   - install tomwer & orange (see :doc:`../install`)
   - download data for the tutorial from
   - define the 'TARGET_OUTPUT_FOLDER' directory to avoid having to define the output directory each time.
   - you might want to have a look on the official `orange website <https://orange.biolab.si/>`_ where some documentation can be found.

.. include:: launchorange.rst

Creation of the `data watcher` - `data transfert` workflow
----------------------------------------------------------

First we want to create an instance of `data watcher`.

.. image:: img/dataWatcherInstance.png

The goal of the `data watcher` is to detect when some data are acquired and ended.

Then in order to perform the transfert we need to connect it with an instance of `data transfert`.
To do it create a new `data transfert`

.. image:: img/dataWatcher_dataTransfert_noConnected.png

To perform the connection click on the widget output (dotted line on the right) and release it on the entry of the `data transfert` widget.
The result should look like :

.. image:: img/dataWatcher_dataTransfert_Connected.png

Now we are ready for launching the workflow.
Configure the `datawatcher` to retrieve your data: using the `select folder` button. The `datawatcher` will parse the folder and all subfolders
in order to find valid acquisitions.

.. image:: img/dataWatcher_interface.png

.. note:: tool buttons on the right are used respectively for tunning the 'validity of a scan' and accessing the history of valid found scan.

Then press `start observation`. A waiting wheel quickly followed by a green tick should appear meaning that the acquisition has been found
Once found it will active the next widget which will deal with the data transfert.

This mean that the scan should quickly be moved to the 'TARGET_OUTPUT_FOLDER' defined. If none are defined then it will ask you for defining the output folder.

The `data watcher` is looping until we stop it. This mean that it can work continuously.

You can save the workflow using File/saveAs action.

More details on the orange canvas are accessible here:

.. note:: some help on widget is accessible on the bottom left of orange and can also be accessible from the 'F1' key.
