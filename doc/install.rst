installation
::::::::::::

.. toctree::
   :hidden:

   other/help.rst
   web.rst

tomwer tool for workflow is based on Orange.
So you will need python version >= 3.4.

Step 0 - Create a virtual env
'''''''''''''''''''''''''''''

It is recommended to create a python virtual environment to run the workflow tool.
Virtual environment might avoid some conflict between python packages. But you can also install it on your 'current' python environment and move to step 1.

.. code-block:: bash

   virtualenv --python=python3 --system-site-packages myvirtualenv


Then activate the virtual environment

.. code-block:: bash

   source myvirtualenv/bin/activate

.. note:: To quit the virtual environment

   .. code-block:: bash

      deactivate

Step 1 - Orange3 installation
'''''''''''''''''''''''''''''

You will need a fork of the original Orange project in order to run the tomwer project.
This is needed because small modification have been made in order to get the behavio we wanted (has looping workflows).

The fork is accessible here : https://github.com/payno/orange3.git

So install this fork :

.. code-block:: bash

   git clone https://github.com/payno/orange3.git
   cd orange3
   pip install -r requirements.txt
   pip install -r requirements-gui.txt
   pip install .

.. note:: if you have an old PyQt version, you might need to have a look at https://pythonhosted.org/silx/virtualenv.html?highlight=virtualenv to avoid rebuild of sip... you might want to create a symbolic link:

   If you want to use PyQt4 installed in */usr/lib/python2.7/dist-packages/*:

   .. code-block:: bash

      ln -s /usr/lib/python2.7/dist-packages/PyQt4 silx_venv/lib/python2.7/site-packages/
      ln -s /usr/lib/python2.7/dist-packages/sip.so silx_venv/lib/python2.7/site-packages/


.. note:: Since the version compatible with tomwer 0.2 we add the registration of logs into a log file for linux O.S.
          For now to access this service the `/var/log/orange` folder should exists and the orange applicatin should have the writing rights.


Step 2 - tomwer
'''''''''''''''

clone the tomwer project

.. code-block:: bash

   git clone git@gitlab.esrf.fr:payno/tomwer.git


then install it

.. code-block:: bash

   cd tomwer
   pip install -r requirements.txt
   pip install .


.. note:: -e option will register the add-on into Orange, but you shouldn't copy it into the Python's site-packages directory. This is due to the Orange add-on installation procedure. That mean also that any modification into this source code will be apply during execution time.


Make sure the installation whent well, and that Orange is running correctly.

.. code-block:: bash

   python run_tests.py

Unit test should be executed without any error.


Step 3 - web log
''''''''''''''''

the workflow tool can send some log into graylog in order to get view of the status of the workflow execution.
If this is active (by default) then you will be able to see important log from a web interface.

To get more information see :doc:`web`


Launching Orange
::::::::::::::::

you can simply execute the command:

.. code-block:: bash

   orange-canvas


.. note:: if your installed a virtual environment do not forget to active it :

.. code-block:: bash

   source myvirtualenv/bin/activate


Launching applications
::::::::::::::::::::::

After the installation tomwer is embedding several applications.

Those applications can be launched by calling:

.. code-block:: bash

   tomwer appName {options}

.. note:: if you only call `tomwer` then the man page will be displayed.


Documentation
:::::::::::::

.. code-block:: bash

   cd doc
   make html

The documentation is build in doc/build/html and the entry point is index.html

.. code-block:: bash

   firefox build/html/index.html

.. note:: the build of the documentation need sphinx to be installed. This is not an hard dependacy. So you might need to install it.


You also should generate documentation to be accessible from Orange GUI (pressing the F1 key).

.. code-block:: bash

   cd doc
   make htmlhelp

To get more information about help from Orange see :

:doc:`other/help`
    accessing documentation from the interface


Warning
:::::::

the tomwer GUI is using silx from the silx.gui.qt module which try to use PyQt5, then PySide2, then PyQt4
Order is different for AnyQt, used by Orange3. So some incoherence might append (will bring errors) in the Signal/SLOT connection
during widget instanciation.

You should be carrefull about it if more than one Qt binding is available (one quick and durty fix is to change the AnyQt/__init__.py file - 'availableapi' function for example)
