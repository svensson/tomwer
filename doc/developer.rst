---------------------
Developer information
---------------------

Installation
''''''''''''

A bash script exists in order to automate the installation under debian OS.
It is available in the internal esrf project: https://gitlab.esrf.fr/payno/install_tomwer

Unit tests
''''''''''

A set of unit test have been developed in order to maintain the application.

You can run then by calling

.. code-block:: bash

   python run_test.py

The run_tests.py script have been take back from the silx project.

.. note:: each box should have an associated set of unit test

.. note:: We created a class 'orangecontrib.esrf.test.OrangeWorkflowTest' in order to test a full workflow with different boxes ...

As the tool is by default linked with a grayscale server the unit tests are running without this graypy connection.
If you want to turn on web connection for unit tests use the --web option.

Documentation
'''''''''''''

Documentation is available in 'http://www.silx.org/pub/doc/tomwer/latest/install.html'

In order to generate the full documentation (html + htmlhelp) you can go for:

.. code-block:: bash

   ./bootstrap.py setup.py build build_doc build_sphinx -b htmlhelp

Environment variable
''''''''''''''''''''

.. toctree::
   other/environment.rst

Web
'''

.. toctree::
   web.rst


Architecture
''''''''''''

   The idea is to decorelate the Graphical part of the processing part.
   So the project is containing the core package which should be able to process all the action without any call to qt.GUI infine.


API
'''

.. toctree::
   :hidden:


   modules/index.rst


:doc:`modules/index`


None graphical workflow
'''''''''''''''''''''''

we can launch orange workflows from command line by using the interpreter

But you should follow the structure :

orangecontrib.esrf.core.xxx
orangecontrib.esrf.widgets.xxxWidgets

orangecontrib.esrf.core should be Qt free.


Release
'''''''

To create a release we simply run :

.. code-block:: bash

    python setup.py bdist_wheel --universal

As we don't have any compile code we can create a universal wheel installable every where.

Just make sure you have wheel installed :

.. code-block:: bash

    pip install wheel

Orange Settings
"""""""""""""""

Orange settings are stored under .local for example:

.. code-block:: bash

    rm /users/opid19/.local/share/Orange/3.5.0.dev/widgets/[WidgetPath].pickle

This is where the widgets settings are stored. In the case of the Ftseries, if the structure of the FastSetupDefaultGlobal
is evolving you will have to remove this file in order to reset the new parameters set.
