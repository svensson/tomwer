dark and flat field construction
````````````````````````````````
.. snapshotqt:: img/darkrefwidget.png

    from tomwer.gui.darkref.darkrefcopywidget import DarkRefAndCopyWidget
    from tomwer.synctools.ftseries import QReconsParams
    widget = DarkRefAndCopyWidget(reconsparams=QReconsParams())
    widget.show()


Behavior
''''''''

This widget generate dark and/or flat field for an acquisition.

* Average. Compute the mean value
* Median. Compute the median value

Two options are existing:

* `remove if existing`: this will remove dark **and** ref files if already existing
* `skip if already existing`: this will skip dark and/or flat field generation if files are already there.


.. note:: the dark and or flat field will be generated inside the acquisition directory

.. note:: you can access this process without orange by running

    .. code-block:: bash

        tomwer darkref [acquisition folder]


File pattern
''''''''''''

In order to find the ref file and the darkend files we are using python `re library <https://docs.python.org/3/library/re.html>`_.
Here are a fiew example of how to use it:

* `.*conti_dark.*` will filter files containing `conti_dark` sentence
* `darkend[0-9]{3,4}` will filter files named `darkend` followed by three or four digit characters (and having the .edf extension)
* `ref*.*[0-9]{3,4}_[0-9]{3,4}` will filter files named `ref` followed by any character and ending by X_Y where X and Y are groups of three or four digit characters.

To have more example and usage see the `re library page <https://docs.python.org/3/library/re.html>`_.
