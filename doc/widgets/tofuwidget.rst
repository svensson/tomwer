tofu reconstruction
```````````````````

.. snapshotqt:: img/tofuwidget.png

   import tempfile
   from tomwer.gui.lamino.tofu import TofuWindow
   from tomwer.core import utils

   folder = tempfile.mkdtemp()
   utils.fastMockAcquisition(folder)
   utils.mockReconstruction(folder, nRecons=2, nPagRecons=3)

   widget = TofuWindow(parent=None)
   widget.show()

Behavior
''''''''

This widget will call the tofu application. So this mean that tofu has to be installed before launching any laminography reconstruction.
