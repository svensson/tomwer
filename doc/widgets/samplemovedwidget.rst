sample moved
````````````

.. snapshotqt:: img/samplemoved.png

    from tomwer.core.scan.edfscan import EDFTomoScan
    from tomwer.gui.samplemoved import SampleMovedWidget
    from tomwer.test.utils import UtilsTest
    from silx.io.url import DataUrl
    import os

    app = qt.QApplication.instance() or qt.QApplication([])

    widget = SampleMovedWidget()
    widget.setMinimumSize(800, 800)
    folder = UtilsTest.getInternalTestDir("001_0.28_19keV_Al63")

    scan = EDFTomoScan(scan=folder)
    rawSlices = scan.getSampleEvolScan()
    widget.clearOnLoadActions()
    widget.setImages(rawSlices)
    widget.setOnLoadAction(scan.flatFieldCorrection)

    _file1 = os.path.join(folder, '001_0.28_19keV_Al630000.edf')
    _file2 = os.path.join(folder, '001_0.28_19keV_Al630001.edf')
    widget.setSelection(url_img_a=DataUrl(file_path=_file1, scheme='fabio').path(),
                        url_img_b=DataUrl(file_path=_file2, scheme='fabio').path())
    widget.show()


Visualization widget to display some specific radio to be able to determine if the scan moved during an acquisition.

Behavior
''''''''

* show two images side by side.
* if possible determine the angle of the radio.
* each plot can plot the image or his symmetrical by x
