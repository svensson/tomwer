scan filter
```````````

.. snapshotqt:: img/filterwidget.png

   from tomwer.gui.conditions.filter import FileNameFilterWidget
   widget = FileNameFilterWidget()
   widget.show()

This widget allow to filter some widgets according to the name of the scan folder
