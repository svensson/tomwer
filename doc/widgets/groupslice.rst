slice stack
```````````

.. snapshotqt:: img/slicestackwidget.png

    import tempfile
    from tomwer.gui.stacks import SliceStack
    from tomwer.core import utils

    folder = tempfile.mkdtemp()
    utils.fastMockAcquisition(folder)
    utils.mockReconstruction(folder, nRecons=2, nPagRecons=3)

    widget = SliceStack()
    widget.addLeafScan(folder)
    widget.show()

This widget memorized all slice received and allow to browse through them.
