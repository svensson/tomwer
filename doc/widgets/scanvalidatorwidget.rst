data validator
``````````````

.. snapshotqt:: img/scanvalidatorwidget.png

    import tempfile
    from tomwer.test.utils import UtilsTest
    from orangecontrib.tomwer.widgets.control.DataValidatorOW import DataValidatorOW
    from tomwer.gui.viewerqwidget import ImageStackViewerValidator
    from tomwer.core.scan.scanfactory import ScanFactory
    from tomwer.core import utils

    folder = UtilsTest.getInternalTestDir("001_0.28_19keV_Al63")
    scan = ScanFactory.create_scan_object(folder)

    widget = DataValidatorOW()
    widget.addScan(scan)
    widget.widget.stackImageViewerTab.setCurrentIndex(1)
    widget.show()


Widget displaying results of a reconstruction and asking to the user if he want to validate or not the reconstruction.
User can also ask for some modification on the reconstruction parameters.

Behavior
''''''''

Buttons
*******

* 'Change reconstruction parameters' will send a signal to the ftseries widget and ask for the user new reconstruction parameters (and then run again a reconstruction)
* 'Validate' will send a signal to the next connected boxes saying this data is ok for nect operations

This inherit from [ImageStackViewerWidget](imagestackviewer.html)
