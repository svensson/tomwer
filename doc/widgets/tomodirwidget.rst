data watcher
============


.. snapshotqt:: img/datawatcher.png

    from tomwer.gui.datawatcher import DataWatcherWidget
    widget = DataWatcherWidget()
    widget.show()


The widget will observe folder and sub folders of a given path and waiting for acquisition to be ended.
The widget will infinitely wait until an acquisition is ended.
If an acquisition is ended then a signal containing the folder path is emitted.

* 'Select folder' will bring you to a dialog to select the top level directory to observe. 
* 'Start observation' to launch the observation. 
* 'Stop observation' to launch the observation. 

Environment variable
--------------------

The widget will automatically set the value of DATADIR if such an environment variable exists.

Note
----

Internally the observation will be runned from a thread managed by ftseries
