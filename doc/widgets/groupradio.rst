radio stack
```````````

.. snapshotqt:: img/radiostackwidget.png
   :width: 600 px
   :align: center

   from tomwer.test.utils import UtilsTest
   from tomwer.gui.stacks import RadioStack
   from tomwer.core import utils
   folder = UtilsTest.getInternalTestDir("001_0.28_19keV_Al63")
   widget = RadioStack()
   widget.addLeafScan(folder)
   widget.show()


This widget memorized all radio received and allow to browse through them.
