data selector
`````````````

.. snapshotqt:: img/scanselectorwidget.png

    import tempfile
    from tomwer.gui.scanselectorwidget import ScanSelectorWidget

    widget = ScanSelectorWidget()
    widget.add(tempfile.mkdtemp())
    widget.add(tempfile.mkdtemp())
    widget.show()


List all received scan and allow user to select a specific one to be moved to the next boxes.
