timer
`````

.. snapshotqt:: img/timerwidget.png

    from orangecontrib.tomwer.widgets.control.TimerOW import _TimerWidget
    widget = _TimerWidget(parent=None)
    widget.show()


This widget allow to postpone the treatment of the scan to the next treatment by specifying some time,
