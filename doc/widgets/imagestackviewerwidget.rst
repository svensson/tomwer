data viewer
```````````

.. snapshotqt:: img/imagestackviewerwidget.png

    import tempfile
    from tomwer.gui.stacks import SliceStack
    from tomwer.core import utils

    folder = tempfile.mkdtemp()
    utils.fastMockAcquisition(folder)
    utils.mockReconstruction(folder, nRecons=2, nPagRecons=3)

    widget = SliceStack()
    widget.addLeafScan(folder)
    widget.show()


Show a small recap of the reconstruction runned


Behavior
''''''''

Tabs
****

* 'scan to treat tab' is the tab containig the pill of scan received by the box on the chronological order.
* 'other' is the tab from where the user can enter a reconstruction ID ('Select folder' button) to see the information about this reconstruction.

When receiving a "data" signal this will update the 'scan to treat' tab and update the path openned when the user is activating 'select folder' button
