data list
`````````

.. snapshotqt:: img/scanlistwidget.png

    import tempfile
    from tomwer.gui.datalist import DataListWidget

    widget = DataListWidget(parent=None)
    widget.add(tempfile.mkdtemp())
    widget.add(tempfile.mkdtemp())
    widget.show()


List reconsturction path to be send to the next box once validated

Behavior
''''''''

* 'Add' button will bring you to an folder selection dialog to select n folder to the list
* 'Remove' Remove the selected folders to the list
* 'Send' send the folder one after the other to the connected boxes
