data transfert
``````````````

This widget insure data transfert of the received data to the given directory
To change the output directory press the 'select output folder' button.
For now the outputdir is automatically computed.

Environment variable
''''''''''''''''''''

The output dir will be automatically set to TARGET_OUTPUT_FOLDER if the environment variable exists.
