Orange and graylog
``````````````````

esrf orange is by default sending log messages to host/port defined in orangecontrib/esrf/web/config.py file
You can change this configuration directly from the file. For example if you want to install it locally:

.. code-block:: python

    grayport_port = 12201
    grayport_host = 'linazimov'


If orange is not able to find out the host then it will notice you during box creation. But run anyway.

If you want to avoid this communication then you can also define the environment variable "ORANGE_WEB_LOG" to "False"

Note: unit test are by default not sending message to graylog.
But it can be done by adding the --web option when running the unit test:

.. code-block:: bash

    python run_test --web

If a graylog server is running at the given hostname then you should be able to access it from a web browser at http://<grayport_host>:9000


Graylog installation
''''''''''''''''''''

install graylog via docker using this tutorial: http://docs.graylog.org/en/2.2/pages/installation/docker.html

then the communication between workflows and graylog will be made using udp (User datagram protocol).
So the docker-compose file should include the udp communication.

Then you should configure the docker-compose.yml file which will specify the docker.

Here is an example of such a file:

.. code-block:: text

    version: '2'
    services:
        mongo:
            image: "mongo:3"
            volumes:
                - /graylog/data/mongo:/data/db
        elasticsearch:
            image: "elasticsearch:2"
            command: "elasticsearch -Des.cluster.name='graylog'"
            volumes:
                - /graylog/data/elasticsearch:/tmp/elasticsearch/data
        graylog:
            image: graylog2/server:2.2.1-1
            volumes:
                - /graylog/data/journal:/tmp/graylog/data/journal
                - /graylog/config:/tmp/graylog/data/config
            environment:
                GRAYLOG_PASSWORD_SECRET: somepasswordpepper
                GRAYLOG_ROOT_PASSWORD_SHA2: 8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918
                GRAYLOG_WEB_ENDPOINT_URI: http://hostIP:9000/api
                GRAYLOG_REST_LISTEN_URL: http://hostIP:9000/api
                GRAYLOG_REST_TRANSPORT_URI: http://hostIP:9000/api
            depends_on:
                - mongo
                - elasticsearch
            ports:
                - "9000:9000"
                - "12201/udp:12201/udp"

Then you can launch docker

.. code-block:: bash

    docker-compose up

Then the graylog server should be launch. You can access to the server at http://hostIP:9000 (http://127.0.0.1:9000 in case of localhost)
and access to the monitoring of logs. Default login/password is admin/admin.

Then create a new input for GELF UDP. The configuration should look like something close to:

.. code-block:: text

    bind_address: 0.0.0.0
    decompress_size_limit: 8388608
    override_source: <empty>
    port: 12201
    recv_buffer_size: 262144


Then logs will be received and filter by graylog.

Here is a set of view of the graylog view.

general input interface

.. image:: img/graylog-input.png
   :width: 800 px
   :align: center

image received for one particular input

.. image:: img/graylog-UDP.png
   :width: 800 px
   :align: center

detail of one log received

.. image:: img/graylog-mess.png
   :width: 800 px
   :align: center

logs are containing multiple 'classical' information such as file and line of the log...
It also contains machine id and the name of the scan treated if the log come from a box
processing a particular scan. Thurthermore the name of the scheme (doc title) is specified.

Here is the command to stop docker.

.. code-block:: bash

    docker-compose down

Here are a fiew variables to be updated in the graylog.conf file if you want the graulog servr to be accessible from outside :
- rest_listen_uri = http://hostIP:9000/api