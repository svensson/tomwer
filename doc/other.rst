Other
-----

.. toctree::
   :hidden:

   other/environment.rst
   other/help.rst

:doc:`other/environment`
    used variable environment

:doc:`other/help`
    How to get help when executing the environment variable
