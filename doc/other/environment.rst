Environment variable
````````````````````

A set of environment variable can be used with esrf-orange

   - `TARGET_OUTPUT_FOLDER` : default output dir of the transfertWidget
   - `DATADIR` : root directory of the observation of ..class: tomoDirWidget
   - `PYHST_DIR` : path to the PYHST executables
   - `ORANGE_WEB_LOG` : if not setted will try to send log to grayscale. If True also. If false won't emit log messages.
   - `ORANGE_COLOR_STDOUT_LOG_` : to add color logs to the stdout. But this will break logs for orange log view as the color is directly inserted on the log message. This is why it is used for development purpose only.
                                 This variable will be set when launching orange-canvas. Use '--color-stdout-logs' to active it.
