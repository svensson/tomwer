--------------------------
Help files from Orange GUI
--------------------------
When F1 is press on open widget or on a selected widget icon, a help file is shown. Help
files are ordinary documentation pages, written in rst and placed into doc/widgets folder.

Help pages can be written in markdown or in rst. The only requirement is that the title of
the page is the same as the widget name. To allow automatic discovery of help pages, all
widget help pages should be listed on a documentation page inside a section named Widget.

The location of the documentation is set using the "orange.canvas.help" entrypoint. It
usually points to the constant WIDGET_HELP_PATH in the package containing widgets. The
constant should list the possible locations of the master page (the one that links to
all widgets). This example add-on includes three different locations:

- Development documentation is available for developers that manually build documentation using sphinx
- Locally installed documentation, which is available if the add-on was installed using a wheel package. Local installation has to be enabled in setup.py by calling function include_documentation.
- Online documentation points to the hosted documentation such as read the docs or python hosted.

The htmlhelp documentation is stored in edna-site.org under /data/distributions/doc/tomwer/.htmlhelp/[x.y.z] folders.

The locations are tried in order they are listed, the first one available will be used.

Beside widgets, add-on can also include tutorial schemas, that are shown in the Welcome
dialog. Tutorials are ordinary ows files, but often include annotations that guide the user.
Tutorial packages are registered in setup.py using "orange.widgets.tutorials" entry point.
