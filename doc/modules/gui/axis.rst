tomwer\.gui\.ftserie\.axis package
==================================

Module contents
---------------

.. automodule:: tomwer.gui.ftserie.axis.axis
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: tomwer.gui.ftserie.axis.ftaxiswidget
    :members:
    :undoc-members:
    :show-inheritance:
