tomwer\.gui\.datawatcher package
================================

Submodules
----------

tomwer\.gui\.datawatcher\.datawatcher module
--------------------------------------------

.. automodule:: tomwer.gui.datawatcher.datawatcher
    :members:
    :undoc-members:
    :show-inheritance:

tomwer\.gui\.datawatcher\.actions module
----------------------------------------

.. automodule:: tomwer.gui.datawatcher.actions
    :members:
    :undoc-members:
    :show-inheritance:

tomwer\.gui\.datawatcher\.configuration module
----------------------------------------------

.. automodule:: tomwer.gui.datawatcher.configuration
    :members:
    :undoc-members:
    :show-inheritance:

tomwer\.gui\.datawatcher\.history module
----------------------------------------

.. automodule:: tomwer.gui.datawatcher.history
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: tomwer.gui.datawatcher
    :members:
    :undoc-members:
    :show-inheritance:
