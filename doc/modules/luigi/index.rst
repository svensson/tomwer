tomwer.luigi package
====================

.. currentmodule:: tomwer.gui

.. automodule:: tomwer.gui

.. toctree::
   :maxdepth: 1

   scheme.rst
   task/index.rst
