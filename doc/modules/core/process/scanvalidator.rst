ScanValidator
`````````````

.. currentmodule:: tomwer.core.process.scanvalidator

.. autoclass:: tomwer.core.process.scanvalidator.ScanValidator
    :members:
