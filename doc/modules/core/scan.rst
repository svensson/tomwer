tomwer\.core\.scan package
==========================

Package to define scan and available operations on them

Submodules
----------

Module contents
---------------


tomwer\.core\.scan\.scanbase module
--------------------------------------------

.. automodule:: tomwer.core.scan.scanbase
    :members:
    :undoc-members:
    :show-inheritance:


tomwer\.core\.scan\.edfscan module
--------------------------------------------

.. automodule:: tomwer.core.scan.edfscan
    :members:
    :undoc-members:
    :show-inheritance:

