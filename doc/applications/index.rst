-------------------
tomwer applications
-------------------

Several application are existing for tomwer.


.. note::

   To launch an application simply use:

    .. code-block:: bash

        tomwer [myapplication] [application parameters]

    you can access application help using:

    .. code-block:: bash

        tomwer help

    or tomwer -h to access the applications available.


Here is a list of them:


.. toctree::
   :maxdepth: 1

   darkref.rst
   ftserie.rst
   lamino.rst
   radiostack.rst
   samplemoved.rst
   slicestack.rst
