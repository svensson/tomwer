
tomwer darkref
==============

Purpose
-------

The *tomwer darkref* command is provided to define generate dark and flat field references from an acquisition folder containing reference images.
It can generate the median or the mean of those files.


.. snapshotqt:: img/darkrefwidget.png
   :width: 400 px
   :align: center

   from tomwer.gui.darkref.darkrefcopywidget import DarkRefAndCopyWidget
   from tomwer.synctools.ftseries import QReconsParams
   widget = DarkRefAndCopyWidget(reconsparams=QReconsParams())
   widget.show()

Usage
-----

::

    tomwer darkref [-h] [scan_path [scan_path ...]]



Options
-------

::

  scan_path             Path to the acquisition folder that we want to observe.


  -h, --help            show this help message and exit


Examples of usage
-----------------

tomwer darkref myscan/
