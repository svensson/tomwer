
tomwer ftserie
==============

Purpose
-------

The *tomwer ftserie* command is provided to define reconstruction parameters and launch a fastomo3 reconstruction.


.. snapshotqt:: img/ftserieswidget.png
   :width: 600 px
   :align: center

   from tomwer.gui.ftserie import FtserieWidget
   widget = FtserieWidget()
   widget.show()


Usage
-----

::

    tomwer ftserie [-h] [scan_path [scan_path ...]]



Options
-------

::

  scan_path             Path to the acquisition folder that we want to observe.


  -h, --help            show this help message and exit


Examples of usage
-----------------

tomwer ftserie myscan/
