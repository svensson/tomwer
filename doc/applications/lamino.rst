
tomwer lamino
=============

Purpose
-------


.. snapshotqt:: img/tofuwidget.png
   :width: 600 px
   :align: center

   import tempfile
   from tomwer.gui.lamino.tofu import TofuWindow
   from tomwer.core import utils

   folder = tempfile.mkdtemp()
   utils.fastMockAcquisition(folder)
   utils.mockReconstruction(folder, nRecons=2, nPagRecons=3)

   widget = TofuWindow(parent=None)
   widget.show()


GUI to call `tofu <https://github.com/ufo-kit/tofu>`_ and run tomographic reconstruction

This widget will call the tofu application. So this mean that tofu has to be installed before launching any laminography reconstruction.

Note
----

A set of option is automatically managed by the widget (available in 'expert' tab, options managed) but you can specify any other option by defining them in the 'expert' tab, in Additional options

Usage
-----

::

    tomwer lamino [-h] [scan_path]



Options
-------

::

  scan_path             Path to the acquisition folder where is stored the data to be reconstructed.


  -h, --help            show this help message and exit


Examples of usage
-----------------

tomwer lamino myscan/
