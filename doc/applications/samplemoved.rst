
tomwer samplemoved
==================

Purpose
-------

The *tomwer samplemoved* command is provided to help the user observe if a
sample moved during an acquisition.


.. image:: ../img/sampleMoved.png
   :width: 800 px
   :align: center


Usage
-----

::

    tomwer samplemoved [-h] [scan_path [scan_path ...]]



Options
-------

::

  scan_path             Path to the acquisition folder that we want to observe.


  -h, --help            show this help message and exit


Examples of usage
-----------------

tomwer samplemoved myscan/
