
tomwer radiostack
=================

Purpose
-------

The *tomwer radiostack* command is provided to view all existing slices under a root directory.
It will parse all sub folder for existing reconstruction and store them into a browsable stack.
Slices will be sorted first by folder then by slice index.

.. snapshotqt:: img/radiostackwidget.png
   :width: 600 px
   :align: center

   from tomwer.test.utils import UtilsTest
   from tomwer.gui.stacks import RadioStack
   from tomwer.core import utils
   folder = UtilsTest.getInternalTestDir("001_0.28_19keV_Al63")
   widget = RadioStack()
   widget.addLeafScan(folder)
   widget.show()


Usage
-----

::

    tomwer radiostack [-h] [scan_path [scan_path ...]]



Options
-------

::

  scan_path             Path of the root folder.


  -h, --help            show this help message and exit


Examples of usage
-----------------

tomwer radiostack rootdir/
