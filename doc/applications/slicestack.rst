
tomwer slicestack
=================

Purpose
-------

The *tomwer slicestack* command is provided to view all existing slices under a root directory.
It will parse all sub folder for existing reconstruction and store them into a browseable stack.
Slices will be sorted first by folder then by slice index.

.. snapshotqt:: img/slicestackwidget.png
   :width: 600 px
   :align: center

   import tempfile
   from tomwer.gui.stacks import SliceStack
   from tomwer.core import utils

   folder = tempfile.mkdtemp()
   utils.fastMockAcquisition(folder)
   utils.mockReconstruction(folder, nRecons=2, nPagRecons=3)

   widget = SliceStack()
   widget.addLeafScan(folder)
   widget.show()


Usage
-----

::

    tomwer slicestack [-h] [scan_path [scan_path ...]]



Options
-------

::

  scan_path             Path of the root folder.


  -h, --help            show this help message and exit


Examples of usage
-----------------

tomwer slicestack rootdir/
