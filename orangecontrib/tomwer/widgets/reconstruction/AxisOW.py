# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/03/2019"


from Orange.widgets import widget, gui
from Orange.widgets.settings import Setting
from orangecontrib.tomwer.orange.settings import CallbackSettingsHandler
from orangecontrib.tomwer.widgets.utils import (convertInputsForOrange,
                                                convertOutputsForOrange)
from tomwer.core.process.reconstruction.axis import AxisProcess
from tomwer.synctools.ftseries import QReconsParams
from tomwer.gui.ftserie.axis import AxisWindow
from tomwer.core.scan.scanbase import TomoBase

import logging

logger = logging.getLogger(__name__)


class AxisOW(widget.OWWidget, AxisProcess):
    """
    Widget used to defined the center of rotation axis to be used for a
    reconstruction.

    :param bool _connect_handler: True if we want to store the modifications
                      on the setting. Need for unit test since
                      keep alive qt widgets.
    """
    name = "axis definition"
    id = "orange.widgets.tomwer.axis"
    description = "use to define the axis position"
    icon = "icons/axis.png"
    priority = 14
    category = "esrfWidgets"
    keywords = ["tomography", "axis", "tomwer", "reconstruction", "rotation",
                "position", "ftseries"]

    inputs = convertInputsForOrange(AxisProcess.inputs)
    outputs = convertOutputsForOrange(AxisProcess.outputs)

    want_main_area = True
    resizing_enabled = True
    allows_cycle = True
    compress_signal = False

    settingsHandler = CallbackSettingsHandler()

    _rpSetting = Setting(dict())
    "AxisRP store as dict"

    def __init__(self, parent=None, _connect_handler=True):
        self.__lastAxisProcessParamsCache = None
        recons_params = QReconsParams()
        if self._rpSetting != dict():
            try:
                recons_params.axis.load_from_dict(self._rpSetting)
            except:
                logger.warning('fail to load reconstruction settings')

        widget.OWWidget.__init__(self, parent)
        AxisProcess.__init__(self, recons_params=recons_params.axis)

        self._widget = AxisWindow(parent=self, axis=recons_params)

        # TODO: add validation, cancel ...
        self._layout = gui.vBox(self.mainArea, self.name).layout()
        self._layout.setContentsMargins(0, 0, 0, 0)

        self._layout.addWidget(self._widget)

        if _connect_handler:
            self.settingsHandler.addCallback(self._updateSettingsVals)

        # connect Signal / Slot
        self._widget.sigComputationRequested.connect(self.__compute)
        self._widget.sigApply.connect(self.__validate)
        self._widget.sigAxisEditionLocked.connect(self.__lockReconsParams)

        # expose API
        self.setLocked = self._widget.setLocked

    def __compute(self):
        if self.__scan:
            self.__lastAxisProcessParamsCache = self.recons_params.to_dict()
            AxisProcess.process(self, self.__scan)

    def __validate(self):
        if self.__scan:
            # if parameters has changed since last processing, reprocess axis method
            if self.recons_params.to_dict() != self.__lastAxisProcessParamsCache:
                self.__compute()

            self.signalReady(self.__scan)
            self.accept()

    def __lockReconsParams(self, lock):
        AxisProcess.set_editable(self, not lock)

    def signalReady(self, scanID):
        assert isinstance(scanID, TomoBase)
        self.send("data", scanID)

    def process(self, scan=None):
        """overwrite for updating gui"""
        if self._widget.isLocked():
            scan_id = id(scan)
            scan_res = AxisProcess.process(self, scan)
            assert id(scan_res) == scan_id
            assert scan.tomo_recons_params is not None
            assert scan.tomo_recons_params.axis is not None
            self.signalReady(scan_res)
        else:
            self.__scan = scan
            self.exec()

    def _updateSettingsVals(self):
        self._rpSetting = self._recons_params.to_dict()

    @property
    def recons_params(self):
        return self._recons_params

    def _instanciateReconsParams(self):
        return QReconsParams(empty=True)
