# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "19/07/2018"

from Orange.widgets import widget, gui
from silx.gui import qt
from Orange.canvas.registry.description import InputSignal, OutputSignal
from tomwer.web.client import OWClient
from tomwer.gui.conditions.filter import FileNameFilterWidget
from tomwer.core.scan.scanbase import TomoBase
import logging

logger = logging.getLogger(__name__)


class NameFilterOW(widget.OWWidget, OWClient):
    name = "scan filter"
    id = "orange.widgets.tomwer.filterow"
    description = "Simple widget which filter some data directory if the name" \
                  "doesn't match with the pattern defined."
    icon = "icons/namefilter.svg"
    priority = 106
    category = "esrfWidgets"
    keywords = ['tomography', 'selection', 'tomwer', 'folder', 'filter']

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    inputs = [InputSignal(name="data", handler='applyfilter', type=TomoBase)]
    outputs = [OutputSignal(name="data", type=TomoBase)]


    def __init__(self, parent=None):
        """
        """
        widget.OWWidget.__init__(self, parent)
        OWClient.__init__(self, (logger))

        self.widget = FileNameFilterWidget(parent=self)
        self.widget.setContentsMargins(0, 0, 0, 0)

        layout = gui.vBox(self.mainArea, self.name).layout()
        layout.addWidget(self.widget)
        spacer = qt.QWidget(parent=self)
        spacer.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Expanding)
        layout.addWidget(spacer)

    def applyfilter(self, scan):
        if self.widget.isFiltered(scan):
            assert isinstance(scan, TomoBase)
            self.send("data", scan)
