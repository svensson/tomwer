# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["C. Nemoz", "H. Payno"]
__license__ = "MIT"
__date__ = "07/12/2016"

import logging

from Orange.widgets import widget, gui
from Orange.widgets import settings

from silx.gui import qt
from orangecontrib.tomwer.widgets.utils import (convertInputsForOrange,
                                                convertOutputsForOrange)
from orangecontrib.tomwer.orange.settings import CallbackSettingsHandler
from tomwer.core.process.datatransfert import FolderTransfert, \
    logger as FTLogger
from tomwer.web.client import OWClient
from tomwer.gui.datatransfert import DataTransfertSelector

logger = logging.getLogger(__name__)


class DataTransfertOW(widget.OWWidget, FolderTransfert, OWClient):
    """
    A simple widget managing the copy of an incoming folder to an other one

    :param parent: the parent widget
    """
    name = "data transfert"
    id = "orange.widgets.tomwer.foldertransfert"
    description = "This widget insure data transfert of the received data "
    description += "to the given directory"
    icon = "icons/folder-transfert.svg"
    priority = 30
    category = "esrfWidgets"
    keywords = ["tomography", "transfert", "cp", "copy", "move", "file",
                "tomwer", 'folder']

    inputs = convertInputsForOrange(FolderTransfert.inputs)
    outputs = convertOutputsForOrange(FolderTransfert.outputs)

    settingsHandler = CallbackSettingsHandler()

    want_main_area = True
    resizing_enabled = True
    compress_signal = False

    dest_dir = settings.Setting(str())
    """Parameters directly editabled from the TOFU interface"""

    scanready = qt.Signal(str)

    def __init__(self, parent=None):
        FolderTransfert.__init__(self)
        widget.OWWidget.__init__(self, parent)
        OWClient.__init__(self, (logger, FTLogger))

        # define GUI
        self._widget = DataTransfertSelector(parent=self,
                                             rnice_option=True,
                                             default_root_folder=self._getDefaultOutputDir())
        layout = gui.vBox(self.mainArea, self.name).layout()
        layout.addWidget(self._widget)

        # signal / SLOT connection
        self._widget.sigSelectionChanged.connect(self.setDestDir)
        self.settingsHandler.addCallback(self._updateSettingsVals)

        # setting configuration
        if self.dest_dir != '':
            self._widget.setFolder(self.dest_dir)

    def _requestFolder(self):
        """Launch a QFileDialog to ask the user the output directory"""
        dialog = qt.QFileDialog(self)
        dialog.setWindowTitle("Destination folder")
        dialog.setModal(1)
        dialog.setFileMode(qt.QFileDialog.DirectoryOnly)

        if not dialog.exec_():
            dialog.close()
            return None

        return dialog.selectedFiles()[0]

    def signalTransfertOk(self, scanID):
        self.send("data", scanID)

    def _updateSettingsVals(self):
        """function used to update the settings values"""
        self.dest_dir = self._destDir

