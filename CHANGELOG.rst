Change Log
==========

0.3.0: 2019/01/08
-----------------

* core
    * add simple python binding for launching Tofu (for now only called in python 2)
    * conditions: new package to include some conditions (folder names...) on a workflow.
    * ftseries
        * duplicate octave outputs to 'octave.log'
        * possibility to run several Paganin reconstructions with different parameters
    * create different ;odes for loading images stack
    * data transfert: add pre-transfert operations
    * create tomwer.core.scan package which should contains the different type of scan we can encounter (edfscan, hdf5scan...).
      Define also interface to sue and specific treatment for those.

* gui
    * lamino: add a TofuWidget to call lamino reconstruction from Tofu.
    * compare images: use the one from silx if silx >= 0.9
    * data list:
        * add a remove all button
        * deal with mouse drop
    * add widgets radio stack and slice stack which are recording all radio or slice passing by those widget and allow browsing through them.
    * live slice: embed live slice into an Orange widget if installed.
    * add gui for new conditions process
    * separate tomwer's orange add-on widgets into three part: control, reconstruction and visualization.
    * data validator: add three different option to load images (just in time, when required and load as soon as possible )

* doc
    * add automatic screenshots for doc generation

* luigi
    * add first proto step. No functional yet


0.2.0: 2018/07/16
-----------------

* add `dark & flat fields` process to manage dark and flat field creation (create madian of mean).
  manage also copy of dark and flat field if not existing in one repository.
* refactoring. tomwer core functions and qt base widgets are moved in the tomwer package. Now the orangecontrib subpackage only contains the orange add-on.
* ftseries:
    * remove the octave version readed and loaded
* data wather
    * now making observations in parallel
    * if the acquisition folder contains a `.abo` file then 'skip it'. Considering it as their was no data.
    * gui refactoring
* scan validator: gui as stack. User can browse through the stack.
* add group slice to display all reconstructed sliced received
* add data selector: simple gui to select one scan in a list
* sample moved: interface to display 'key radios' and see if the sample has moved during acquisition.
* add applications:
    * darkref
    * ftserie
    * samplemoved
    * slicestack


0.1.0: 2017/09/28
-----------------

* add core functionalities

    * tomodir: aka data watcher. Detect acquisition finished. Sync data with destination directory as soon as possible if in lbsram system.
    * ftseries: interface for fastomo3 (octave)

* add orange-add on

    * ftseries widget
    * scan validator: validate a scan or ask for a reconstruction changing parameters
    * data watcher
    * data viewer
    * data list
    * data transfert
