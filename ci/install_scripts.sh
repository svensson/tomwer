#!/bin/bash

function install_silx(){

    if [ "$2" = 'latest' ]; then
        pip install silx
    else
        unset http_proxy
        unset https_proxy
        git clone https://github.com/silx-kit/silx.git
        cd silx
        pip install mako
        pip install pybind11
        pip install -r requirements.txt
        pip install .
        export http_proxy=http://proxy.esrf.fr:3128/
        export https_proxy=http://proxy.esrf.fr:3128/
    fi
}


function silx_version(){
    python -c 'import silx; print(silx.version)'
}


function install_orange3(){
    unset http_proxy
    unset https_proxy
    git clone --single-branch --branch master https://github.com/payno/orange3.git
    cd orange3
    pip install -r requirements-core.txt
    pip install .
    cd ..
    export http_proxy=http://proxy.esrf.fr:3128/
    export https_proxy=http://proxy.esrf.fr:3128/
}


function install_anyqt(){

    if [ "$2" = 'master' ]; then
        unset http_proxy
        unset https_proxy
        git clone --single-branch https://github.com/ales-erjavec/anyqt.git
        cd anyqt
        pip install .
        cd ..
        export http_proxy=http://proxy.esrf.fr:3128/
        export https_proxy=http://proxy.esrf.fr:3128/
    fi
}
